O projeto final da Unidade Curricular de
Laboratório Multimédia 4 consistiu no desenvolvimento
de uma aplicação com base em linguagens de hipertexto
HTML, formatação CSS e as linguagens de programação
JavaScript e PHP, além da linguagem de pesquisa
declarativa padrão de bases de dados relacionais, SQL.
Para ser definida a temática relativa ao desenvolvimento
das aplicações, foi criada uma parceria com a Fábrica
Centro de Ciência Viva, onde estas seriam desenvolvidas
para posterior utilização em contexto aproximado do
mundo real de trabalho.

Na ótica do grupo, o que mais se destacou durante a
visita à Fábrica foi o facto desta aceitar as inscrições para
eventos por email e controlar o número de inscrições
com suporte de papel, o que não é nada cómodo, prático
ou eficiente.

Perante os problemas apresentados, o grupo decidiu
trabalhar a questão da organização dos eventos da
Fábrica, criando assim a Agenda Viva. Esta aplicação
visa uma maior organização para os funcionários na área
das inscrições para as atividades organizadas, assim
como mostrar de forma detalhada e organizada os
eventos que decorrerão, oferecendo dessarte uma
pesquisa mais rápida e completa. Um dos objetivos
passou por tornar a inscrição na atividade mais cómoda e
rápida, de modo a não continuar a haver a atual mudança
pouco eficiente entre a página web da Fábrica e a página
de correio eletrónico.