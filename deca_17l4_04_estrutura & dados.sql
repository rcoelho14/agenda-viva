-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 13-Jun-2018 às 14:47
-- Versão do servidor: 5.6.23-log
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `deca_17l4_04`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `bilhetes`
--

CREATE TABLE `bilhetes` (
  `id_bilhetes` int(11) NOT NULL,
  `numero_bilhetes` int(11) DEFAULT NULL,
  `date_creation` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `pago` tinyint(4) DEFAULT NULL,
  `finalizado` tinyint(4) DEFAULT NULL,
  `id_buyer` int(11) NOT NULL,
  `ref_id_user_geral` int(11) NOT NULL,
  `ref_id_eventos_horarios` int(11) NOT NULL,
  `date_pago` datetime DEFAULT NULL,
  `date_finalizado` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `bilhetes`
--

INSERT INTO `bilhetes` (`id_bilhetes`, `numero_bilhetes`, `date_creation`, `pago`, `finalizado`, `id_buyer`, `ref_id_user_geral`, `ref_id_eventos_horarios`, `date_pago`, `date_finalizado`) VALUES
(1, 2, '2018-06-04 11:44:51', 15, 1, 0, 69, 2, '2018-06-04 01:00:00', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `escolaridade`
--

CREATE TABLE `escolaridade` (
  `id_escolaridade` int(11) NOT NULL,
  `tipo` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `escolaridade`
--

INSERT INTO `escolaridade` (`id_escolaridade`, `tipo`) VALUES
(1, '4º Ano'),
(2, '6º Ano'),
(3, '9º Ano'),
(4, '12º Ano'),
(5, 'Licenciatura'),
(6, 'Mestrado'),
(7, 'Doutoramento');

-- --------------------------------------------------------

--
-- Estrutura da tabela `eventos`
--

CREATE TABLE `eventos` (
  `id_eventos` int(11) NOT NULL,
  `nome` varchar(45) NOT NULL,
  `description_short` varchar(100) DEFAULT NULL,
  `description` longtext,
  `lotacao` int(11) NOT NULL,
  `preco` decimal(5,2) NOT NULL,
  `ref_id_users_admin` int(11) NOT NULL,
  `ref_id_eventos_tipo` int(11) NOT NULL,
  `date_creation` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `imagem` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `eventos`
--

INSERT INTO `eventos` (`id_eventos`, `nome`, `description_short`, `description`, `lotacao`, `preco`, `ref_id_users_admin`, `ref_id_eventos_tipo`, `date_creation`, `imagem`) VALUES
(1, 'DOMINGO DE MANHÃ NA BARRIGA DO CARACOL', 'Dentro de um caracol gorducho, contam-se histórias com ciência para explorar em família!', 'Para um fim de semana em cheio, a Fábrica oferece aos mais novos um programa bem diferente. Dentro de um caracol gorducho, contam-se histórias com ciência para explorar em família!\r\n\r\nHistória de um Palhaço\r\nMal o viram, as crianças gritaram: “Pa-lha-ço! Pa-lha-ço!”… E o sr. Pepe viu-se avançar Circo adentro, equilibrado lá tão alto, de pernas desengonçadas na corda bamba, sorriso envergonhado, roupa colorida e os seus dois baldes de água, um de cada lado, bem abaixo, na vara flexível gigante… E a cada pingo cá para baixo, na cabeça da bailarina zangada, o seu coração feliz só ouvia: gargalhadas e mais gargalhadas! Palmas e gargalhadas! Viva!\r\n\r\n\r\n', 25, '5.00', 69, 2, '2018-06-12 09:30:55', ''),
(3, 'BABYSITTING DE CIÊNCIA', 'Vamos cuidar de ti com ciência!', 'Está cientificamente provado: os filhos precisam de ter um fim de dia sem pais, uma vez por mês! E não é só para se divertirem com ciência! É para crescerem saudáveis! É a 4ª lei de New Town, Aveiro, e confirma-se com experiências inesquecíveis, na Fábrica!\r\nO programa inclui atividades científicas, jantar (opcional) e ceia.\r\n\r\n\r\nOFERTA AOS PAIS\r\n1 bebida no Bar Toc’aqui  \r\n\r\n', 20, '10.00', 69, 2, '2018-06-12 09:31:07', ''),
(8, 'CIÊNCIA AO PEQUENO-ALMOÇO', 'Vamos tomar o pequeno almoço com ciência!', 'Ciência ao pequeno-almoço! é uma atividade da Fábrica Centro Ciência Viva de Aveiro, que acontece no Hotel As Américas, num espaço acolhedor e familiar, entre cientistas e crianças. \r\nNeste encontro, as conversas com ciência, sempre acompanhadas de muitas experiências, serão ingredientes fundamentais!\r\nO desafio está lançado… inscreve-te! \r\n\r\n\r\n<br>Programa:\r\n\r\n<br>14 jan \'18 | Fibonacci fresquinho, logo pela manhã! | Convidada: Maria Costa | Investigadora do Depto. de Matemática da Universidade de Aveiro\r\n\r\n\r\n<br>18 fev \'18 | És tu a jogar, calhou: \"célula\"! | Convidada: Catarina Almeida | Investigadora do Depto. de Ciências Médicas da Universidade de Aveiro\r\n\r\n\r\n<br>11 mar \'18 | Química qb! | Convidada: Rosário Domingues | Investigadora do Depto. de Química da Universidade de Aveiro\r\n\r\n\r\n<br>15 abril \'18 | Sem a força gravítica o fiambre foge do pão?! | Convidado: Pedro Pombo | Docente do Depto. de Física da Universidade de Aveiro e Diretor da Fábrica Centro Ciência Viva de Aveiro\r\n\r\n\r\n<br>20 maio \'18 | Convidado: Ana Morete | Médica do Serviço de Imunoalergologia, do Centro Hospitalar do Baixo Vouga\r\n\r\n\r\n', 25, '5.00', 69, 2, '2018-06-12 09:31:16', ''),
(9, 'CLUBE DO CIENTISTA', 'Vamos criar pequenos cientistas!', 'Ainda não és sócio do melhor Clube de Cientistas do mundo?! Está no Aveiro Center!\r\nParticipa nas atividades disponíveis, um domingo por mês, e carimba a tua experiência divertida no cartão do Cientista que recebes!\r\n\r\nO “Clube do Cientista” é um projeto da Fábrica Centro Ciência Viva de Aveiro em parceria com o Aveiro Center, cujo objetivo é “criar pequenos cientistas”, promovendo o gosto pela ciência junto dos mais novos. Este projeto, que decorre no Aveiro Center desde maio de 2014, consiste num conjunto de atividades de ciência que decorrem num domingo por mês, até abril de 2018.\r\n\r\nAos participantes das sessões é entregue o cartão do ”Clube do Cientista” para carimbar a participação em cada uma das atividades. \r\n\r\n\r\n\r\n<br>Programa:\r\n\r\n<br>7 maio ’17 | Faz o teu sabonete \r\n\r\n<br>11 junho ’17 | Plasticina condutora \r\n\r\n<br>9 julho ’17 | Faz o teu exfoliante de pele \r\n\r\n<br>6 agosto ’17 | Protocolo bombom! \r\n\r\n<br>3 setembro ’17 | Gelado científico \r\n\r\n<br>1 outubro ’17 | Ciência em prosa \r\n\r\n<br>5 novembro ’17 | Robôs Ozobot \r\n\r\n<br>3 dezembro ’17 | Workshop de Natal \r\n\r\n<br>7 janeiro ’18 | Massa ao Centro... para brincar!\r\n\r\n<br>4 fevereiro ’18 | Não me toques que me eletrificas! \r\n\r\n<br>4 março ’18 | Workshop de origamis \r\n\r\n<br>8 abril ’18 | A história vai ao Clube\r\n\r\n', 25, '0.00', 69, 2, '2018-06-12 09:31:24', ''),
(10, 'DÓING', 'Vamos “pensar com as mãos”', 'Um espaço de produção criativa que relaciona arte com ciência e engenharia. Neste maker space o público pode manipular tecnologias digitais e analógicas para explorar ideias, aprender técnicas e criar novos produtos.\r\n\r\nInfluenciado pelo movimento maker e na cultura do-it-yourself, este é um espaço onde os visitantes são encorajados a “pensar com as mãos” e a “aprender fazendo”. É um espaço para criar, fazer, experimentar, construir e partilhar, onde tentativa e erro se conjugam de forma divertida e inspiradora. Os visitantes têm à sua disposição um conjunto de materiais, ferramentas e equipamentos para desenvolverem os seus próprios projetos.\r\n\r\nNeste maker space, disponível para escolas e famílias, são dinamizadas atividades e workshops regulares, que combinam arte, ciência e tecnologia.', 25, '5.00', 69, 1, '2018-06-12 09:31:32', ''),
(11, 'ARTE EM PLÁSTICO', 'Vamos juntar arte à reciclagem de plásticos!', 'Os plásticos são polímeros presentes no nosso dia a dia e compõem grande parte do lixo produzido atualmente. O próximo desafio Dóing que te lançamos é o de juntar arte à reciclagem de plásticos. Usa a tua imaginação e cria colares, pulseiras, porta-chaves, entre outros, usando embalagens de plástico velhas e sem grande utilidade aparente. Vem contribuir para melhorar o ambiente do planeta e aprender algumas das características destes polímeros tão comuns. Participa e vais ver como é fácil e divertido ser amigo do ambiente.', 10, '5.00', 69, 1, '2018-06-12 09:31:41', ''),
(12, 'VEM FAZER UM HOLOGRAMA', 'Vamos perceber o que é um holograma!', 'Já ouviste falar em hologramas? Neste workshop Doing vai ser possível perceber de forma clara o que é um holograma e explorar as diferenças, vantagens e limitações dos hologramas em relação a outras tecnologias de projeção 3D. Numa metodologia Maker todos os participantes poderão fazer o seu holograma e testar técnicas de registo holográfico. No final usa a impressora 3D e cria uma moldura para o teu holograma. Participa!', 20, '5.00', 69, 1, '2018-06-12 09:31:48', ''),
(13, 'FAZ TU MESMO', 'Queres iniciar-te no mundo da programação mas não sabes por onde começar? ', 'Queres iniciar-te no mundo da programação mas não sabes por onde começar? Então este workshop é para ti! \r\nVem dar asas à tua criatividade, aprendendo a programar microcontroladores Arduino de uma forma simples e divertida. Não faltes!', 10, '5.00', 69, 1, '2018-06-12 09:31:54', ''),
(14, 'MÃOS NA MASSA', 'Vamos brincar com as leis da ótica!', 'Uma exposição que integra módulos interativos, envolvendo várias áreas da ciência. Estes incidem em diferentes conteúdos científicos e tecnológicos, que muitas vezes encontramos no nosso dia a dia.\r\nBrincar com as leis da ótica através do manuseamento de um Labirinto Ótico; observar as diferenças de temperatura do corpo com uma Câmara Térmica; escrever mensagens em Código Morse, ou ficar de cabelos em pé com o Gerador de Van de Graaff são apenas alguns dos desafios propostos. \r\n\r\nAo longo da exposição, o visitante é estimulado a interagir com os módulos de forma a explorar autonomamente os fenómenos científicos apresentados. \r\n\r\nUma exposição onde é proibido não mexer!', 25, '0.00', 69, 3, '2018-06-12 09:43:23', ''),
(15, 'JANELAS DE LUZ', 'Uma exposição sobre luz que convida o público a uma viagem ao mundo dos hologramas.', 'De forma cativante e envolvente, o público é levado a explorar diversos aspetos da ciência que está por trás desta tecnologia de imagens 3D. \r\n\r\nAo longo da exposição é possível responder às questões: para que servem? como se fazem? quais os materiais necessários? Para tal, a exposição inclui 5 áreas: 1 laboratório, 1 módulo com 6 painéis interativos, 1 mostra de 15 hologramas, 1 bancada de hologramas riscados e 1 quiosque informativo. \r\n\r\nFica feito o convite: venha espreitar as nossas “Janelas de Luz”!', 25, '0.00', 69, 3, '2018-06-12 09:43:05', ''),
(16, 'PERSPETIVAS ARTE & CIÊNCIA', 'Ver para crer: um zoom ao organismo', 'A Fábrica promove exposições temporárias de ilustração e fotografia de conteúdo científico, genericamente designadas por \"Perspetivas Arte & Ciência\". Têm sido convidados diversos autores, cujo trabalho se torna uma ferramenta de comunicação de ciência com um forte cariz artístico.\r\n\r\n<br>Atualmente encontra-se patente ao público a Exposição Coletiva de Imagens Científicas \"Ver para crer: um zoom ao organismo\", da autoria de 26 investigadores da Universidade de Aveiro e tem como curadores Catarina Almeida e Miguel Aroso do Instituto de Biomedicina (iBiMED).\r\n<br>O funcionamento do organismo humano é governado por vários acontecimentos que começamos agora a compreender. A área da BioMedicina utiliza diferentes abordagens e organismos modelo para estudar todos os detalhes dos fenómenos que ocorrem tanto na saúde como na doença. Esta exposição mostra diferentes formas utilizadas pelos investigadores da Universidade de Aveiro para visualizar e representar estes acontecimentos. E são várias as técnicas que permitem tornar visível o que antes não era percetível (por exemplo a microscopia, eletroforese, modelação molecular, entre outras). \r\n\r\n<br>Aceite o nosso convite para observar, tal como um cientista, o que ocorre nos tecidos, células, organelos e proteínas que constituem o seu corpo.', 25, '0.00', 69, 3, '2018-06-12 09:45:05', ''),
(17, 'SALA DA MATEMÁTICA', 'Vamos exercitar o raciocínio matemático!', 'Através de atividades com diferentes níveis de dificuldade exercita-se o raciocínio matemático de forma divertida e desafiante.\r\nO raciocínio lógico, a rapidez de decisão, o respeito e a criatividade são algumas das capacidades que estarão implicadas. Os jogos de tabuleiro Ouri, Go, Semáforo, Amazonas, Hex e Pontos e Quadrados são os jogos matemáticos disponíveis.', 20, '5.00', 69, 4, '2018-06-12 09:59:47', ''),
(18, 'SALA DA ROBÓTICA', 'Queres construir ou programar um robô?', 'Os visitantes mais novos podem ser exímios programadores de sistemas robóticos! Construir um robô com peças Lego, usar sensores, motores, rodas dentadas, correias, alavancas, engrenagens (conceitos de física, afinal) e assim conseguir pôr um macaco a tocar tambor, um pião a girar ou um par de passaritos a bailar, pode ser uma experiência inesquecível.\r\nUma série de mini desafios permitem programar um robô que desempenha um conjunto de tarefas de modo a conseguir chutar uma bola no final. Não faltarão ainda corridas entre equipas para ver quem é mais rápido, mas sem esquecer que é preciso pontaria para acertar na bola.\r\n', 20, '10.00', 69, 4, '2018-06-12 10:03:07', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `eventos_horarios`
--

CREATE TABLE `eventos_horarios` (
  `id_eventos_horarios` int(11) NOT NULL,
  `date` date NOT NULL,
  `hora_inicio` varchar(8) NOT NULL,
  `duracao` varchar(8) NOT NULL,
  `lotacao` int(11) NOT NULL,
  `bilhetes_disponiveis` int(11) NOT NULL DEFAULT '10',
  `ref_id_eventos` int(11) NOT NULL,
  `ref_id_users_admin` int(11) NOT NULL,
  `date_creation` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `url` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `eventos_horarios`
--

INSERT INTO `eventos_horarios` (`id_eventos_horarios`, `date`, `hora_inicio`, `duracao`, `lotacao`, `bilhetes_disponiveis`, `ref_id_eventos`, `ref_id_users_admin`, `date_creation`, `url`) VALUES
(15, '2018-06-03', '10:00', '2:00', 25, 25, 13, 69, '2018-06-12 09:48:24', ''),
(17, '2018-06-12', '10:00', '2:00', 20, 20, 3, 69, '2018-06-12 09:49:06', ''),
(18, '2018-05-27', '10:00', '2:00', 20, 20, 12, 69, '2018-06-12 09:49:06', ''),
(19, '2018-06-10', '10:00', '3:00', 10, 10, 11, 69, '2018-06-12 09:49:06', ''),
(20, '2018-06-17', '10:00', '3:00', 25, 25, 10, 69, '2018-06-12 09:49:06', ''),
(21, '2018-06-17', '10:00', '2:00', 25, 25, 1, 69, '2018-06-12 09:49:06', ''),
(22, '2018-06-30', '11:00', '1:00', 25, 25, 8, 69, '2018-06-12 09:49:06', ''),
(23, '2018-06-23', '11:00', '1:00', 25, 25, 9, 69, '2018-06-12 09:49:06', ''),
(24, '2018-06-30', '10:00', '8:00', 25, 25, 14, 69, '2018-06-12 09:46:29', ''),
(25, '2018-06-29', '10:00', '8:00', 25, 25, 15, 69, '2018-06-12 09:47:05', ''),
(26, '2018-07-01', '10:00', '8:00', 25, 25, 16, 69, '2018-06-12 09:48:03', ''),
(27, '2018-06-26', '10:00', '1:00', 20, 20, 17, 69, '2018-06-12 10:09:09', ''),
(28, '2018-06-19', '14:00', '1:00', 20, 20, 18, 69, '2018-06-12 10:08:57', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `eventos_tipo`
--

CREATE TABLE `eventos_tipo` (
  `id_eventos_tipo` int(11) NOT NULL,
  `tipo` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `eventos_tipo`
--

INSERT INTO `eventos_tipo` (`id_eventos_tipo`, `tipo`) VALUES
(1, 'workshops'),
(2, 'atividades'),
(3, 'exposições'),
(4, 'sala');

-- --------------------------------------------------------

--
-- Estrutura da tabela `pais`
--

CREATE TABLE `pais` (
  `id_pais` int(11) NOT NULL,
  `nome` varchar(70) DEFAULT NULL,
  `name` varchar(70) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `pais`
--

INSERT INTO `pais` (`id_pais`, `nome`, `name`) VALUES
(1, 'AFEGANISTÃO', 'AFGHANISTAN'),
(2, 'ACROTÍRI E DECELIA', 'AKROTIRI E DEKÉLIA'),
(3, 'ÁFRICA DO SUL', 'SOUTH AFRICA'),
(4, 'ALBÂNIA', 'ALBANIA'),
(5, 'ALEMANHA', 'GERMANY'),
(6, 'AMERICAN SAMOA', 'AMERICAN SAMOA'),
(7, 'ANDORRA', 'ANDORRA'),
(8, 'ANGOLA', 'ANGOLA'),
(9, 'ANGUILLA', 'ANGUILLA'),
(10, 'ANTÍGUA E BARBUDA', 'ANTIGUA AND BARBUDA'),
(11, 'ANTILHAS NEERLANDESAS', 'NETHERLANDS ANTILLES'),
(12, 'ARÁBIA SAUDITA', 'SAUDI ARABIA'),
(13, 'ARGÉLIA', 'ALGERIA'),
(14, 'ARGENTINA', 'ARGENTINA'),
(15, 'ARMÉNIA', 'ARMENIA'),
(16, 'ARUBA', 'ARUBA'),
(17, 'AUSTRÁLIA', 'AUSTRALIA'),
(18, 'ÁUSTRIA', 'AUSTRIA'),
(19, 'AZERBAIJÃO', 'AZERBAIJAN'),
(20, 'BAHAMAS', 'BAHAMAS, THE'),
(21, 'BANGLADECHE', 'BANGLADESH'),
(22, 'BARBADOS', 'BARBADOS'),
(23, 'BARÉM', 'BAHRAIN'),
(24, 'BASSAS DA ÍNDIA', 'BASSAS DA INDIA'),
(25, 'BÉLGICA', 'BELGIUM'),
(26, 'BELIZE', 'BELIZE'),
(27, 'BENIM', 'BENIN'),
(28, 'BERMUDAS', 'BERMUDA'),
(29, 'BIELORRÚSSIA', 'BELARUS'),
(30, 'BOLÍVIA', 'BOLIVIA'),
(31, 'BÓSNIA E HERZEGOVINA', 'BOSNIA AND HERZEGOVINA'),
(32, 'BOTSUANA', 'BOTSWANA'),
(33, 'BRASIL', 'BRAZIL'),
(34, 'BRUNEI DARUSSALAM', 'BRUNEI DARUSSALAM'),
(35, 'BULGÁRIA', 'BULGARIA'),
(36, 'BURQUINA FASO', 'BURKINA FASO'),
(37, 'BURUNDI', 'BURUNDI'),
(38, 'BUTÃO', 'BHUTAN'),
(39, 'CABO VERDE', 'CAPE VERDE'),
(40, 'CAMARÕES', 'CAMEROON'),
(41, 'CAMBOJA', 'CAMBODIA'),
(42, 'CANADÁ', 'CANADA'),
(43, 'CATAR', 'QATAR'),
(44, 'CAZAQUISTÃO', 'KAZAKHSTAN'),
(45, 'REPÚBLICA CENTRO-AFRICANA', 'CENTRAL AFRICAN REPUBLIC'),
(46, 'CHADE', 'CHAD'),
(47, 'CHILE', 'CHILE'),
(48, 'CHINA', 'CHINA'),
(49, 'CHIPRE', 'CYPRUS'),
(50, 'COLÔMBIA', 'COLOMBIA'),
(51, 'COMORES', 'COMOROS'),
(52, 'CONGO', 'CONGO'),
(53, 'REPÚBLICA DEMOCRÁTICA DO CONGO', 'CONGO DEMOCRATIC REPUBLIC'),
(54, 'COREIA DO NORTE', 'KOREA NORTH'),
(55, 'COREIA DO SUL', 'KOREA SOUTH'),
(56, 'COSTA DO MARFIM', 'IVORY COAST'),
(57, 'COSTA RICA', 'COSTA RICA'),
(58, 'CROÁCIA', 'CROATIA'),
(59, 'CUBA', 'CUBA'),
(60, 'DINAMARCA', 'DENMARK'),
(61, 'DOMÍNICA', 'DOMINICA'),
(62, 'EGIPTO', 'EGYPT'),
(63, 'EMIRADOS ÁRABES UNIDOS', 'UNITED ARAB EMIRATES'),
(64, 'EQUADOR', 'ECUADOR'),
(65, 'ERITREIA', 'ERITREA'),
(66, 'ESLOVÁQUIA', 'SLOVAKIA'),
(67, 'ESLOVÉNIA', 'SLOVENIA'),
(68, 'ESPANHA', 'SPAIN'),
(69, 'ESTADOS UNIDOS', 'UNITED STATES'),
(70, 'ESTÓNIA', 'ESTONIA'),
(71, 'ETIÓPIA', 'ETHIOPIA'),
(72, 'FAIXA DE GAZA', 'GAZA STRIP'),
(73, 'FIJI', 'FIJI'),
(74, 'FILIPINAS', 'PHILIPPINES'),
(75, 'FINLÂNDIA', 'FINLAND'),
(76, 'FRANÇA', 'FRANCE'),
(77, 'GABÃO', 'GABON'),
(78, 'GÂMBIA', 'GAMBIA'),
(79, 'GANA', 'GHANA'),
(80, 'GEÓRGIA', 'GEORGIA'),
(81, 'GIBRALTAR', 'GIBRALTAR'),
(82, 'GRANADA', 'GRENADA'),
(83, 'GRÉCIA', 'GREECE'),
(84, 'GRONELÂNDIA', 'GREENLAND'),
(85, 'GUADALUPE', 'GUADELOUPE'),
(86, 'GUAM', 'GUAM'),
(87, 'GUATEMALA', 'GUATEMALA'),
(88, 'GUERNSEY', 'GUERNSEY'),
(89, 'GUIANA', 'GUYANA'),
(90, 'GUIANA FRANCESA', 'FRENCH GUIANA'),
(91, 'GUINÉ', 'GUINEA'),
(92, 'GUINÉ EQUATORIAL', 'EQUATORIAL GUINEA'),
(93, 'GUINÉ-BISSAU', 'GUINEA-BISSAU'),
(94, 'HAITI', 'HAITI'),
(95, 'HONDURAS', 'HONDURAS'),
(96, 'HONG KONG', 'HONG KONG'),
(97, 'HUNGRIA', 'HUNGARY'),
(98, 'IÉMEN', 'YEMEN'),
(99, 'ILHA BOUVET', 'BOUVET ISLAND'),
(100, 'ILHA CHRISTMAS', 'CHRISTMAS ISLAND'),
(101, 'ILHA DE CLIPPERTON', 'CLIPPERTON ISLAND'),
(102, 'ILHA DE JOÃO DA NOVA', 'JUAN DE NOVA ISLAND'),
(103, 'ILHA DE MAN', 'ISLE OF MAN'),
(104, 'ILHA DE NAVASSA', 'NAVASSA ISLAND'),
(105, 'ILHA EUROPA', 'EUROPA ISLAND'),
(106, 'ILHA NORFOLK', 'NORFOLK ISLAND'),
(107, 'ILHA TROMELIN', 'TROMELIN ISLAND'),
(108, 'ILHAS ASHMORE E CARTIER', 'ASHMORE AND CARTIER ISLANDS'),
(109, 'ILHAS CAIMAN', 'CAYMAN ISLANDS'),
(110, 'ILHAS COCOS (KEELING)', 'COCOS (KEELING) ISLANDS'),
(111, 'ILHAS COOK', 'COOK ISLANDS'),
(112, 'ILHAS DO MAR DE CORAL', 'CORAL SEA ISLANDS'),
(113, 'ILHAS FALKLANDS (ILHAS MALVINAS)', 'FALKLAND ISLANDS (ISLAS MALVINAS)'),
(114, 'ILHAS FEROE', 'FAROE ISLANDS'),
(115, 'ILHAS GEÓRGIA DO SUL E SANDWICH DO SUL', 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS'),
(116, 'ILHAS MARIANAS DO NORTE', 'NORTHERN MARIANA ISLANDS'),
(117, 'ILHAS MARSHALL', 'MARSHALL ISLANDS'),
(118, 'ILHAS PARACEL', 'PARACEL ISLANDS'),
(119, 'ILHAS PITCAIRN', 'PITCAIRN ISLANDS'),
(120, 'ILHAS SALOMÃO', 'SOLOMON ISLANDS'),
(121, 'ILHAS SPRATLY', 'SPRATLY ISLANDS'),
(122, 'ILHAS VIRGENS AMERICANAS', 'UNITED STATES VIRGIN ISLANDS'),
(123, 'ILHAS VIRGENS BRITÂNICAS', 'BRITISH VIRGIN ISLANDS'),
(124, 'ÍNDIA', 'INDIA'),
(125, 'INDONÉSIA', 'INDONESIA'),
(126, 'IRÃO', 'IRAN'),
(127, 'IRAQUE', 'IRAQ'),
(128, 'IRLANDA', 'IRELAND'),
(129, 'ISLÂNDIA', 'ICELAND'),
(130, 'ISRAEL', 'ISRAEL'),
(131, 'ITÁLIA', 'ITALY'),
(132, 'JAMAICA', 'JAMAICA'),
(133, 'JAN MAYEN', 'JAN MAYEN'),
(134, 'JAPÃO', 'JAPAN'),
(135, 'JERSEY', 'JERSEY'),
(136, 'JIBUTI', 'DJIBOUTI'),
(137, 'JORDÂNIA', 'JORDAN'),
(138, 'KIRIBATI', 'KIRIBATI'),
(139, 'KOWEIT', 'KUWAIT'),
(140, 'LAOS', 'LAOS'),
(141, 'LESOTO', 'LESOTHO'),
(142, 'LETÓNIA', 'LATVIA'),
(143, 'LÍBANO', 'LEBANON'),
(144, 'LIBÉRIA', 'LIBERIA'),
(145, 'LÍBIA', 'LIBYAN ARAB JAMAHIRIYA'),
(146, 'LISTENSTAINE', 'LIECHTENSTEIN'),
(147, 'LITUÂNIA', 'LITHUANIA'),
(148, 'LUXEMBURGO', 'LUXEMBOURG'),
(149, 'MACAU', 'MACAO'),
(150, 'MACEDÓNIA', 'MACEDONIA'),
(151, 'MADAGÁSCAR', 'MADAGASCAR'),
(152, 'MALÁSIA', 'MALAYSIA'),
(153, 'MALAVI', 'MALAWI'),
(154, 'MALDIVAS', 'MALDIVES'),
(155, 'MALI', 'MALI'),
(156, 'MALTA', 'MALTA'),
(157, 'MARROCOS', 'MOROCCO'),
(158, 'MARTINICA', 'MARTINIQUE'),
(159, 'MAURÍCIA', 'MAURITIUS'),
(160, 'MAURITÂNIA', 'MAURITANIA'),
(161, 'MAYOTTE', 'MAYOTTE'),
(162, 'MÉXICO', 'MEXICO'),
(163, 'MIANMAR', 'MYANMAR BURMA'),
(164, 'MICRONÉSIA', 'MICRONESIA'),
(165, 'MOÇAMBIQUE', 'MOZAMBIQUE'),
(166, 'MOLDÁVIA', 'MOLDOVA'),
(167, 'MÓNACO', 'MONACO'),
(168, 'MONGÓLIA', 'MONGOLIA'),
(169, 'MONTENEGRO', 'MONTENEGRO'),
(170, 'MONTSERRAT', 'MONTSERRAT'),
(171, 'NAMÍBIA', 'NAMIBIA'),
(172, 'NAURU', 'NAURU'),
(173, 'NEPAL', 'NEPAL'),
(174, 'NICARÁGUA', 'NICARAGUA'),
(175, 'NÍGER', 'NIGER'),
(176, 'NIGÉRIA', 'NIGERIA'),
(177, 'NIUE', 'NIUE'),
(178, 'NORUEGA', 'NORWAY'),
(179, 'NOVA CALEDÓNIA', 'NEW CALEDONIA'),
(180, 'NOVA ZELÂNDIA', 'NEW ZEALAND'),
(181, 'OMÃ', 'OMAN'),
(182, 'PAÍSES BAIXOS', 'NETHERLANDS'),
(183, 'PALAU', 'PALAU'),
(184, 'PALESTINA', 'PALESTINE'),
(185, 'PANAMÁ', 'PANAMA'),
(186, 'PAPUÁSIA-NOVA GUINÉ', 'PAPUA NEW GUINEA'),
(187, 'PAQUISTÃO', 'PAKISTAN'),
(188, 'PARAGUAI', 'PARAGUAY'),
(189, 'PERU', 'PERU'),
(190, 'POLINÉSIA FRANCESA', 'FRENCH POLYNESIA'),
(191, 'POLÓNIA', 'POLAND'),
(192, 'PORTO RICO', 'PUERTO RICO'),
(193, 'PORTUGAL', 'PORTUGAL'),
(194, 'QUÉNIA', 'KENYA'),
(195, 'QUIRGUIZISTÃO', 'KYRGYZSTAN'),
(196, 'REINO UNIDO', 'UNITED KINGDOM'),
(197, 'REPÚBLICA CHECA', 'CZECH REPUBLIC'),
(198, 'REPÚBLICA DOMINICANA', 'DOMINICAN REPUBLIC'),
(199, 'ROMÉNIA', 'ROMANIA'),
(200, 'RUANDA', 'RWANDA'),
(201, 'RÚSSIA', 'RUSSIAN FEDERATION'),
(202, 'SAHARA OCCIDENTAL', 'WESTERN SAHARA'),
(203, 'SALVADOR', 'EL SALVADOR'),
(204, 'SAMOA', 'SAMOA'),
(205, 'SANTA HELENA', 'SAINT HELENA'),
(206, 'SANTA LÚCIA', 'SAINT LUCIA'),
(207, 'SANTA SÉ', 'HOLY SEE'),
(208, 'SÃO CRISTÓVÃO E NEVES', 'SAINT KITTS AND NEVIS'),
(209, 'SÃO MARINO', 'SAN MARINO'),
(210, 'SÃO PEDRO E MIQUELÃO', 'SAINT PIERRE AND MIQUELON'),
(211, 'SÃO TOMÉ E PRÍNCIPE', 'SAO TOME AND PRINCIPE'),
(212, 'SÃO VICENTE E GRANADINAS', 'SAINT VINCENT AND THE GRENADINES'),
(213, 'SEICHELES', 'SEYCHELLES'),
(214, 'SENEGAL', 'SENEGAL'),
(215, 'SERRA LEOA', 'SIERRA LEONE'),
(216, 'SÉRVIA', 'SERBIA'),
(217, 'SINGAPURA', 'SINGAPORE'),
(218, 'SÍRIA', 'SYRIA'),
(219, 'SOMÁLIA', 'SOMALIA'),
(220, 'SRI LANCA', 'SRI LANKA'),
(221, 'SUAZILÂNDIA', 'SWAZILAND'),
(222, 'SUDÃO', 'SUDAN'),
(223, 'SUÉCIA', 'SWEDEN'),
(224, 'SUÍÇA', 'SWITZERLAND'),
(225, 'SURINAME', 'SURINAME'),
(226, 'SVALBARD', 'SVALBARD'),
(227, 'TAILÂNDIA', 'THAILAND'),
(228, 'TAIWAN', 'TAIWAN'),
(229, 'TAJIQUISTÃO', 'TAJIKISTAN'),
(230, 'TANZÂNIA', 'TANZANIA'),
(231, 'TERRITÓRIO BRITÂNICO DO OCEANO ÍNDICO', 'BRITISH INDIAN OCEAN TERRITORY'),
(232, 'TERRITÓRIO DAS ILHAS HEARD E MCDONALD', 'HEARD ISLAND AND MCDONALD ISLANDS'),
(233, 'TIMOR-LESTE', 'TIMOR-LESTE'),
(234, 'TOGO', 'TOGO'),
(235, 'TOKELAU', 'TOKELAU'),
(236, 'TONGA', 'TONGA'),
(237, 'TRINDADE E TOBAGO', 'TRINIDAD AND TOBAGO'),
(238, 'TUNÍSIA', 'TUNISIA'),
(239, 'TURKS E CAICOS', 'TURKS AND CAICOS ISLANDS'),
(240, 'TURQUEMENISTÃO', 'TURKMENISTAN'),
(241, 'TURQUIA', 'TURKEY'),
(242, 'TUVALU', 'TUVALU'),
(243, 'UCRÂNIA', 'UKRAINE'),
(244, 'UGANDA', 'UGANDA'),
(245, 'URUGUAI', 'URUGUAY'),
(246, 'USBEQUISTÃO', 'UZBEKISTAN'),
(247, 'VANUATU', 'VANUATU'),
(248, 'VENEZUELA', 'VENEZUELA'),
(249, 'VIETNAME', 'VIETNAM'),
(250, 'WALLIS E FUTUNA', 'WALLIS AND FUTUNA'),
(251, 'ZÂMBIA', 'ZAMBIA'),
(252, 'ZIMBABUÉ', 'ZIMBABWE');

-- --------------------------------------------------------

--
-- Estrutura da tabela `pontos_gastos`
--

CREATE TABLE `pontos_gastos` (
  `id_pontos_gastos` int(11) NOT NULL,
  `pontos_gastos` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ref_id_users_geral` int(11) NOT NULL,
  `ref_id_users_admin` int(11) NOT NULL,
  `ref_id_bilhetes` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `profissao`
--

CREATE TABLE `profissao` (
  `id_profissao` int(11) NOT NULL,
  `tipo` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `profissao`
--

INSERT INTO `profissao` (`id_profissao`, `tipo`) VALUES
(1, 'Desempregado'),
(2, 'Estudante'),
(3, 'Empregado a Conta Própria'),
(4, 'Empregado a Conta de Outrém'),
(5, 'Trabalhador Estudante'),
(6, 'Reformado');

-- --------------------------------------------------------

--
-- Estrutura da tabela `recupera_pass`
--

CREATE TABLE `recupera_pass` (
  `id_recupera` int(11) NOT NULL,
  `codigo1` varchar(10) NOT NULL,
  `codigo2` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `reviews`
--

CREATE TABLE `reviews` (
  `id_reviews` int(11) NOT NULL,
  `rating` int(11) NOT NULL,
  `text_pro` longtext,
  `text_con` longtext,
  `date_creation` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ref_id_users_geral_admin` int(11) DEFAULT NULL,
  `ref_id_users_geral_geral` int(11) NOT NULL,
  `ref_id_eventos_horarios` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `reviews`
--

INSERT INTO `reviews` (`id_reviews`, `rating`, `text_pro`, `text_con`, `date_creation`, `ref_id_users_geral_admin`, `ref_id_users_geral_geral`, `ref_id_eventos_horarios`) VALUES
(2, 50, 'Muito divertido', 'Nada a melhorar', '2018-06-03 23:11:33', 69, 63, 2),
(3, 100, 'Melhor dia de sempre!', 'Devia de existir uma aplicação com agenda', '2018-06-03 23:11:33', 36, 1, 3),
(4, 1, 'htht', 'gfgf', '2018-06-04 20:24:54', 74, 27, 7);

-- --------------------------------------------------------

--
-- Estrutura da tabela `salas`
--

CREATE TABLE `salas` (
  `id_salas` int(11) NOT NULL,
  `nome` varchar(45) NOT NULL,
  `description_short` varchar(100) DEFAULT NULL,
  `description` longtext,
  `ref_id_users_admin` int(11) NOT NULL,
  `date_creation` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `photo_name_uniqe` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `salas`
--

INSERT INTO `salas` (`id_salas`, `nome`, `description_short`, `description`, `ref_id_users_admin`, `date_creation`, `photo_name_uniqe`) VALUES
(1, 'Sala do Caracoll', 'Esta é a sala do caracol', 'A sala do Caracol é muito fixe!!!!!', 51, '2018-06-12 15:44:15', '3681788325b1fea692f8ca9.03734916WIN_20180612_15_44_30_Pro.jpg'),
(2, 'Cozinha', 'Sala da cozinha', 'Nesta sala vamos cozinhar muiiiiiiito e com ciências, que divertido!', 68, '2018-06-04 11:12:34', ''),
(3, 'Sala da Cozinha', 'Esta é a sala da cozinha', 'A Sala da Cozinha é muito divertida, aqui podes cozinhar com a ciência', 1, '2018-06-04 15:45:49', ''),
(15, '', '', '', 1, '2018-06-04 17:10:49', ''),
(17, 'doasp', 'jdsoap', 'jdoasp', 1, '2018-06-04 17:26:21', ''),
(18, 'SALA DE ESTUDO', 'SALA BARULHENTA', 'SALA GRANDE', 1, '2018-06-04 17:27:12', ''),
(19, 'nova_sala', 'sala pequena', 'sala grande', 1, '2018-06-04 17:28:17', ''),
(20, 'Sala da Bea', 'bea', 'beeeeeeeeeeea', 69, '2018-06-04 17:47:39', ''),
(21, 'Sala do Robot', 'Robot', 'Roooooobot', 69, '2018-06-04 17:51:43', ''),
(22, 'WC', 'wc', 'wc', 51, '2018-06-04 22:11:57', ''),
(23, 'teste', 'teste -', 'teste +', 82, '2018-06-05 13:03:18', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `salas_has_eventos`
--

CREATE TABLE `salas_has_eventos` (
  `id_salas_has_eventos` int(11) NOT NULL,
  `ref_id_salas` int(11) NOT NULL,
  `ref_id_eventos` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipo`
--

CREATE TABLE `tipo` (
  `id_tipo` int(11) NOT NULL,
  `tipo` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tipo`
--

INSERT INTO `tipo` (`id_tipo`, `tipo`) VALUES
(1, 'public'),
(2, 'admin');

-- --------------------------------------------------------

--
-- Estrutura da tabela `users_geral`
--

CREATE TABLE `users_geral` (
  `id_users_geral` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `apelido` varchar(100) NOT NULL,
  `data_nascimento` varchar(10) NOT NULL,
  `email` varchar(50) NOT NULL,
  `username` varchar(15) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `password_hash` varchar(255) NOT NULL,
  `date_creation` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ref_id_tipo` int(11) NOT NULL DEFAULT '1',
  `photo_name_uniq` varchar(128) DEFAULT NULL,
  `codigo1` varchar(10) DEFAULT NULL,
  `codigo2` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `users_geral`
--

INSERT INTO `users_geral` (`id_users_geral`, `nome`, `apelido`, `data_nascimento`, `email`, `username`, `activo`, `password_hash`, `date_creation`, `ref_id_tipo`, `photo_name_uniq`, `codigo1`, `codigo2`) VALUES
(1, 'alvaro_master', 'master', '2018-06-14', 'admin@admin.admin', 'alvaro', 1, '$2y$10$ZxnDJKjRUCH4XKzBGz46KOlDDJsnVMaRZeJ.5Tt90arB0q1vQu6pK', '2018-06-04 22:11:27', 1, '15656608305b15aba37f0683.98992886este.png', NULL, NULL),
(2, '', '', '', 'valu1@gmail.com', 'valu', 1, '$2y$10$SnfnTRQgpZVYatHh7.8kdeZzP0VMXJp9n58L74LCSdsmJh9gVEFva', '2018-06-04 20:29:15', 2, '', NULL, NULL),
(3, '', '', '', 'eu@eu.pt', 'maria', 0, '$2y$10$jlJ598kWZLTiUyldgEmX8uJNUL0IPTNS/elMl51sFPpnhUWGwEB2i', '2018-06-04 09:44:04', 1, '15037678565b12cfa8de72d7.24432138SLBenfica3estrelas.png', NULL, NULL),
(4, '', '', '', 'ola@ola.ola', 'ola', 0, '$2y$10$lvzmVnWzVqSgjYRVTcmJYewkj37hmIRABdNXEndpRM2lkrFPiGpCe', '2018-05-31 18:20:14', 1, '', NULL, NULL),
(6, '', '', '', 'f@ie.pfre', 'ef', 0, '$2y$10$kG23S2CUbY6GY9nNY9fqLOUlfd6Zcz5wpkURkbBfVpaDCbqsZ7xre', '2018-05-31 18:29:23', 1, '', NULL, NULL),
(7, '', '', '', 'fgjf', 'fdsf', 0, '$2y$10$FsDO6mb8aAI.r6.aRe/CUOLAwiF2Tf9yTToT4efDhF7r98KweqbiS', '2018-06-04 14:43:45', 1, '', NULL, NULL),
(8, '', '', '', 'meu@ua.pt', 'meu', 0, '$2y$10$NOgeqlKU36GQGoV9hnYDyuUXK1tkOFvcALdLNW8Dz9bgYLwR/fYMC', '2018-05-31 18:45:29', 1, '', NULL, NULL),
(10, '', '', '', 'ritagpinho1@ua.pt', 'rita1', 0, '$2y$10$lgw2CsdjQXC8HhGK7pYat.IhdzKZ49kcNtc4b.2F38d6JIL2c8YBK', '2018-06-04 19:38:14', 1, '', NULL, NULL),
(23, '', '', '', 'aso1@io.pt', 'asasas', 0, '$2y$10$yt1V4NCSZqDOchG2kgN8u..uDhK.Jv3zAEngEkTEibOX2vzD6tifW', '2018-06-04 19:35:57', 1, '', NULL, NULL),
(24, '', '', '', 'aso2@io.pt', 'asasas1', 0, '$2y$10$.UENLQo3B5LLa2Mvs3G6p.45C4OQXEVKD7YTbrtBKGEQdlHZW2ggi', '2018-06-04 19:36:52', 1, '', NULL, NULL),
(25, '', '', '', 'aso3@io.pt', 'asasasd', 0, '$2y$10$f59LNGK5fqgfB62tlUJpXO6vsebfYsjkFMjXus71vGKCOmwK4G0Zm', '2018-06-04 19:36:13', 1, '', NULL, NULL),
(26, '', '', '', 'aso@io.pt', 'asasasds', 0, '$2y$10$jbqXJjI6KuhnL7kUPuAyH..zlXAgl9l9qAcjErIe2QQ9V6JUriXxG', '2018-05-31 19:33:22', 1, '', NULL, NULL),
(27, '', '', '', 'wq@mdkos.ds', 'fewertyutrewq', 0, '$2y$10$EgGdtdol.qPHT8ivNKLnzObDIT0evEpxHsCiuE.0OEHzFRW44TTVS', '2018-05-31 19:35:53', 1, '', NULL, NULL),
(28, '', '', '', 'dew@jioew.cw', 'dew', 0, '$2y$10$7KA853sADHK7cRBufOmbJO/ml1gVfuboS9g//JGCtN3NYG0S4SZIS', '2018-05-31 19:36:47', 1, '', NULL, NULL),
(30, '', '', '', 'rc123@mailo.come', 'ricardo', 0, '$2y$10$04ybGqumhczUo0seoL9vaO31YZoOQVTNw.cv7ZAaH6snTPslCcPLe', '2018-05-31 20:20:46', 1, '', NULL, NULL),
(31, '', '', '', 'va@la.pr', 'vá lá', 0, '$2y$10$WhJFwMiADYUxDxmta9vlc.4te8./9XYonF/C.wN8kqneRvY5gd5sq', '2018-05-31 20:32:25', 1, '', NULL, NULL),
(32, '', '', '', '2@kjnaed', 'valu_teste2', 0, '$2y$10$TZAJRsjXin.6bignKa6MQ.wgSvUG9ujp2Cwgz5P91fr55AFuuppHS', '2018-06-04 19:37:14', 1, '10376176515b1261aa7b4d33.93195239WIN_20180118_04_19_57_Pro.jpg', NULL, NULL),
(33, '', '', '', '1@kjnaed', 'valu_teste', 0, '$2y$10$7GJ8tdCwjPmxq7gdt97hXOfnhaU41lNW1NTPtqDvltexlNAXfWdsa', '2018-06-02 09:21:26', 1, '10376176515b1261aa7b4d33.93195239WIN_20180118_04_19_57_Pro.jpg', NULL, NULL),
(34, '', '', '', 'rc1@rc.rc', 'rc3', 1, '$2y$10$ZJUyf2BG68D5Me.q5GCSru22RvRnCHhvZ60UGD5emAHJjAmC7rncS', '2018-06-04 21:43:50', 2, '', NULL, NULL),
(35, '', '', '', 'rmsc@123.com', 'r', 0, '$2y$10$PrKuyPjYRsmChSlgsd5zkuV6k3atQ3XF9sUxaZlNS6TO8YXHwurfC', '2018-06-01 19:58:00', 1, '', NULL, NULL),
(36, '', '', '', 'rmsc2@123.com', 'rar', 0, '$2y$10$YWwbeIzogTlK3nyn7Iol3O3oBHZI5YlSSIqLkxlI6syRESSLV5UW6', '2018-06-04 19:39:21', 1, '', NULL, NULL),
(37, '', '', '', 'por@favor.reza', 'POR FAVOR', 0, '$2y$10$sYGobeLsb0rM0/CwmX4ae.Tk.0Ls8wyeNJ94yyAH7TMDCRE1EY.3a', '2018-06-01 20:19:21', 1, '', NULL, NULL),
(38, '', '', '', 'rmsc1@123.com', 'rars', 0, '$2y$10$yz6.oGE60GGDld8FytTOAe4fAtIVbb1AqTD7894.XFd1YiwGMwdwa', '2018-06-04 19:39:16', 1, '', NULL, NULL),
(39, '', '', '', 'fgrtttt@gmail.com', 'xxxxxxxxxxxx', 0, '$2y$10$gwMoy5y6fKVsWocGrVaIV.pOlpTl/SsMF4/nPktSlbtvxOwoNYYO.', '2018-06-01 20:29:07', 1, '', NULL, NULL),
(40, '', '', '', 'nuser@mail.com', 'user', 0, '$2y$10$lknK0NW.WxuTx.5jddrDEeCDpRY7nwpDWS2H29oRGi4F0E/ROqnQ6', '2018-06-12 13:08:42', 1, '', 'oioioi', 'oioioioi'),
(41, '', '', '', 'sdjfjds@fasdfds.com', 'username', 0, '$2y$10$01rRcwbY81vNQzeWxi8l2eCqufWowosVK9GMJX3cDlsgrZIEAbn4y', '2018-06-01 20:31:52', 1, '', NULL, NULL),
(42, '', '', '', 'qewqww@ht.yjghf', 'eqwww', 0, '$2y$10$Mg1dkSLPzWKdEgRQwNnYdu7zvphrSgiGSaAeA5uczuxHo9jYaygPi', '2018-06-01 20:33:35', 1, '', NULL, NULL),
(43, '', '', '', 'cdddddds@effrtytu.yutytrew', 'ssssss', 0, '$2y$10$ESPOjtJJUPobvzqp6la3t.sfka3xC7wQs.AnsByu.NiBst5l.JwXy', '2018-06-01 20:34:56', 1, '', NULL, NULL),
(44, '', '', '', 'cw@cer.btr', 'jhgfd', 0, '$2y$10$zechQORhf.6v5RnplwxyKO7BB/i0e.gowpYHK/mPeKephucgomDUK', '2018-06-01 20:36:15', 1, '', NULL, NULL),
(45, '', '', '', 'cw1@cer.btr', 'jhgfdccccccce', 0, '$2y$10$z98AcWgZ/tJmuiq5uS.Q1OAYTuTo/CdaX7nNSPYwgCRh8rM8/Agvu', '2018-06-04 19:39:33', 1, '', NULL, NULL),
(46, '', '', '', 'teste2@teste.pt', 'teste2', 0, '$2y$10$nJ7WCNt714dydI5rM9Z2NOdu8dm/nyjjqOcLV82opYjpccnNWvUXi', '2018-06-04 19:38:36', 1, '', NULL, NULL),
(47, '', '', '', 'teste3@teste.pt', 'teste3', 0, '$2y$10$YGZMu8kZwPutqttkhxLb8u8C.2l74vVgf0DWX2bYV0h5za3wzsnxW', '2018-06-04 19:38:40', 1, '', NULL, NULL),
(48, '', '', '', 'teste4@teste.pt', 'teste4', 0, '$2y$10$9szHYbCV3/pcfig23bvtPemgXg48Nk6LN8jzCVOBd/d8dcps6Hz.2', '2018-06-04 19:38:44', 1, '', NULL, NULL),
(49, '', '', '', 'teste5@teste.pt', 'teste5', 0, '$2y$10$UVXwla7UkjXsJ/kAUVpqWuuVkhrrb9iehWI2BrWlGOoz8LBaviMi2', '2018-06-04 19:38:47', 1, '', NULL, NULL),
(50, '', '', '', 'teste6@teste.pt', 'teste6', 0, '$2y$10$MkleN02saw.L7VkVCsxMbeGOyey890QJ4WivEcqvonA2Z1gcdIieu', '2018-06-04 19:38:50', 1, '', NULL, NULL),
(51, 'Ricardo', 'Coelho', '1993-08-18', 'ricardo.coelho@ua.pt', 'ricardocoelho14', 1, '$2y$10$ZHnqYMJJCE2kaFXRkKDP/ebKkEV/hcjHnZSMIEM1FjNq6znIKW0/u', '2018-06-12 10:37:11', 2, '', 'jhgfds', 'sdfghj'),
(52, '', '', '', 'jaios@jioacs.sac', 'axiojxioaj', 0, '$2y$10$CMyO0WjPCm3F23X6Twy/Hu5Xne0vWQDIk9cvVqXqFWhwMHvyzXvmG', '2018-06-01 21:03:17', 1, '', NULL, NULL),
(53, '', '', '', 'ritagpinho2@ua.pt', 'Rita', 0, '$2y$10$fbEBCdhkD83YQDHQpXJt/.uZjiTPss2vLcxHDvObdQW8tf7c5ecpO', '2018-06-04 19:40:14', 1, '', NULL, NULL),
(54, '', '', '0', 'js@uadsi.few', 'finalmente', 1, '$2y$10$X6d36yp1HCo34SdmJ4UOJ.ibxK47ZFMHyet6H2ZdOk7n8TOH.VUpa', '2018-06-04 20:59:21', 1, '', NULL, NULL),
(55, 'Uno Dos', 'Tres Cuatro', '1969-12-31', 'uno@dos.tres', 'unodos', 1, '$2y$10$nb627GkAQubnTq7f4RBvQ.bIusq1DkF.j7zTaXbsZUv0yQBNgFK5C', '2018-06-04 21:55:46', 1, '20572462055b15b5627695a9.74417334u2hmw7uvcy0z.jpg', NULL, NULL),
(56, '', '', '', 'TESTE1@TESTE.TESTE', 'TESTE_', 0, '$2y$10$8SxQTCobk73sVozxDFv8CemGlIAIpLZOOquchtXRsY/nYY6kZ8R7u', '2018-06-04 19:37:32', 1, '', NULL, NULL),
(57, '', '', '', 'jiojio@e.de', 'cdq', 1, '$2y$10$iMGv5B5/GlqmCpJa2gItPOSRe1AFUQ7qRlzlF8LKPfeOSl3Pb7JT.', '2018-06-04 20:59:38', 1, '', NULL, NULL),
(58, 'ndiao', '', 'na', 'noi', 'nionads', 0, 'ndisoa', '2018-06-01 22:59:36', 1, '', NULL, NULL),
(59, 'dsa', '', '2018-06-01', 'dasssss@j.po', 'dsad', 1, '$2y$10$qaIovdDcY0OT387iBvlGWeXktyOiiVO0aPqjgJnddeMZHCncyKdLm', '2018-06-04 21:01:21', 1, '', NULL, NULL),
(60, 's', 'd', 'd', 's', 's', 0, 's', '2018-06-01 23:09:55', 1, '', NULL, NULL),
(61, 'dgs', 'gds', '2018-06-06', 'gds@jh.ydsa', 'dcsgsdddddd', 1, '$2y$10$hiGS1rV2wTFMdisTQZwqA.9cUMYTUdW6agvCR.QJkCOjWhoZDn5gW', '2018-06-04 21:07:19', 1, '', NULL, NULL),
(62, 'benfica', 'benfica', '2018-05-31', 'benfica@slb.pt', 'sssslllllbbbb', 0, '$2y$10$3uZ2Y01I1jnJ2o1MhfBOouOmKE1l7mU0YCanQDw9OQues91v/qrhG', '2018-06-03 13:11:41', 1, NULL, NULL, NULL),
(63, 'User', 'Temp', '1999-09-09', 'user@temp.pt', 'usertemp', 0, '$2y$10$.mLR/aeqI/cvjy3JT7pOuuzEQGlQ.tzFvBm2K24DxhKt0wuWqPrq2', '2018-06-03 13:38:38', 1, NULL, NULL, NULL),
(64, 'xsa', 'xx', '2018-06-27', 'ritagpinho3@ua.pt', 'as', 0, '$2y$10$KFMQpuKl2b2XYst/CTuQdO2G21B9vjcboDUMao3q3z99aMr6yWVy.', '2018-06-04 19:40:18', 1, NULL, NULL, NULL),
(65, 'assa', 'assaa', '2018-06-06', 'ritagpinho@ua.pt', 'saaaaaa', 0, '$2y$10$ZArgEWmmm/sD.sUpew1de.WFavXVn6cjn4v7U9oJeLWNY/fgw3nlS', '2018-06-12 13:20:47', 1, NULL, 'dccw', 'dwdws'),
(66, 'ajdsoi', 'asoidjo', '2018-06-02', 'jadsoi@jiods.cds', 'BOA SORTE', 0, '$2y$10$1y.iYCRRFhglUv2MMnS5eOuabZKBvF4CbeCaXgho5jmEdp4cPnvxm', '2018-06-03 20:40:48', 1, NULL, NULL, NULL),
(67, 'asd', 'asd', '2018-05-28', 'asd@asd.asd', 'isto funciona', 0, '$2y$10$fc1xou9KmpCLnDYOWubYBeXFdAoDQ5wz09pBkQTkbjw3atleDWrce', '2018-06-03 21:10:07', 1, NULL, NULL, NULL),
(68, 'Beatriz', 'Marques', '1996', 'be@gmail.com', 'bea_NAO', 0, '$2y$10$GUkuZ9mUfJ7ubpwX1ehHvuWQAwWsk3xw9ToKXORZ57gV5v8f5HtSi', '2018-06-04 15:55:51', 1, '', NULL, NULL),
(69, 'Beatriz', 'Marques', '1996-11-08', 'bea@gmail.com', 'bea', 1, '$2y$10$mluRPsgS2N9UDEu3aHjiJ.PpWWUagdr81B/KoJoYlnwYCDEAMRpVK', '2018-06-12 09:30:30', 2, '15362682015b15af8ba888a0.78715859beaaa.jpg', NULL, NULL),
(70, 'fsdf', 'fddfs', 'fdfsfds', 'fafds', 'fdds', 1, 'afdf', '2018-06-04 10:20:37', 1, NULL, NULL, NULL),
(71, 'SIHDO', 'SDIHO', '2018-06-04', 'SOIH@jdas.das', 'dasihoqasQ', 1, '$2y$10$CZCBiOYm0xbwwPT0lAyAp.mjjPahL4q0J68n4e8uYB6SoG5T/R0Fm', '2018-06-04 10:21:50', 1, NULL, NULL, NULL),
(72, 'a', 'a', '2018-06-06', 'a@s.com', 'a', 1, '$2y$10$pIF/On37JWXjxiccvW3s.e7ZCaCK5rJpgfKSQ4zW3zZDpzACA8hsu', '2018-06-04 10:22:00', 1, NULL, NULL, NULL),
(73, 'Marcelo', 'Bettencourt', '1991-07-02', 'marcelobettencourt@ua.pt', 'marcelo', 1, '$2y$10$d49MhyoDY9Lv6oMfsPWEXuJ/ropaHAbvy2TqNGLt8Ku3g.NLMVt1K', '2018-06-04 10:22:41', 1, NULL, NULL, NULL),
(74, 'Inês', 'Silva', '1996-11-03', 'ines@gmail.com', 'Inês', 1, '$2y$10$gfo0DZtNEepmTww31ztk8O8sWp6EgrqgLnR3cjNlEkY7gX314X9fC', '2018-06-04 10:22:43', 1, NULL, NULL, NULL),
(75, 'joana', 'joana', '1111-11-11', 'valu@gmail.com', 'joana', 1, '$2y$10$CE5Dl29iB69y0jbXqU2Kne7RtqDwW.j.x/Y33jHoJPS3El.c4ZoeO', '2018-06-04 18:14:30', 1, NULL, NULL, NULL),
(76, 'r', 'c', '2018-06-04', 'rc1@mail.com', 'rc1', 1, '$2y$10$8rLRDaYJRElHWv9IdstmGuIX8BI2Khuga7TrFXcLo17MkN68aZS6a', '2018-06-04 19:38:25', 1, NULL, NULL, NULL),
(77, 'r', 'c', '2018-06-04', 'rc2@mail.com', 'rc2', 1, '$2y$10$2NZ.7tL/2Wtovq0ta70s/.xyLYxrSV1vNtrPo8ElQJ/lSYhN0AVt6', '2018-06-04 19:38:19', 1, NULL, NULL, NULL),
(78, 'r', 'c', '2018-05-29', 'rc@cr.rc', 'rc', 1, '$2y$10$6HKNcYRO1.bVvnojdr13i.pks4dLISuKvBN41IF2uV.kLHbxgPoH6', '2018-06-04 19:04:59', 1, NULL, NULL, NULL),
(79, 'ric', 'ms', '2018-06-04', 'rc_lolxd@mail.com', 'rc123', 1, '$2y$10$IsC5WYkj6IkNxNPtfE80ruuiLnK1r.CPeJEC.zQdRgbCagNVu/tHy', '2018-06-04 21:22:38', 1, NULL, NULL, NULL),
(81, 'try', 'try', '2018-06-12', 'try@gmail.com', 'try', 1, '$2y$10$tStS1PFcggKattgdK8oBAOUWuRuXZfDolOpnKdoaWZRL.BDx3wZcG', '2018-06-04 21:24:32', 1, NULL, NULL, NULL),
(82, 'admin', 'Admin', '2222-06-04', 'Admin.admin@admin.com', 'admin', 1, '$2y$10$mvlZsksM.SjcZnZ70NKQdePSjxyqSa7QKk1LqngoSABDnGlZu6ZIK', '2018-06-12 16:27:59', 2, '2756055595b1ff4a9a86782.19056201WIN_20171107_19_46_24_Pro.jpg', NULL, NULL),
(83, 'normal', 'normal', '2018-06-04', 'normal.normal@normal.com', 'normal', 1, '$2y$10$qMNtODU3ypHZZGoI9jQeWeAZLHCTlUxu5jYeCVe9A2xII8670KdXW', '2018-06-04 21:38:36', 1, '12654647985b15b15c28bf31.8803826118620467_10211307940567436_2326635236480565696_n.jpg', NULL, NULL),
(84, 'jfsdiosdjio', 'fjsdioijfdso', '2018-06-01', 'fqjdossido@jifsdoji.sfdijo', 'desespero', 1, '$2y$10$TfxmUaNAkGiF4X2aDEFQWOIJljwEhKM1nCiEBVPoof/HorZhwiGNi', '2018-06-04 22:00:25', 1, NULL, NULL, NULL),
(85, 'ricardo', 'coelho', '2018-06-04', 'ricardinho@ua.pt', 'ricardinho', 1, '$2y$10$Q92PITYnSQqm8RbmQMwTfOnEF9UlKk6sTX7PZF7DFfSJ0U1O2Dude', '2018-06-04 22:19:17', 1, '12670116885b15bae50987a5.84951939u2hmw7uvcy0z.jpg', NULL, NULL),
(86, 'Aquele', 'User', '2018-06-01', 'user@email.com', 'userteste', 1, '$2y$10$PQn0V3SJPZohtxo0iagb5evZRuMcRXQFNtI1vpyV0w1BlKEE.m3dC', '2018-06-04 23:23:48', 1, NULL, NULL, NULL),
(87, 'teste', 'teste', '1999-10-10', 'teste@teste', 'teste', 1, '$2y$10$B0rPY1f38CaCTUaGRTvsk.KRrNgL2pvgM2fT/qXfFr1K4hvI7ihHG', '2018-06-05 13:01:08', 2, NULL, NULL, NULL),
(88, 'João', 'Pinho', '2018-06-06', 'joaogpinho123@gmail.com', 'João', 1, '$2y$10$0cuxpCX3L3QbC8TJ41TGD.auLHoWXi.Qfzu2/aq4jrjcAU5Abafs.', '2018-06-12 16:11:53', 1, NULL, 'mQW6u9Vr', 'B5sIesn8');

-- --------------------------------------------------------

--
-- Estrutura da tabela `user_public`
--

CREATE TABLE `user_public` (
  `ref_id_users_geral` int(11) NOT NULL,
  `ref_id_pais` int(11) NOT NULL,
  `ref_id_profissao` int(11) NOT NULL,
  `ref_id_escolaridade` int(11) NOT NULL,
  `pontos_disponiveis` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `user_public`
--

INSERT INTO `user_public` (`ref_id_users_geral`, `ref_id_pais`, `ref_id_profissao`, `ref_id_escolaridade`, `pontos_disponiveis`) VALUES
(1, 1, 1, 1, 0),
(10, 193, 3, 4, 0),
(34, 1, 1, 1, 0),
(51, 193, 2, 4, 0),
(52, 1, 1, 1, 0),
(54, 1, 1, 1, 0),
(55, 115, 3, 7, 0),
(56, 17, 4, 4, 0),
(57, 1, 1, 1, 0),
(59, 1, 1, 1, 0),
(61, 1, 1, 1, 0),
(62, 1, 1, 1, 0),
(63, 8, 5, 7, 0),
(64, 1, 1, 1, 0),
(65, 1, 1, 1, 0),
(66, 1, 1, 1, 0),
(67, 1, 1, 1, 0),
(68, 193, 2, 5, 0),
(69, 193, 2, 5, 0),
(71, 1, 1, 1, 0),
(72, 19, 2, 5, 0),
(73, 193, 2, 4, 0),
(74, 33, 2, 6, 0),
(75, 1, 1, 1, 0),
(76, 1, 1, 1, 0),
(78, 1, 1, 1, 0),
(79, 44, 6, 7, 0),
(81, 246, 1, 1, 0),
(82, 193, 3, 5, 0),
(83, 193, 2, 5, 0),
(84, 1, 1, 1, 0),
(85, 1, 1, 1, 0),
(86, 145, 3, 3, 0),
(87, 2, 6, 6, 0),
(88, 1, 1, 1, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bilhetes`
--
ALTER TABLE `bilhetes`
  ADD PRIMARY KEY (`id_bilhetes`),
  ADD KEY `fk_bilhetes_user_public1_idx` (`ref_id_user_geral`),
  ADD KEY `fk_bilhetes_eventos_horarios1_idx` (`ref_id_eventos_horarios`);

--
-- Indexes for table `escolaridade`
--
ALTER TABLE `escolaridade`
  ADD PRIMARY KEY (`id_escolaridade`);

--
-- Indexes for table `eventos`
--
ALTER TABLE `eventos`
  ADD PRIMARY KEY (`id_eventos`),
  ADD KEY `fk_eventos_users_geral1_idx` (`ref_id_users_admin`),
  ADD KEY `fk_eventos_eventos_tipo1_idx` (`ref_id_eventos_tipo`);

--
-- Indexes for table `eventos_horarios`
--
ALTER TABLE `eventos_horarios`
  ADD PRIMARY KEY (`id_eventos_horarios`),
  ADD KEY `fk_eventos_horarios_eventos1_idx` (`ref_id_eventos`),
  ADD KEY `fk_eventos_horarios_users_geral1_idx` (`ref_id_users_admin`);

--
-- Indexes for table `eventos_tipo`
--
ALTER TABLE `eventos_tipo`
  ADD PRIMARY KEY (`id_eventos_tipo`);

--
-- Indexes for table `pais`
--
ALTER TABLE `pais`
  ADD PRIMARY KEY (`id_pais`);

--
-- Indexes for table `pontos_gastos`
--
ALTER TABLE `pontos_gastos`
  ADD PRIMARY KEY (`id_pontos_gastos`),
  ADD KEY `fk_pontos_gastos_user_public1_idx` (`ref_id_users_geral`),
  ADD KEY `fk_pontos_gastos_users_geral1_idx` (`ref_id_users_admin`),
  ADD KEY `fk_pontos_gastos_bilhetes1_idx` (`ref_id_bilhetes`);

--
-- Indexes for table `profissao`
--
ALTER TABLE `profissao`
  ADD PRIMARY KEY (`id_profissao`);

--
-- Indexes for table `recupera_pass`
--
ALTER TABLE `recupera_pass`
  ADD PRIMARY KEY (`id_recupera`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id_reviews`),
  ADD KEY `fk_reviews_users_geral1_idx` (`ref_id_users_geral_admin`),
  ADD KEY `fk_reviews_users_geral2_idx` (`ref_id_users_geral_geral`),
  ADD KEY `fk_reviews_eventos_horarios1_idx` (`ref_id_eventos_horarios`);

--
-- Indexes for table `salas`
--
ALTER TABLE `salas`
  ADD PRIMARY KEY (`id_salas`),
  ADD KEY `fk_salas_users_geral1_idx` (`ref_id_users_admin`);

--
-- Indexes for table `salas_has_eventos`
--
ALTER TABLE `salas_has_eventos`
  ADD PRIMARY KEY (`id_salas_has_eventos`),
  ADD KEY `fk_salas_has_eventos_eventos1_idx` (`ref_id_eventos`),
  ADD KEY `fk_salas_has_eventos_salas1_idx` (`ref_id_salas`);

--
-- Indexes for table `tipo`
--
ALTER TABLE `tipo`
  ADD PRIMARY KEY (`id_tipo`);

--
-- Indexes for table `users_geral`
--
ALTER TABLE `users_geral`
  ADD PRIMARY KEY (`id_users_geral`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `fk_users_geral_tipo1_idx` (`ref_id_tipo`);

--
-- Indexes for table `user_public`
--
ALTER TABLE `user_public`
  ADD PRIMARY KEY (`ref_id_users_geral`),
  ADD KEY `fk_user_public_pais_idx` (`ref_id_pais`),
  ADD KEY `fk_user_public_profissao1_idx` (`ref_id_profissao`),
  ADD KEY `fk_user_public_escolaridade1_idx` (`ref_id_escolaridade`),
  ADD KEY `fk_user_public_users_geral1_idx` (`ref_id_users_geral`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bilhetes`
--
ALTER TABLE `bilhetes`
  MODIFY `id_bilhetes` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `escolaridade`
--
ALTER TABLE `escolaridade`
  MODIFY `id_escolaridade` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `eventos`
--
ALTER TABLE `eventos`
  MODIFY `id_eventos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `eventos_horarios`
--
ALTER TABLE `eventos_horarios`
  MODIFY `id_eventos_horarios` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `eventos_tipo`
--
ALTER TABLE `eventos_tipo`
  MODIFY `id_eventos_tipo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `pais`
--
ALTER TABLE `pais`
  MODIFY `id_pais` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=253;

--
-- AUTO_INCREMENT for table `pontos_gastos`
--
ALTER TABLE `pontos_gastos`
  MODIFY `id_pontos_gastos` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `profissao`
--
ALTER TABLE `profissao`
  MODIFY `id_profissao` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `recupera_pass`
--
ALTER TABLE `recupera_pass`
  MODIFY `id_recupera` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id_reviews` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `salas`
--
ALTER TABLE `salas`
  MODIFY `id_salas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `salas_has_eventos`
--
ALTER TABLE `salas_has_eventos`
  MODIFY `id_salas_has_eventos` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tipo`
--
ALTER TABLE `tipo`
  MODIFY `id_tipo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users_geral`
--
ALTER TABLE `users_geral`
  MODIFY `id_users_geral` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT for table `user_public`
--
ALTER TABLE `user_public`
  MODIFY `ref_id_users_geral` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
