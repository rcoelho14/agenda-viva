<!DOCTYPE html>
<head>

    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>AGENDA VIVA - Admin</title>
    <meta name="generator" content="Bootply"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <?php include_once 'helpers/css.php'; ?>
</head>
<body>
<?php include_once 'components/navbar_top.php'; ?>


<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3">
            <!-- Left column -->
            <?php include_once 'components/navbar_left.php'; ?>
            <!-- /col-3 -->
            <div class="col-sm-9">

                <!-- column 2 -->

                <div class="row">
                    <!-- center left-->
                    <div class="col-md-10">

                        <hr>
                        <div id="page-wrapper">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h1 class="page-header">Gestão de Eventos</h1>
                                </div>
                                <!-- /.col-lg-12 -->
                            </div>
                            <!-- /.row -->
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Edição dos eventos
                                        </div>
                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <table class="table table-striped">
                                                    <?php
                                                    if (isset($_GET["id"])) {
                                                        $id_eventos = $_GET["id"];
                                                        // We need the function!
                                                        require_once "connections/sql_connection.php";


                                                        // Create a new DB connection
                                                        $link = new_db_connection();

                                                        /* create a prepared statement */
                                                        $stmt = mysqli_stmt_init($link);

                                                        $query = "SELECT id_eventos, eventos.nome, eventos.imagem, eventos.description_short, eventos.description, eventos.lotacao, eventos.preco, eventos.date_creation,  eventos_tipo.id_eventos_tipo, eventos.area, eventos.idade FROM eventos INNER JOIN eventos_tipo ON id_eventos_tipo = ref_id_eventos_tipo WHERE id_eventos = ?";

                                                        if (mysqli_stmt_prepare($stmt, $query)) {

                                                            mysqli_stmt_bind_param($stmt, 'i', $id_eventos);

                                                            /* execute the prepared statement */
                                                            mysqli_stmt_execute($stmt);

                                                            /* bind result variables */
                                                            mysqli_stmt_bind_result($stmt, $id, $nome, $imagem, $short, $long, $lotacao, $preco, $data_cre, $tipo_eventos, $area, $idade);
                                                            /* fetch values */
                                                            while (mysqli_stmt_fetch($stmt)) {
                                                                echo '<form role="form" method="post" action="scripts/eventos_update.php" enctype="multipart/form-data">
                                                <input type="hidden" name="id_eventos" value="' . $id_eventos . '">
                                                <div class="form-group">
                                                    <label>ID do evento</label>
                                                    <p class="form-control-static">' . $id_eventos . '</p>
                                                </div>
                                                <div class="form-group">
                                                    <label>Data de modificação</label>
                                                    <p class="form-control-static">' . $data_cre . '</p>
                                                </div>
                                                <div class="form-group">
                                                    <label>Área de interesse</label>
                                                    <select name="interesse" class="form-control" id="exampleSelect1">';
            if ($area == 1) {
                echo'<option value="1" selected>Matemática</option>
            <option value="2">Arte</option>
            <option value="3">Física</option>
            <option value="4">Robótica</option>
            <option value="5">Ciências</option>';
            } if ($area == 2) {
                                                                    echo'<option value="1" >Matemática</option>
            <option value="2" selected>Arte</option>
            <option value="3">Física</option>
            <option value="4">Robótica</option>
            <option value="5">Ciências</option>';
                                                                }
                                                                if ($area==3) {
                                                                    echo'<option value="1" >Matemática</option>
            <option value="2" selected>Arte</option>
            <option value="3">Física</option>
            <option value="4">Robótica</option>
            <option value="5">Ciências</option>';
                                                                }if ($area==4) {
                                                                    echo'<option value="1" >Matemática</option>
            <option value="2" >Arte</option>
            <option value="3" selected>Física</option>
            <option value="4">Robótica</option>
            <option value="5">Ciências</option>';
                                                                }if ($area==5) {
                                                                    echo'<option value="1" >Matemática</option>
            <option value="2" >Arte</option>
            <option value="3">Física</option>
            <option value="4" selected>Robótica</option>
            <option value="5">Ciências</option>';
                                                                }

                                                                echo'
        
            
        </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Escalão</label>
                                                    <select name="idade" class="form-control" id="exampleSelect1">';

                                                                if ($idade == 1) {
                                                                    echo'
                <option value="1" selected>Crianças</option>
                <option value="2">Jovens</option>
                <option value="3">Adultos</option>';
                                                                } if ($idade == 2) {
                                                                    echo'
                <option value="1" >Crianças</option>
                <option value="2" selected>Jovens</option>
                <option value="3">Adultos</option>';
                                                                }
                                                                if ($idade==3) {
                                                                    echo'
                <option value="1" >Crianças</option>
                <option value="2" >Jovens</option>
                <option value="3" selected>Adultos</option>';
                                                                }

            echo'
            
            </select>
                                                    </div>
                                                <div class="form-group">
                                                    <label>Nome</label>
                                                    <input class="form-control" name="nome_eventos"
                                                           value="' . $nome . '">
                                                </div>
                                                <div class="form-group">
                                                        <label>Imagem do Evento</label>
                                                        <img height="60"  src="images/eventos' . $imagem . '" class="avatar img-fluid img-responsive" alt="avatar" >
                                                    </div>
                                                    <h6>Upload a different photo...</h6>
                                                    <div class="form-group">
                                                        <input class="form-control" type="file" id="fileToUpload"
                                                               name="fileToUpload">
                                                    </div>';

                                                                if (isset($_SESSION["imagem_nao"])) {
                                                                    if ($_SESSION["imagem_nao"] == 23) {
                                                                        echo "<div class='alert-danger'>
                <strong>Erro!</strong> Formato de imagem não suportado!
          </div>";
                                                                        $_SESSION["imagem_nao"] = 12;
                                                                    }
                                                                }
                                                                if (isset($_SESSION["imagem_grande"])) {
                                                                    if ($_SESSION["imagem_grande"] == 22) {
                                                                        echo "<div class='alert-danger'>
                <strong>Erro!</strong> Formato de imagem não suportado!
          </div>";
                                                                        $_SESSION["imagem_grande"] = 12;
                                                                    }
                                                                }

                                                                echo '
                                               
                                                <div class="form-group">
                                                    <label>Descrição Curta</label>
                                                    <input class="form-control" name="desc_curta_eventos" value="' . $short . '">
                                                    </div>
                                                    <div class="form-group">
                                                    <label>Descrição Longa</label>
                                                    <br>
                                                    <textarea cols="108" rows="8" name="desc_longa_eventos">' . $long . '</textarea>
                                                    </div>
                                                    <div class="form-group">
                                                    <label>Lotação</label>
                                                    <input class="form-control" name="lotacao_eventos" value="' . $lotacao . '">
                                                    </div>';
                                                                if (isset($_SESSION["lotacao"])) {
                                                                    if ($_SESSION["lotacao"] == 20) {
                                                                        echo "<div class='alert-danger'>
                <strong>Erro!</strong> Lotação inválida!
          </div>";
                                                                        $_SESSION["lotacao"] = 12;
                                                                    }
                                                                }
                                                                echo '
                                                   
                                                    <div class="form-group">
                                                    <label>Preço</label>
                                                    <input class="form-control" name="preco_eventos" value="' . $preco . '">
                                                    </div>';
                                                                if (isset($_SESSION["preco"])) {
                                                                    if ($_SESSION["preco"] == 21) {
                                                                        echo "<div class='alert-danger'>
                <strong>Erro!</strong> Preço inválido!
          </div>";
                                                                        $_SESSION["preco"] = 12;
                                                                    }
                                                                }
                                                                echo '
                                                     <div class="form-group">
                                                    <label>Tipo de Evento</label>
                                                    <select class="form-control" name="id_tipo">';
                                                                if ($tipo_eventos == "1") {
                                                                    echo '<option value="1" selected>Workshop</option>
<option value="2" >Atividade</option>
<option value="3" >Exposição</option>
<option value="4" >Sala</option>;';
                                                                } elseif ($tipo_eventos == "2") {
                                                                    echo '<option value="1">Workshop</option>
<option value="2" selected>Atividade</option>
<option value="3" >Exposição</option>
<option value="4" >Sala</option>;';
                                                                } elseif ($tipo_eventos == "3") {
                                                                    echo '<option value="1">Workshop</option>
<option value="2">Atividade</option>
<option value="3"  selected>Exposição</option>
<option value="4" >Sala</option>;';
                                                                } elseif ($tipo_eventos == "4") {
                                                                    echo '<option value="1">Workshop</option>
<option value="2">Atividade</option>
<option value="3" >Exposição</option>
<option value="4" selected>Sala</option>;';
                                                                }
                                                                echo '</select >
                                                    
                                               
                                                    </div>
                                                </div>
                                                </div >
                                                <button type = "submit" class="btn btn-default" > Submeter alterações
            </button >
 </form >';
                                                            }

                                                            /* close statement */
                                                            mysqli_stmt_close($stmt);
                                                        }

                                                        /* close connection */
                                                        mysqli_close($link);
                                                    }
                                                    ?>
                                                    <br>
                                                    <div class="panel-body">
                                                        <div class="table-responsive">
                                                            <table class="table table-striped">
                                                                <thead>

                                                                </thead>
                                                                <tbody>


                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <!-- /.table-responsive -->
                                                    </div>
                                                    <!-- /.panel-body -->
                                            </div>
                                            <!-- /.panel -->
                                        </div>

                                    </div>
                                    <!-- /.row -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</body>