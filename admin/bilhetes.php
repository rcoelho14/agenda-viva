    <?php 
    require_once 'connections/sql_connection.php'; 

    $link = new_db_connection();

    $query="SELECT `bilhetes`.*, `eventos`.`nome`, `eventos_horarios`.`hora_inicio`
    FROM `eventos_horarios`
    LEFT JOIN `bilhetes` ON `bilhetes`.`ref_id_eventos_horarios` = `eventos_horarios`.`id_eventos_horarios`
    LEFT JOIN `eventos` ON `eventos_horarios`.`ref_id_eventos` = `eventos`.`id_eventos`";

    $stmt = mysqli_stmt_init($link);
    mysqli_stmt_prepare($stmt, $query);;
    mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt, $id_bilhetes, $numero_bilhetes, $date_creation, $pago, $finalizado, $id_users_geral, $eventos_horarios, $date_pago, $date_finalizado, $nome_evento,$hora_inicio );
    mysqli_stmt_execute($stmt); 
    



    ?>
    <!DOCTYPE html>



    <head>

        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>AGENDA VIVA - Admin</title>
        <meta name="generator" content="Bootply" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <?php include_once 'helpers/css.php'; ?>
    </head>
    <body>
        <?php include_once 'components/navbar_top.php'; ?>



        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-3">
                    <!-- Left column -->
                    <?php include_once 'components/navbar_left.php'; ?>
                    <!-- /col-3 -->
                    <div class="col-sm-9">

                        <!-- column 2 -->

                        <div class="row">
                            <!-- center left-->
                            <div class="col-md-10">

                                <hr>
                                <div id="page-wrapper">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <h1 class="page-header">Bilhetes</h1>
                                        </div>
                                        <!-- /.col-lg-12 -->
                                    </div>
                                    <!-- /.row -->
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">

                                                </div>
                                                <!-- /.panel-heading -->

                                                <div class="panel-body">
                                                    <div class="table-responsive">
                                                        <table class="table table-striped">
                                                            <thead>
                                                                <tr>
                                                                    
                                                                    <th>ID</a></th>
                                                                    <th>Número de Bilhetes</a></th>
                                                                    <th>Utilizador</a></th>
                                                                    <th>Data de pagamento</a></th>
                                                                    <th>Nome da sala</a></th>
                                                                    <th>Hora de início</a></th>

                                                                    <th>Operações</th>
                                                                </tr>
                                                               <?php while (mysqli_stmt_fetch($stmt)) {
                                                          echo      "<tr>
                                                                     <td>$id_bilhetes</td>
                                                                      <td>$numero_bilhetes</td>
                                                                      <td>$id_users_geral</td>
                                                                      <td>$date_pago</td>
                                                                      <td>$nome_evento</td>
                                                                      <td>$hora_inicio</td>
                                                                    </tr>"; 


                                                               }
                                                                        ?>

                                                            </thead>
                                                            <tbody>





                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <!-- /.table-responsive -->
                                                </div>

                                                <!-- /.panel-body -->
                                            </div>
                                            <!-- /.panel -->
                                        </div>

                                    </div>
                                    <!-- /.row -->
                                </div>



                                <?php include_once 'helpers/js.php'; ?>
                            </body>
                            </html>
