<?php

require_once 'connections/sql_connection.php';

$link = new_db_connection();


$query="SELECT 
        COUNT(id_users_geral)
        FROM users_geral";


$stmt = mysqli_stmt_init($link);
mysqli_stmt_prepare($stmt, $query);
mysqli_stmt_execute($stmt);
mysqli_stmt_bind_result($stmt, $count_users);
mysqli_stmt_execute($stmt);
mysqli_stmt_fetch($stmt);


$query="SELECT 
        COUNT(id_eventos)
        FROM eventos";


$stmt = mysqli_stmt_init($link);
mysqli_stmt_prepare($stmt, $query);
mysqli_stmt_execute($stmt);
mysqli_stmt_bind_result($stmt, $count_eventos);
mysqli_stmt_execute($stmt);
mysqli_stmt_fetch($stmt);


$query="SELECT 
        COUNT(id_reviews)
        FROM reviews";

$stmt = mysqli_stmt_init($link);
mysqli_stmt_prepare($stmt, $query);
mysqli_stmt_execute($stmt);
mysqli_stmt_bind_result($stmt, $count_reviews);
mysqli_stmt_execute($stmt);
mysqli_stmt_fetch($stmt);


$query="SELECT
        COUNT(id_bilhetes)
        FROM bilhetes";

$stmt = mysqli_stmt_init($link);
mysqli_stmt_prepare($stmt, $query);
mysqli_stmt_execute($stmt);
mysqli_stmt_bind_result($stmt, $count_bilhetes);
mysqli_stmt_execute($stmt);
mysqli_stmt_fetch($stmt);



?>


<div class="table-responsive">
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Users</th>
            <th>Eventos</th>
            <th>Reviews</th>
            <th>Bilhetes</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td><?= $count_users ?></td>
            <td><?= $count_eventos ?></td>
            <td><?= $count_reviews ?></td>
            <td><?= $count_bilhetes ?></td>
        </tr>

        </tbody>
    </table>
</div>
