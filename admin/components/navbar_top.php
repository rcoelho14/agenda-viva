<?php
session_start();
if (isset($_SESSION['username']) && $_SESSION['tipo']) {

    $username = $_SESSION['username'];
    $tipo = $_SESSION['tipo'];


    if ($tipo != 2) {
        header('Location: public/index.php?msg=4');
    }

}
else {
    header('Location: public/index.php?msg=5');
}

?>


<div id='top-nav' class='navbar navbar-inverse navbar-static-top'>
    <div class='container-fluid'>
        <div class='navbar-header'>
            <button type='button' class='navbar-toggle' data-toggle='collapse' data-target='.navbar-collapse'>
                <span class='icon-bar'></span>
                <span class='icon-bar'></span>
                <span class='icon-bar'></span>
            </button>
            <a class='navbar-brand' href='../public/index.php'>Agenda Viva</a>
        </div>
        <div class='navbar-collapse collapse'>
            <ul class='nav navbar-nav navbar-right'>
                <li><a href='../public/user_profile.php'><?= $username?></a></li>
                <li><a href='../public/components/logout.php'>Logout</a></li>
            </ul>
        </div>
    </div>
</div>
