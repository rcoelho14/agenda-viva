<?php
require_once 'connections/sql_connection.php'; ?>
<!DOCTYPE html>
<head>

    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>AGENDA VIVA - Admin</title>
    <meta name="generator" content="Bootply"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <?php include_once 'helpers/css.php'; ?>
</head>
<body>
<?php include_once 'components/navbar_top.php'; ?>


<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3">
            <!-- Left column -->
            <?php include_once 'components/navbar_left.php'; ?>
            <!-- /col-3 -->
            <div class="col-sm-9">

                <!-- column 2 -->

                <div class="row">
                    <!-- center left-->
                    <div class="col-md-10">

                        <hr>
                        <div id="page-wrapper">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h1 class="page-header">Gestão de eventos</h1>
                                </div>
                                <!-- /.col-lg-12 -->
                            </div>
                            <!-- /.row -->
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Adicionar evento
                                        </div>
                                        <!-- /.panel-heading -->

                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <table class="table table-striped">
                                        <form role="form" method="post" action="scripts/add_eventos.php"  enctype="multipart/form-data">
                                            <div class="form-group">
                                                <label>Nome</label>
                                                <input class="form-control" name="nome_evento"
                                                       placeholder="Nome do evento">
                                            </div>
                                            <div class="form-group">
                                                <label>Escalão</label>
                                            <select name="idade" class="form-control" id="exampleSelect1">
                                                <option value="1">Crianças</option>
                                                <option value="2">Jovens</option>
                                                <option value="3">Adultos</option>';
                                            </select>

                                            </div>
                                            <div class="form-group">
                                                <label>Área de interesse</label>
                                            <select name="interesse" class="form-control" id="exampleSelect1">
                                                <option value="1" >Matemática</option>
                                                <option value="2" >Arte</option>
                                                <option value="3">Física</option>
                                                <option value="4">Robótica</option>
                                                <option value="5">Ciências</option>
                                            </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Descrição pequena</label>
                                                <input class="form-control" name="pequena_descricao"
                                                       placeholder="Pequena descrição da sala">
                                            </div>
                                            <div class="form-group">
                                                <label>Descrição longa</label>
                                                <textarea rows="4" cols="50" class="form-control" name="grande_descricao" placeholder="Longa descrição da sala"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label>Lotação</label>
                                                <input class="form-control" name="lotacao"
                                                       placeholder="Lotação">
                                            </div>
                                            <?php
                                            if (isset($_SESSION["lotacao"])) {
                                                if ($_SESSION["lotacao"] == 20) {
                                                    echo "<div class='alert-danger'>
                <strong>Erro!</strong> Lotação inválida!
          </div>";
                                                    $_SESSION["lotacao"] = 0;
                                                }
                                            }?>
                                            <div class="form-group">
                                                <label>Preço</label>
                                                <input class="form-control" name="preco"
                                                       placeholder="Preço de cada bilhete">
                                            </div>
                                            <?php
                                            if (isset($_SESSION["preco"])) {
                                                if ($_SESSION["preco"] == 21) {
                                                    echo "<div class='alert-danger'>
                <strong>Erro!</strong> Preço inválido!
          </div>";
                                                    $_SESSION["preco"] = 0;
                                                }
                                            }?>
                                            <div class="form-group">
                                                <label>Tipo de evento</label><br>
                                                <select class="selectpicker" name="tipo_evento">
                                                    <?php
                                                    $link = new_db_connection(); // Create a new DB connection
                                                    $stmt = mysqli_stmt_init($link); // create a prepared statement
                                                    $query = "SELECT id_eventos_tipo, eventos_tipo.tipo FROM eventos_tipo";
                                                    if (mysqli_stmt_prepare($stmt, $query)) { // Prepare the statement
                                                        mysqli_stmt_execute($stmt); // Execute the prepared statement
                                                        mysqli_stmt_bind_result($stmt, $id_evento_tipo, $tipo_evento); // Bind results
                                                        while (mysqli_stmt_fetch($stmt)) { // Fetch values
                                                            echo "<option value='$id_evento_tipo'>$tipo_evento</option>";
                                                        }
                                                        mysqli_stmt_close($stmt); // Close statement
                                                    }
                                                    mysqli_close($link); // Close connection
                                                    ?>
                                                </select>
                                                <br><br>
                                            </div>
                                            <div class="form-group">
                                                <label>Imagem do Evento</label>
                                                <h6>Upload a different photo...</h6>
                                                <input class="form-control" type="file" id="fileToUpload"
                                                       name="fileToUpload">
                                            </div>
                                            <?php
                                            if (isset($_SESSION["imagem_nao"])) {
                                                if ($_SESSION["imagem_nao"] == 23) {
                                                    echo "<div class='alert-danger'>
                <strong>Erro!</strong> Imagem demasiado grande!
          </div>";
                                                    $_SESSION["imagem_nao"] = 0;
                                                }
                                            }?>
                                            <?php
                                            if (isset($_SESSION["imagem_grande"])) {
                                                if ($_SESSION["imagem_grande"] == 22) {
                                                    echo "<div class='alert-danger'>
                <strong>Erro!</strong> Formato de imagem não suportado!
          </div>";
                                                    $_SESSION["imagem_grande"] = 0;
                                                }
                                            }?>
                                            <?php
                                            if (isset($_SESSION["msg"])) {
                                                if ($_SESSION["msg"] == 1) {
                                                    echo "<div class='alert-danger'>
                <strong>Erro!</strong> Preencha os dados corretamente!
          </div>";
                                                    $_SESSION["msg"] = 0;
                                                }
                                            }?>
                                            <br>
                                            <button type = "submit" class="btn btn-default" > Submeter alterações
                                            </button >
                                        </form >

                                        <br>
                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <table class="table table-striped">
                                                    <thead>

                                                    </thead>
                                                    <tbody>


                                                    </tbody>
                                                </table>
                                            </div>
                                            <!-- /.table-responsive -->
                                        </div>
                                        <!-- /.panel-body -->
                                    </div>
                                    <!-- /.panel -->
                                </div>

                            </div>
                            <!-- /.row -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>