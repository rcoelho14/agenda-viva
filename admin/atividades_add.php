<?php require_once "connections/sql_connection.php" ?>
<!DOCTYPE html>
<head>

    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>AGENDA VIVA - Admin</title>
    <meta name="generator" content="Bootply"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <?php include_once 'helpers/css.php'; ?>
</head>
<body>
<?php include_once 'components/navbar_top.php'; ?>


<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3">
            <!-- Left column -->
            <?php include_once 'components/navbar_left.php'; ?>
            <!-- /col-3 -->
            <div class="col-sm-9">

                <!-- column 2 -->

                <div class="row">
                    <!-- center left-->
                    <div class="col-md-10">

                        <hr>
                        <div id="page-wrapper">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h1 class="page-header">Gestão de eventos ativos</h1>
                                </div>
                                <!-- /.col-lg-12 -->
                            </div>
                            <!-- /.row -->
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Adicionar evento ativo
                                        </div>
                                        <!-- /.panel-heading -->

                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <table class="table table-striped">
                                                    <form role="form" method="post" action="scripts/add_atividades.php">
                                                        <div class="form-group">
                                                            <label>Nome de evento</label><br>
                                                            <select class="selectpicker" name="nome_evento">
                                                                <?php
                                                                $link = new_db_connection(); // Create a new DB connection
                                                                $stmt = mysqli_stmt_init($link); // create a prepared statement
                                                                $query = "SELECT id_eventos, eventos.nome FROM eventos";
                                                                if (mysqli_stmt_prepare($stmt, $query)) { // Prepare the statement
                                                                    mysqli_stmt_execute($stmt); // Execute the prepared statement
                                                                    mysqli_stmt_bind_result($stmt, $id_evento, $nome_evento); // Bind results
                                                                    while (mysqli_stmt_fetch($stmt)) { // Fetch values
                                                                        echo "<option value='$id_evento'>$nome_evento</option>";
                                                                    }
                                                                    mysqli_stmt_close($stmt); // Close statement
                                                                }
                                                                mysqli_close($link); // Close connection
                                                                ?>
                                                            </select>
                                                            <br>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Data</label>
                                                            <input type="date" class="form-control" name="data"
                                                                   placeholder="Data do evento">
                                                        </div>
                                                        <?php
                                                        if (isset($_SESSION["date"])) {
                                                            if ($_SESSION["date"] == 1) {
                                                                echo "<div class='alert-danger'>
                <strong>Erro!</strong> Data inválida!
          </div>";
                                                                $_SESSION["date"] = 0;
                                                            }
                                                        }
                                                        ?>
                                                        <div class="form-group">
                                                            <label>Hora início</label>
                                                            <br>
                                                            <?php
                                                            // Make a DateTime object with the current date and time
                                                            $today = new DateTime();

                                                            // Make an empty array to contain the hours
                                                            $aHours = array();

                                                            // Make another DateTime object with the current date and time
                                                            $oStart = new DateTime('now');

                                                            // Set current time to midnight
                                                            $oStart->setTime(0, 0);

                                                            // Clone DateTime object (This is like 'copying' it)
                                                            $oEnd = clone $oStart;

                                                            // Add 1 day (24 hours)
                                                            $oEnd->add(new DateInterval("P1D"));

                                                            // Add each hour to an array
                                                            while ($oStart->getTimestamp() < $oEnd->getTimestamp()) {
                                                                $aHours[] = $oStart->format('H');
                                                                $oStart->add(new DateInterval("PT1H"));
                                                            }

                                                            // Create an array with halfs
                                                            $halfs = array(
                                                                '0',
                                                                '30',
                                                            );

                                                            // Get the current quarter
                                                            $currentHalf = $today->format('i') - ($today->format('i') % 30);
                                                            ?>
                                                            <select name="vaivaivai">
                                                                <option value="-1">Choose a time</option>
                                                                <?php foreach ($aHours as $hour): ?>
                                                                    <?php foreach ($halfs as $half): ?>
                                                                        <option value="<?= sprintf("%02d:%02d", $hour, $half); ?>" <?= ($hour == $today->format('H') && $half == $currentHalf) ?: ''; ?>>
                                                                            <?= sprintf("%02d:%02d", $hour, $half); ?>
                                                                        </option>
                                                                    <?php endforeach; ?>
                                                                <?php endforeach; ?>
                                                            </select></div><?php if (isset($_SESSION["hora"])) {
                                                            if ($_SESSION["hora"] == 1) {
                                                                echo "<div class='alert-danger'>
                <strong>Erro!</strong> Hora inválida!
          </div>";
                                                                $_SESSION["hora"] = 0;
                                                            }
                                                        }
                                                        ?>
                                                        <div class="form-group">
                                                            <label>Parte do dia</label>
                                                            <select name="parte" class="form-control"
                                                                    id="exampleSelect1">
                                                                <option value="1">Manhã</option>
                                                                <option value="2">Tarde</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Duração</label>
                                                            <input class="form-control" name="duracao"
                                                                   placeholder="Duração do evento">
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Lotação</label>
                                                            <input class="form-control" name="lotacao"
                                                                   placeholder="Lotação">
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Url</label>
                                                            <input class="form-control" name="url"
                                                                   placeholder="Url do evento">
                                                        </div>
                                                        <button type="submit" class="btn btn-default"> Submeter
                                                            alterações
                                                        </button>
                                                    </form>
                                                    <br>
                                                    <!-- /.panel-body -->
                                            </div>
                                            <!-- /.panel -->
                                        </div>

                                    </div>
                                    <!-- /.row -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</body>