<!DOCTYPE html>
<head>

    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>AGENDA VIVA - Admin</title>
    <meta name="generator" content="Bootply"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <?php include_once 'helpers/css.php'; ?>
</head>
<body>
<?php include_once 'components/navbar_top.php'; ?>


<div class="container-fluid">
    <div class="row">
        <div class="col-sm-2">
            <!-- Left column -->
            <?php include_once 'components/navbar_left.php'; ?>
            <!-- /col-3 -->
            <div class="col-sm-9">

                <!-- column 2 -->

                <div class="row">
                    <!-- center left-->
                    <div class="col-md-15">

                        <hr>
                        <div id="page-wrapper">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h1 class="page-header">Gestão de utilizadores</h1>
                                </div>
                                <!-- /.col-lg-12 -->
                            </div>
                            <!-- /.row -->
                            <div class="row">
                                <div class="col-lg-50">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Utilizadores registados
                                        </div>
                                        <!-- /.panel-heading -->


                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <table class="table table-striped">
                                                    <thead>
                                                    <tr>
                                                        <th> Id</th>
                                                        <th style="width: 10px !important;">Username</th>
                                                        <th style="width: 10px !important;">Nome</th>
                                                        <th style="width: 10px !important;">Email</th>
                                                        <th>Tipo</th>
                                                        <th>Estado</th>
                                                        <th>Apagar</th>
                                                        <th>Editar</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                    require_once 'connections/sql_connection.php';
                                                    $link = new_db_connection();
                                                    $stmt = mysqli_stmt_init($link);
                                                    $query = "SELECT id_users_geral, users_geral.nome, apelido, data_nascimento, email, username, activo, for_delete, profissao.tipo, escolaridade.tipo, pais.nome, users_geral.ref_id_tipo FROM users_geral INNER JOIN user_public ON id_users_geral = ref_id_users_geral INNER JOIN pais ON pais.id_pais = ref_id_pais INNER JOIN escolaridade ON id_escolaridade = ref_id_escolaridade INNER JOIN profissao ON id_profissao = ref_id_profissao";

                                                    if (mysqli_stmt_prepare($stmt, $query)) {
                                                        mysqli_stmt_execute($stmt);

                                                        mysqli_stmt_bind_result($stmt, $id, $nome, $apelido, $data, $email, $username, $activo, $for_delete, $profissao, $escolaridade, $pais, $ref_id_tipo);

                                                        /* fetch values */
                                                        while (mysqli_stmt_fetch($stmt)) { ?>
                                                            <tr>
                                                                <td><?php echo $id ?></td>
                                                                <td><?php echo $username ?>
                                                                <td><?php echo $apelido ?></td>
                                                                <td><?php echo $email ?></td>
                                                                <td><?php
                                                                    if ($ref_id_tipo == 2) {
                                                                        echo "Admin";
                                                                    } elseif ($ref_id_tipo == 1) {
                                                                        echo "Normal";
                                                                    }
                                                                    ?></td>
                                                                <td><?php
                                                                    if ($activo == 1) {
                                                                        echo "Ativo";
                                                                    } elseif ($activo == 0) {
                                                                        echo "Desativo";
                                                                    }
                                                                    ?></td>

                                                                <td><?php
                                                                    if ($for_delete == 0) {
                                                                        echo "Não";
                                                                    } elseif ($for_delete == 1) {
                                                                        echo "Sim";
                                                                    }
                                                                    ?></td>
                                                                <td><a href='user_edit.php?id=<?php echo $id ?>'> <i
                                                                                class="fa fa-edit"></i> </a>

                                                                    <?php

                                                                    if ($activo == 0) {

                                                                        echo "<a onclick='javascript:confirmationDelete($(this));return false;' href='scripts/user_delete_confirm.php?id=" . $id . "'><i
                                                                            class='fa fa-trash' title='Delete'></i></a>";

                                                                    }
                                                                    ?>

                                                                    <script>function confirmationDelete(anchor) {
                                                                            var conf = confirm('Tem a certeza que pretende apagar este utilizador?\nEsta ação é IRREVERSÍVEL!');
                                                                            if (conf)
                                                                                window.location = anchor.attr('href');
                                                                        }</script>

                                                                </td>
                                                            </tr>
                                                            <?php
                                                        }

                                                        /* close statement */
                                                        mysqli_stmt_close($stmt);
                                                    }

                                                    /* close connection */
                                                    mysqli_close($link);

                                                    ?>


                                                    </tbody>
                                                </table>
                                                <?php

                                                if (date("j") == 20) {

                                                    echo "<button  class = 'btn btn-default' onclick='javascript:confirmationDelete($(this));return false;' href='scripts/monthly_delete_confirm.php?id=1'><a><i
                                                                            class='fa fa-trash' title='Delete'></i></a> Apagar Utilizadores: Esta opção apaga TODOS os utilizadores que fizeram pedido para serem apagados.</button>";

                                                }
                                                ?>

                                                <script>function confirmationDelete(anchor) {
                                                        var conf = confirm('Tem a certeza que pretende apagar estes utilizadores?\nEsta ação é IRREVERSÍVEL!');
                                                        if (conf)
                                                            window.location = anchor.attr('href');
                                                    }</script>
                                            </div>
                                            <!-- /.table-responsive -->
                                        </div>
                                        <!-- /.panel-body -->
                                    </div>
                                    <!-- /.panel -->
                                </div>

                            </div>
                            <!-- /.row -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
