<!DOCTYPE html>
<head>

    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>AGENDA VIVA - Admin</title>
    <meta name="generator" content="Bootply"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <?php include_once 'helpers/css.php'; ?>
</head>
<body>
<?php include_once 'components/navbar_top.php'; ?>


<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3">
            <!-- Left column -->
            <?php include_once 'components/navbar_left.php'; ?>
            <!-- /col-3 -->
            <div class="col-sm-9">

                <!-- column 2 -->

                <div class="row">
                    <!-- center left-->
                    <div class="col-md-10">

                        <hr>
                        <div id="page-wrapper">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h1 class="page-header">Gestão de utilizadores</h1>
                                </div>
                                <!-- /.col-lg-12 -->
                            </div>
                            <!-- /.row -->
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Edição dos utilizadores
                                        </div>
                                        <!-- /.panel-heading -->

                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <table class="table table-striped">
                                                    <?php
                                                    if (isset($_GET["id"])) {
                                                        $id_user = $_GET["id"];
                                                        // We need the function!
                                                        require_once("connections/sql_connection.php");

                                                        // Create a new DB connection
                                                        $link = new_db_connection();

                                                        /* create a prepared statement */
                                                        $stmt = mysqli_stmt_init($link);

                                                        $query = "SELECT id_users_geral, username, date_creation, email, activo, users_geral.nome,apelido,data_nascimento, photo_name_uniq, ref_id_tipo, profissao.tipo, escolaridade.tipo, pais.nome 
                                                                  FROM users_geral 
                                                                  INNER JOIN user_public ON id_users_geral = ref_id_users_geral 
                                                                  INNER JOIN pais ON pais.id_pais = ref_id_pais 
                                                                  INNER JOIN escolaridade ON id_escolaridade = ref_id_escolaridade 
                                                                  INNER JOIN profissao ON id_profissao = ref_id_profissao 
                                                                  WHERE id_users_geral=?";
                                                        if (mysqli_stmt_prepare($stmt, $query)) {

                                                            mysqli_stmt_bind_param($stmt, 'i', $id_user);

                                                            /* execute the prepared statement */
                                                            mysqli_stmt_execute($stmt);

                                                            /* bind result variables */
                                                            mysqli_stmt_bind_result($stmt, $id_users_geral, $username, $date_creation, $email, $activo, $nome, $apelido, $data_nascimento, $photo_name_uniq, $ref_id_tipo, $profissao, $escolaridade, $pais);
                                                            /* fetch values */
                                                            while (mysqli_stmt_fetch($stmt)) {
                                                                echo '<form role="form" method="post" action="scripts/users_update.php">
                                                <input type="hidden" name="id_users" value="' . $id_users_geral . '">
                                                <div class="form-group">
                                                    <label>ID do utilizador</label>
                                                    <p class="form-control-static">' . $id_users_geral . '</p>
                                                </div>
                                                <div class="form-group">
                                                    <label>Data de criação</label>
                                                    <p class="form-control-static">' . $date_creation . '</p>
                                                </div>
                                               <div class="form-group">
                                                    <label>Username</label>
                                                    <p class="form-control-static">' . $username . '</p>
                                                </div>
                                                <div class="form-group">
                                                    <label>Email</label>
                                                    <p class="form-control-static">' . $email . '</p>
                                                </div>
                                                <div class="form-group">
                                                    <label>Nome</label>
                                                    <p class="form-control-static">' . $nome . '</p>
                                                </div>
                                                <div class="form-group">
                                                    <label>Apelido</label>
                                                    <p class="form-control-static">' . $apelido . '</p>
                                                </div>
                                                <div class="form-group">
                                                    <label>Data de nascimento</label>
                                                    <p class="form-control-static">' . $data_nascimento . '</p>
                                                </div>
                                                <div class="form-group">
                                                    <label>Profissão</label>
                                                    <p class="form-control-static">' . $profissao . '</p>
                                                </div>
                                                <div class="form-group">
                                                    <label>Escolaridade</label>
                                                    <p class="form-control-static">' . $escolaridade . '</p>
                                                </div>
                                                <div class="form-group">
                                                    <label>País</label>
                                                    <p class="form-control-static">' . $pais . '</p>
                                                </div>
                                                <div class="form-group">
                                                    <label>Estado</label>
                                                    <div class="checkbox">
                                                        <label>';
                                                                if ($activo == 1) {
                                                                    $checked = "checked";
                                                                } else {
                                                                    $checked = "";
                                                                }
                                                                echo '<input type="checkbox" name="active" ' . $checked . '>Activo
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>Perfil</label>
                                                    <select class="form-control" name="id_tipo">';
                                                                if ($ref_id_tipo == "2") {
                                                                    echo '<option value="1" selected >Admin</option>
                                                                <option value="2" >Normal</option>;';
                                                                } elseif ($ref_id_tipo == "1") {
                                                                    echo '<option value="1" >Admin</option>
                                                        <option value="2" selected >Normal</option>;';
                                                                }
                                                                echo '</select >
                                                </div >
                                                <button type = "submit" class="btn btn-default" > Submeter alterações
            </button >';
                                                                echo '</form >';
                                                            }

                                                            /* close statement */
                                                            mysqli_stmt_close($stmt);
                                                        }

                                                        /* close connection */
                                                        mysqli_close($link);
                                                    }
                                                    ?>
                                                    <br>
                                                    <div class="panel-body">
                                                        <div class="table-responsive">
                                                            <table class="table table-striped">
                                                                <thead>

                                                                </thead>
                                                                <tbody>


                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <!-- /.table-responsive -->
                                                    </div>
                                                    <!-- /.panel-body -->
                                            </div>
                                            <!-- /.panel -->
                                        </div>

                                    </div>
                                    <!-- /.row -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</body>