<?php
session_start();
require_once "../connections/sql_connection.php";
if ((isset($_POST["nome_evento"])) && (isset($_POST["pequena_descricao"])) && (isset($_POST["grande_descricao"])) && (isset($_POST["lotacao"])) && (isset($_POST["preco"])) && (isset($_POST["tipo_evento"]))) {
    $link = new_db_connection();
    $link2 = new_db_connection();
    $link3 = new_db_connection();
    $stmt = mysqli_stmt_init($link);
    $stmt2 = mysqli_stmt_init($link2);
    $stmt3 = mysqli_stmt_init($link3);
    $nome = $_POST['nome_evento'];
    $pequena_desc = $_POST['pequena_descricao'];
    $grande_desc = $_POST["grande_descricao"];
    $lotacao = $_POST["lotacao"];
    $preco = $_POST["preco"];
    $ref_id_tipo = $_POST["tipo_evento"];
    $idade = $_POST["idade"];
    $area = $_POST["interesse"];
    $select_id = "SELECT users_geral.id_users_geral FROM users_geral WHERE username=?";
    $query = "INSERT INTO eventos(nome, description_short, description, lotacao, preco, ref_id_eventos_tipo, ref_id_users_admin, idade, area) VALUES (?,?,?,?,?,?,?,?,?)";
    if (!is_numeric($lotacao) || $lotacao <= 0) {
        $_SESSION["lotacao"] = 20;
        if (!is_numeric($preco) || $preco <= 0) {
            $_SESSION["preco"] = 21;
        }
        header("Location: ../eventos_add.php");

    } elseif (!is_numeric($preco) || $preco <= 0) {
        $_SESSION["preco"] = 21;;
        if (!is_numeric($lotacao) || $lotacao <= 0) {
            $_SESSION["lotacao"] = 20;
        }

        header("Location: ../eventos_add.php");


    } else {
        if (mysqli_stmt_prepare($stmt, $select_id)) {
            mysqli_stmt_bind_param($stmt, 's', $username);
            $username = $_SESSION["username"];
            mysqli_stmt_bind_result($stmt, $ref_id_admin);
            // devemos validar também o resultado do execute!

            if (mysqli_stmt_execute($stmt)) {
                if (mysqli_stmt_fetch($stmt)) {
                    if (mysqli_stmt_prepare($stmt2, $query)) {
                        mysqli_stmt_bind_param($stmt2, 'sssssiiii', $nome, $pequena_desc, $grande_desc, $lotacao, $preco, $ref_id_tipo, $ref_id_admin, $idade, $area);

                        if (mysqli_stmt_execute($stmt2)) {
                            echo "erro2";
                            mysqli_stmt_close($stmt);
                            mysqli_stmt_close($stmt2);
                            mysqli_close($link);
                            mysqli_close($link2);
                            //header('Location: ../eventos.php');
                        } else {
                            echo "erro1";
                            $_SESSION["msg"] = 1;
                        }

                    }
                    // Fetch values

                }
            }
        } else {
            echo "erro2";
        }

        if ($_FILES["fileToUpload"] !== "") {
            $link4 = new_db_connection();
            $stmt4 = mysqli_stmt_init($link4);
            $foto_rand = uniqid(mt_rand(), true);
            $foto = $_FILES["fileToUpload"]["name"];
            $importar = $foto_rand . $foto;
            $target_dir = "../images/eventos";
            $target_file = $target_dir . basename($importar);
            $uploadOk = 1;
            $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

// Check if image file is a actual image or fake image

            if (isset($_POST["submit"])) {
                $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
                if ($check !== false) {
                    echo "File is an image - " . $check["mime"] . ".";
                    $uploadOk = 1;
                } else {
                    echo "File is not an image.";
                    $uploadOk = 0;
                }
            }
// Check if file already exists
            if (file_exists($target_file)) {
                echo "Sorry, file already exists.";
                $uploadOk = 0;
            }
// Check file size
            if ($_FILES["fileToUpload"]["size"] > 500000) {
                echo "Sorry, your file is too large.";
                $_SESSION["imagem_nao"] = 23;
                $uploadOk = 0;
            }
// Allow certain file formats
            if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                && $imageFileType != "gif") {
                echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                $_SESSION["imagem_grande"] = 22;
                $uploadOk = 0;
            }
// Check if $uploadOk is set to 0 by an error
            if ($uploadOk == 0) {
                echo "Sorry, your file was not uploaded.";
                header("Location: ../eventos_add.php");
// if everything is ok, try to upload file
            } else {

                if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                    $update = "UPDATE eventos SET imagem=? WHERE nome=?";
                    echo "echo3";
                    //$result = mysqli_query($link, $update);
                    var_dump(mysqli_stmt_prepare($stmt4, $update));
                    if (mysqli_stmt_prepare($stmt4, $update)) {
                        mysqli_stmt_bind_param($stmt4, 'ss', $importar, $nome);
                        mysqli_stmt_execute($stmt4);
                        mysqli_stmt_close($stmt4);
                        echo "siiiiiiii";
                        header("Location: ../eventos.php");
                    }
                    mysqli_close($link4);
                } else {
                    echo "Sorry, there was an error uploading your file.";
                    header("Location: ../eventos_add.php");
                }
            }
        }
    }

} else {
    echo "erro3";
}