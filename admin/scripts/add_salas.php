<?php
session_start();
require_once "../connections/sql_connection.php";
if ((isset($_POST["nome_sala"])) && (isset($_POST["pequena_descricao"])) && (isset($_POST["grande_descricao"])) && (isset($_POST["imagem"]))) {
    $link = new_db_connection();
    $link2 = new_db_connection();
    $stmt = mysqli_stmt_init($link);
    $stmt2 = mysqli_stmt_init($link2);

    $select_id = "SELECT users_geral.id_users_geral FROM users_geral WHERE username=?";
    $query = "INSERT INTO salas(nome, imagem, description_short, description, ref_id_users_admin) VALUES (?,?,?,?,?)";
    if (mysqli_stmt_prepare($stmt, $select_id)) {
        mysqli_stmt_bind_param($stmt, 's', $username);
        $username = $_SESSION["username"];
        mysqli_stmt_bind_result($stmt, $ref_id_admin);
        // devemos validar também o resultado do execute!

        if (mysqli_stmt_execute($stmt)) {
            while (mysqli_stmt_fetch($stmt)) {
                if (mysqli_stmt_prepare($stmt2, $query)) {
                    mysqli_stmt_bind_param($stmt2, 'ssssi', $nome, $imagem, $pequena_desc, $grande_desc, $ref_id_admin);
                    $nome = $_POST['nome_sala'];
                    $pequena_desc = $_POST['pequena_descricao'];
                    $grande_desc = $_POST["grande_descricao"];
                    $imagem = $_POST["imagem"];
                    if (mysqli_stmt_execute($stmt2)) {
                        echo "erro2";
                        mysqli_stmt_close($stmt);
                        mysqli_stmt_close($stmt2);
                        mysqli_close($link);
                        mysqli_close($link2);
                        header('Location: ../salas.php');
                    }
                    else {
                        echo "erro";
                    }

                }
                // Fetch values

            }
        }
    } else {
        echo "erro";
    }


} else {
    echo "erro";
}