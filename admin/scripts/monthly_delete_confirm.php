<?php
session_start();

if (isset($_GET['id'])) {
    $for_delete = $_GET['id'];
}

var_dump($for_delete);


require_once '../connections/sql_connection.php';
$link = new_db_connection();
$stmt = mysqli_stmt_init($link);

$query = "DELETE FROM users_geral WHERE for_delete = ?";

if (mysqli_stmt_prepare($stmt, $query)) {

    mysqli_stmt_bind_param($stmt, 'i', $for_delete);
    mysqli_stmt_execute($stmt);
    mysqli_stmt_close($stmt);
    header("Location: ../users.php");
}