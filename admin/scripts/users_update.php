<?php
if (isset($_POST["id_users"])) {
    if (isset($_POST["active"]) && $_POST['active']=='on') {
        $active="1";
    }
    else {
        $active="0";
    }
    if (isset($_POST["id_tipo"]) && $_POST['id_tipo'] == '1') {
        $tipo="2";

    }
    else {
        $tipo="1";
    }


    $id_users_geral = $_POST["id_users"];



    // We need the function!
    require_once("../connections/sql_connection.php");

    // Create a new DB connection
    $link = new_db_connection();

    /* create a prepared statement */
    $stmt = mysqli_stmt_init($link);
    $query = "UPDATE users_geral   /* nome da tabela */
              SET activo = ?, ref_id_tipo = ? /* cenas a fazer update */
              WHERE id_users_geral = ?"; /* onde o id é x */


    if (mysqli_stmt_prepare($stmt, $query)) {
        mysqli_stmt_bind_param($stmt, 'iii', $active, $tipo, $id_users_geral);

        /* execute the prepared statement */
        mysqli_stmt_execute($stmt);

        /* close statement */
        mysqli_stmt_close($stmt);
    }
    /* close connection */
    mysqli_close($link);

    header("Location: ../users.php");

} else {
    echo "algo de estranho se passou...";
}