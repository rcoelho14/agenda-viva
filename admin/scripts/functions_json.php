<?php
function put_json($file, $array)
{
    $jsonData = json_encode($array, JSON_HEX_QUOT|JSON_HEX_TAG|JSON_HEX_AMP|JSON_HEX_APOS);
    file_put_contents($file, $jsonData);
}

function get_json($file)
{
    if ($result = file_get_contents($file)) {
        $array = json_decode($result, true);
        return $array;
    } else {
        $empty = array();
        return $empty;
    }
}
