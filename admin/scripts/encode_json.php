<?php 
require_once 'connections/sql_connection.php';
require_once 'scripts/functions_json.php';

$link = new_db_connection(); 

$stmt = mysqli_stmt_init($link);

$file_json = "events.json";


//Calendário


$query = /*"SELECT eventos.id_eventos AS id,
 				 eventos.nome AS name,
 				 eventos.imagem AS imagem, 
 				 eventos.description_short AS description_short, 
 				 eventos.description AS description, 
 				 eventos.lotacao AS lotacao, 
 				 eventos.preco AS preco,
 				 eventos_tipo.tipo AS class, 
 				 salas.nome AS name_sala, 
 				 eventos_horarios.date AS startdate, 
 				 eventos_horarios.hora_inicio AS starttime, 
 				 eventos_horarios.duracao AS endtime, 
 				 eventos_horarios.bilhetes_disponiveis AS bilhetes_disponiveis
                    FROM eventos
                    INNER JOIN salas_has_eventos
                    ON salas_has_eventos.ref_id_eventos = eventos.id_eventos
                    INNER JOIN salas
                    ON salas_has_eventos.ref_id_salas = salas.id_salas
                    INNER JOIN eventos_tipo
                    ON eventos_tipo.id_eventos_tipo = eventos.ref_id_eventos_tipo
                    INNER JOIN eventos_horarios
                    ON eventos_horarios.ref_id_eventos = eventos.id_eventos";*/

"SELECT DISTINCT eventos.id_eventos AS id,
 				 eventos.nome AS name,
 				 eventos.imagem AS imagem, 
 				 eventos.description_short AS description_short, 
 				 eventos.description AS description, 
 				 eventos.lotacao AS lotacao, 
 				 eventos.preco AS preco,
 				 eventos_tipo.tipo AS tipo, 
 				 eventos_horarios.date AS startdate, 
 				 eventos_horarios.hora_inicio AS starttime,
 				 eventos_horarios.duracao AS endtime, 
 				 eventos_horarios.bilhetes_disponiveis AS bilhetes_disponiveis
                    FROM eventos
                    INNER JOIN eventos_tipo
                    ON eventos_tipo.id_eventos_tipo = eventos.ref_id_eventos_tipo
                    INNER JOIN eventos_horarios
                    ON eventos_horarios.ref_id_eventos = eventos.id_eventos";



    if ( ! $query ) {
        echo mysqli_error();
        die;
    }    

$result=mysqli_query($link,$query);
$rows=array();

	while($row = mysqli_fetch_assoc($result)) {
    		    $rows['monthly'][] = $row;

                      
	}




$encode_json = put_json($file_json, $rows); 
mysqli_free_result($result);
mysqli_close($link);
?>