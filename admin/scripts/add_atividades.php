<?php
session_start();
require_once "../connections/sql_connection.php";
if ((isset($_POST["nome_evento"])) && (isset($_POST["data"])) && (isset($_POST['parte'])) && (isset($_POST["lotacao"])) && (isset($_POST["duracao"])) && (isset($_POST["url"]))) {
    $link = new_db_connection();
    $link2 = new_db_connection();
    $stmt = mysqli_stmt_init($link);
    $stmt2 = mysqli_stmt_init($link2);
    $select_id = "SELECT users_geral.id_users_geral FROM users_geral WHERE username=?";
    $query = "INSERT INTO eventos_horarios(ref_id_eventos, date, hora_inicio, duracao, lotacao, url, ref_id_users_admin, parte) VALUES (?,?,?,?,?,?,?,?)";
    $ref_id_eventos = $_POST["nome_evento"];
    $data = $_POST['data'];
    $data_hoje = date('Y-m-d');
    $duracao = $_POST["duracao"];
    $lotacao = $_POST["lotacao"];
    $url = $_POST["url"];
    $vai = $_POST["vaivaivai"];
    $parte = $_POST['parte'];
    if ($vai == "-1") {
        $_SESSION["hora"] = 1;
    }
    if ($duracao <= 0) {
        echo "não dês hipótese, aos teus rivais";
        $_SESSION["duracao"] = 1;
        $duracao = "";
    }
    if ($data_hoje > $data) {
        echo "tu vais entrar, nas grandes finais";
        $_SESSION["date"] = 1;
        $data = "";
    }
    if ($vai == "-1" || $duracao == "" || $data == "") {
        header("Location: ../atividades_add.php");
    } else {
        if (mysqli_stmt_prepare($stmt, $select_id)) {
            mysqli_stmt_bind_param($stmt, 's', $username);
            $username = $_SESSION["username"];
            mysqli_stmt_bind_result($stmt, $ref_id_admin);
            // devemos validar também o resultado do execute!

            if (mysqli_stmt_execute($stmt)) {
                while (mysqli_stmt_fetch($stmt)) {
                    if (mysqli_stmt_prepare($stmt2, $query)) {
                        mysqli_stmt_bind_param($stmt2, 'isssisii', $ref_id_eventos, $data, $vai, $duracao, $lotacao, $url, $ref_id_admin, $parte);
                        if (mysqli_stmt_execute($stmt2)) {
                            echo "erro2";
                            mysqli_stmt_close($stmt);
                            mysqli_stmt_close($stmt2);
                            mysqli_close($link);
                            mysqli_close($link2);
                            header('Location: ../atividades.php');
                        } else {
                            echo "erro1";
                        }

                    }
                    // Fetch values

                }
            }
        } else {
            echo "erro3";
        }
    }

} else {
    echo "erro4";
}