<?php
session_start();
if (isset($_POST["id_users"])) {
    $id_eventos_horarios = $_POST["id_users"];
    $hora_inicio = $_POST['vaivaivai'];
    $duracao = $_POST['duracao'];
    $lotacao = $_POST['lotacao'];
    $bilhetes_disponiveis = $_POST['bilhetes'];
    $url = $_POST['url'];
    $data_eventu = $_POST['data_eventu'];
    $data = date('Y-m-d');
    $parte = $_POST['parte'];


    // We need the function!
    require_once("../connections/sql_connection.php");

    // Create a new DB connection
    $link = new_db_connection();

    /* create a prepared statement */
    $stmt = mysqli_stmt_init($link);
    $query = "UPDATE eventos_horarios   /* nome da tabela */
              SET date = ?, hora_inicio = ?, duracao = ?, lotacao = ?, bilhetes_disponiveis = ?, url = ?, parte = ? /* cenas a fazer update */
              WHERE id_eventos_horarios = ?"; /* onde o id é x */

    if ($hora_inicio < 0 || $hora_inicio > 24) {
        echo "és o maior, paços de ferreira";
        $_SESSION["hora"] = 1;
        $hora_inicio = "";
    }
    if ($bilhetes_disponiveis > $lotacao) {
        echo "tu vais ficar sempre na primeira";
        $_SESSION["bilhete"] = 1;
        $bilhetes_disponiveis = "";
    }
    if (!(is_numeric($bilhetes_disponiveis))) {
        $_SESSION["num_bil"] = 1;
        $bilhetes_disponiveis = "";
    }
    if (!(is_numeric($lotacao))) {
        $_SESSION["num_lota"] = 1;
        $bilhetes_disponiveis = "";
    }
    if ($duracao < 0) {
        echo "não dês hipótese, aos teus rivais";
        $_SESSION["duracao"] = 1;
        $duracao = "";
    }
    if ($data > $data_eventu) {
        echo "tu vais entrar, nas grandes finais";
        $_SESSION["date"] = 1;
        $data_eventu = "";
    }
    if ($hora_inicio == "" || $bilhetes_disponiveis == "" || $duracao == "" || $data_eventu == "") {
        header("Location: ../atividades_edit.php?id=$id_eventos_horarios");
    } else {
        if (mysqli_stmt_prepare($stmt, $query)) {
            mysqli_stmt_bind_param($stmt, 'ssiiisii', $data_eventu, $hora_inicio, $duracao, $lotacao, $bilhetes_disponiveis, $url, $parte, $id_eventos_horarios);


            mysqli_stmt_execute($stmt);
            header("Location: ../atividades.php");

            mysqli_stmt_close($stmt);
        }
        mysqli_close($link);
    }


} else {
    echo "algo de estranho se passou...";
}