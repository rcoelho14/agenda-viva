<!DOCTYPE html>


<head>

    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>AGENDA VIVA - Admin</title>
    <meta name="generator" content="Bootply"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <?php include_once 'helpers/css.php'; ?>
</head>
<body>
<?php include_once 'components/navbar_top.php'; ?>


<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3">
            <!-- Left column -->
            <?php include_once 'components/navbar_left.php'; ?>
            <!-- /col-3 -->
            <div class="col-sm-9">

                <!-- column 2 -->

                <div class="row">
                    <!-- center left-->
                    <div class="col-md-10">

                        <hr>
                        <div id="page-wrapper">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h1 class="page-header">Gestão de Eventos</h1>
                                </div>
                                <!-- /.col-lg-12 -->
                            </div>
                            <!-- /.row -->
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Eventos
                                        </div>
                                        <!-- /.panel-heading -->

                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <table class="table table-striped">
                                                    <thead>
                                                    <tr>
                                                        <th>ID</th>
                                                        <th>Nome</th>
                                                        <th>Imagem</th>
                                                        <th>Short Desc</th>
                                                        <th>Lotação</th>
                                                        <th>Preço</th>
                                                        <th>Tipo</th>

                                                        <th>Editar</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>

                                                    <?php
                                                    require_once 'connections/sql_connection.php';                                                    $link = new_db_connection();
                                                    $stmt = mysqli_stmt_init($link);
                                                    $query = "SELECT id_eventos, nome, imagem, description_short, description,                                                              lotacao, preco, eventos_tipo.tipo FROM eventos
                                                              INNER JOIN eventos_tipo ON id_eventos_tipo = ref_id_eventos_tipo
                                                              ORDER BY id_eventos";
                                                    if (mysqli_stmt_prepare($stmt, $query)) {
                                                        mysqli_stmt_execute($stmt);

                                                        mysqli_stmt_bind_result($stmt, $id, $nome, $imagem, $short, $long, $lotacao, $preco, $tipo);

                                                        /* fetch values */
                                                        while (mysqli_stmt_fetch($stmt)) { ?>
                                                            <tr>
                                                                <td><?php echo $id ?></td>
                                                                <td><?php echo $nome ?></td>
                                                                <td><img height="60"
                                                                         src="images/eventos<?php echo $imagem ?>"></td>
                                                                <td><?php echo $short ?></td>
                                                                <td><?php echo $lotacao ?></td>
                                                                <td><?php echo $preco ?></td>
                                                                <td><?php echo $tipo ?></td>
                                                                <td><a href='eventos_edit.php?id=<?php echo $id ?>'> <i
                                                                                class="fa fa-edit"></i> </a>
                                                                    <?php
                                                                        echo "<a onclick='javascript:confirmationDelete($(this));return false;' href='scripts/event_delete_confirm.php?id=" . $id . "'><i
                                                                            class='fa fa-trash' title='Delete'></i></a>";
                                                                    ?>

                                                                    <script>function confirmationDelete(anchor) {
                                                                            var conf = confirm('Tem a certeza que pretende apagar este evento?\nEsta ação é IRREVERSÍVEL!');
                                                                            if (conf)
                                                                                window.location = anchor.attr('href');
                                                                        }</script>

                                                                </td>
                                                            </tr> <?php
                                                        }

                                                        /* close statement */
                                                        mysqli_stmt_close($stmt);
                                                    }

                                                    /* close connection */
                                                    mysqli_close($link);

                                                    ?>


                                                    <tr>
                                                        <td><a href="eventos_add.php"><i class="fas fa-plus"></i></a>
                                                        </td>
                                                    </tr>


                                                    </tbody>
                                                </table>
                                            </div>
                                            <!-- /.table-responsive -->
                                        </div>
                                        <!-- /.panel-body -->
                                    </div>
                                    <!-- /.panel -->
                                </div>

                            </div>
                            <!-- /.row -->
                        </div>


                        <?php include_once 'helpers/js.php'; ?>
</body>
</html>