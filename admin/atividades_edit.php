<!DOCTYPE html>
<head>

    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>AGENDA VIVA - Admin</title>
    <meta name="generator" content="Bootply"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <?php include_once 'helpers/css.php'; ?>
</head>
<body>
<?php include_once 'components/navbar_top.php'; ?>


<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3">
            <!-- Left column -->
            <?php include_once 'components/navbar_left.php'; ?>
            <!-- /col-3 -->
            <div class="col-sm-9">

                <!-- column 2 -->

                <div class="row">
                    <!-- center left-->
                    <div class="col-md-10">

                        <hr>
                        <div id="page-wrapper">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h1 class="page-header">Gestão de eventos ativos</h1>
                                </div>
                                <!-- /.col-lg-12 -->
                            </div>
                            <!-- /.row -->
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Edição de eventos ativos
                                        </div>
                                        <!-- /.panel-heading -->
                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <table class="table table-striped">
                                                    <?php
                                                    if (isset($_GET["id"])) {
                                                        $id_user = $_GET["id"];
                                                        // We need the function!
                                                        require_once("connections/sql_connection.php");

                                                        // Create a new DB connection
                                                        $link = new_db_connection();

                                                        /* create a prepared statement */
                                                        $stmt = mysqli_stmt_init($link);

                                                        $query = "SELECT id_eventos_horarios, date, hora_inicio, duracao, lotacao, bilhetes_disponiveis,ref_id_eventos,ref_id_users_admin, date_creation, url, parte FROM eventos_horarios WHERE id_eventos_horarios = ?";

                                                        if (mysqli_stmt_prepare($stmt, $query)) {

                                                            mysqli_stmt_bind_param($stmt, 'i', $id_user);

                                                            /* execute the prepared statement */
                                                            mysqli_stmt_execute($stmt);

                                                            /* bind result variables */
                                                            mysqli_stmt_bind_result($stmt, $id_eventos_horarios, $data_eventu, $hora_inicio, $duracao, $lotacao, $bilhetes_disponiveis, $ref_id_eventos, $ref_id_users_admin, $date_creation, $url, $parte);
                                                            /* fetch values */
                                                            while (mysqli_stmt_fetch($stmt)) {
                                                                echo '<form role="form" method="post" action="scripts/atividades_update.php">
                                                <input type="hidden" name="id_users" value="' . $id_eventos_horarios . '">
                                                <div class="form-group">
                                                    <label>ID do evento</label>
                                                    <p class="form-control-static">' . $id_eventos_horarios . '</p>
                                                </div>
                                                <div class="form-group">
                                                    <label>Data de modificação</label>
                                                    <p class="form-control-static">' . $date_creation . '</p>
                                                </div>
                                                <div class="form-group">
                                                <label>Parte do dia</label>
                                                <select name="parte" class="form-control" id="exampleSelect1">
            ';
                                                                if ($parte == 1) {
                                                                    echo '<option value="1" selected>Manhã</option>
                <option value="2">Tarde</option>';
                                                                }
                                                                if ($parte == 2) {
                                                                    echo '<option value="1">Manhã</option>
                <option value="2" selected>Tarde</option>';
                                                                }
                                                                echo '
            
                
            </select>
        </div>
                                                <div class="form-group">
                                                    <label>Hora de inicío</label>';
                                                                // Make a DateTime object with the current date and time
                                                                $today = new DateTime();

                                                                // Make an empty array to contain the hours
                                                                $aHours = array();

                                                                // Make another DateTime object with the current date and time
                                                                $oStart = new DateTime('now');

                                                                // Set current time to midnight
                                                                $oStart->setTime(0, 0);

                                                                // Clone DateTime object (This is like 'copying' it)
                                                                $oEnd = clone $oStart;

                                                                // Add 1 day (24 hours)
                                                                $oEnd->add(new DateInterval("P1D"));

                                                                // Add each hour to an array
                                                                while ($oStart->getTimestamp() < $oEnd->getTimestamp()) {
                                                                    $aHours[] = $oStart->format('H');
                                                                    $oStart->add(new DateInterval("PT1H"));
                                                                }

                                                                // Create an array with halfs
                                                                $halfs = array(
                                                                    '0',
                                                                    '30',
                                                                );

                                                                // Get the current quarter
                                                                $currentHalf = $today->format('i') - ($today->format('i') % 30);
                                                                ?>
                                                                <div class="form-group">
                                                                    <select name="vaivaivai" id="">
                                                                        <option value="-1">Choose a time</option>
                                                                        <option value="$hora_inicio"
                                                                                selected><?= $hora_inicio ?></option>
                                                                        <?php foreach ($aHours as $hour): ?>
                                                                            <?php foreach ($halfs as $half): ?>
                                                                                <option value="<?= sprintf("%02d:%02d", $hour, $half); ?>" <?= ($hour == $today->format('H') && $half == $currentHalf) ?: ''; ?>>
                                                                                    <?= sprintf("%02d:%02d", $hour, $half); ?>
                                                                                </option>
                                                                            <?php endforeach; ?>
                                                                        <?php endforeach; ?>
                                                                    </select></div>

                                                                <?php
                                                                echo '</div>';
                                                                if (isset($_SESSION["hora"])) {
                                                                    if ($_SESSION["hora"] == 1) {
                                                                        echo "<div class='alert-danger'>
                <strong>Erro!</strong> Hora inválida!
          </div>";
                                                                        $_SESSION["hora"] = 0;
                                                                    }
                                                                }

                                                                echo '<div class="form-group">
                                                    <label>Duração</label>
                                                    <input class="form-control" name="duracao" value="' . $duracao . '">
                                                </div>';
                                                                if (isset($_SESSION["duracao"])) {
                                                                    if ($_SESSION["duracao"] == 1) {
                                                                        echo "<div class='alert-danger'>
                <strong>Erro!</strong> Duração inválida!
          </div>";
                                                                        $_SESSION["duracao"] = 0;
                                                                    }
                                                                }

                                                                echo '
                                                <div class="form-group">
                                                    <label>Data</label>
                                                    <input type="date" class="form-control" name="data_eventu"
                                                           value="' . $data_eventu . '">
                                                </div>';
                                                                if (isset($_SESSION["date"])) {
                                                                    if ($_SESSION["date"] == 1) {
                                                                        echo "<div class='alert-danger'>
                <strong>Erro!</strong> Data inválida!
          </div>";
                                                                        $_SESSION["date"] = 0;
                                                                    }
                                                                }
                                                                echo '<div class="form-group">
                                                    <label>Lotação</label>
                                                    <input class="form-control" name="lotacao"
                                                           value="' . $lotacao . '">
                                                </div>';
                                                                if (isset($_SESSION["num_lota"])) {
                                                                    if ($_SESSION["num_lota"] == 1) {
                                                                        echo "<div class='alert-danger'>
                <strong>Erro!</strong> Lotação inválida!
          </div>";
                                                                        $_SESSION["num_lota"] = 0;
                                                                    }
                                                                }

                                                                echo '<div class="form-group">
                                                    <label>Bilhetes Disponíveis</label>
                                                    <input class="form-control" name="bilhetes"
                                                           value="' . $bilhetes_disponiveis . '">
                                                </div>';

                                                                if (isset($_SESSION["num_bil"])) {
                                                                    if ($_SESSION["num_bil"] == 1) {
                                                                        echo "<div class='alert-danger'>
                <strong>Erro!</strong> Número inválido de bilhetes!<br>
          </div>";
                                                                        $_SESSION["num_bil"] = 0;
                                                                    }
                                                                }
                                                                if (isset($_SESSION["bilhete"])) {
                                                                    if ($_SESSION["bilhete"] == 1) {
                                                                        echo "<div class='alert-danger'>
                <strong>Erro!</strong> A lotação tem de ser maior que o número de bilhetes disponíveis!
          </div>";
                                                                        $_SESSION["bilhete"] = 0;
                                                                    }
                                                                }
                                                                echo '<div class="form-group">
                                                    <label>URL</label>
                                                    <input class="form-control" name="url"
                                                           value="' . $url . '">
                                                </div>
                                                </div>
 
                                              
                                                <button type = "submit" class="btn btn-default" > Submeter alterações
            </button >';
                                                                echo '</form >';
                                                            }

                                                            /* close statement */
                                                            mysqli_stmt_close($stmt);
                                                        }

                                                        /* close connection */
                                                        mysqli_close($link);
                                                    }
                                                    ?>
                                                    <br>
                                                    <div class="panel-body">
                                                        <div class="table-responsive">
                                                            <table class="table table-striped">
                                                                <thead>

                                                                </thead>
                                                                <tbody>


                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <!-- /.table-responsive -->
                                                    </div>
                                                    <!-- /.panel-body -->
                                            </div>
                                            <!-- /.panel -->
                                        </div>

                                    </div>
                                    <!-- /.row -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</body>