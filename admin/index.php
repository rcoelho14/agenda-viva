<?php include_once 'scripts/functions_json.php'; ?>

<!DOCTYPE html>

<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>AGENDA VIVA - Admin</title>
    <meta name="generator" content="Bootply"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <script type="text/javascript" async src="js/scripts.js"></script>

    <?php include_once 'helpers/css.php'; ?>
    <?php include_once 'helpers/js.php'; ?>
</head>
<body>

<?php include_once 'components/navbar_top.php'; ?>


<!-- Main -->
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3">
            <!-- Left column -->
            <?php include_once 'components/navbar_left.php'; ?>
            <!-- /col-3 -->
            <div class="col-sm-9">

                <!-- column 2 -->

                <div class="row">
                    <!-- center left-->
                    <div class="col-md-6">


                        <?php /*include_once 'components/reports.php';*/ ?>
                        <?php include_once 'components/calendar.php'; ?>


                        <hr>

                    </div>
                    <!--/col-->
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4>Noticias</h4></div>
                            <div class="panel-body">
                                <div class="alert alert-info">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    Os bilhetes não estão prontos
                                </div>
                                <p> A página de administração está a funcionar, aqui podem alterar as definições da
                                    Agenda Viva</p>

                            </div>
                        </div>
                        <?php include_once 'components/statistics_table.php'; ?>


                        <!--/panel content-->
                    </div>
                    <!--/panel-->


                </div>
                <!--/col-span-6-->

            </div>
            <!--/row-->

            <hr>

        </div>
    </div>
    <!--/col-span-9-->
</div>
</div>
<!-- /Main -->

<?php include_once 'components/footer.php'; ?>
<!-- /.modal -->
<!-- script references -->
<?php include_once 'helpers/js.php'; ?>
</body>
</html>