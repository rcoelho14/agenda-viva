<!DOCTYPE html>


<head>

    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>AGENDA VIVA - Admin</title>
    <meta name="generator" content="Bootply"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.2/css/star-rating.min.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.2/js/star-rating.min.js"></script>


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        .checked {
            color: orange;
        }
    </style>

    <?php include_once 'helpers/css.php'; ?>
    <?php include_once 'helpers/js.php'; ?>
</head>
<body>
<?php include_once 'components/navbar_top.php'; ?>


<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3">
            <!-- Left column -->
            <?php include_once 'components/navbar_left.php'; ?>
            <!-- /col-3 -->
            <div class="col-sm-9">

                <!-- column 2 -->

                <div class="row">
                    <!-- center left-->
                    <div class="col-md-10">

                        <hr>
                        <div id="page-wrapper">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h1 class="page-header">Gestão de Reviews</h1>
                                </div>
                                <!-- /.col-lg-12 -->
                            </div>
                            <!-- /.row -->
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Reviews
                                        </div>
                                        <!-- /.panel-heading -->

                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <table class="table table-striped">
                                                    <thead>
                                                    <tr>
                                                        <th>ID Reviews</th>
                                                        <th>ID User</th>
                                                        <th>Evento</th>
                                                        <th>Rating</th>
                                                        <th>Comentário</th>
                                                        <th>Data</th>
                                                        <th>Ações</th>


                                                    </tr>
                                                    </thead>
                                                    <tbody>


                                                    <?php
                                                    $query2 = "SELECT  reviews.id_reviews, reviews.ref_id_users_geral_geral, reviews.rating, reviews.comentario, reviews.date_creation, eventos.nome
                                                                FROM reviews
                                                                INNER JOIN eventos
                                                                ON reviews.ref_id_eventos = eventos.id_eventos
                                                                ORDER BY reviews.date_creation ASC";


                                                    require_once 'connections/sql_connection.php';
                                                    $link = new_db_connection();
                                                    $stmt = mysqli_stmt_init($link);


                                                    if (mysqli_stmt_prepare($stmt, $query2)) {

                                                        /* mysqli_stmt_bind_param($stmt, "is", $id);*/

                                                        mysqli_stmt_execute($stmt);
                                                        mysqli_stmt_bind_result($stmt, $id_reviews, $ref_id_users_geral_geral, $rating, $comentario, $date_creation, $event_name);

                                                        while (mysqli_stmt_fetch($stmt)) { ?>
                                                            <tr>
                                                                <td><?php echo $id_reviews ?></td>
                                                                <td><?php echo $ref_id_users_geral_geral ?></td>
                                                                <td><?php echo $event_name ?></td>
                                                                <td><?php
                                                                    switch ($rating) {
                                                                        case 0:
                                                                            { ?>
                                                                                <span class="fa fa-star"></span>
                                                                                <?php
                                                                                break;
                                                                            }
                                                                        case 1:
                                                                            { ?>
                                                                                <span class="fa fa-star checked"></span>

                                                                                <?php
                                                                                break;
                                                                            }
                                                                        case 2:
                                                                            { ?>
                                                                                <span class="fa fa-star checked"></span>
                                                                                <span class="fa fa-star checked"></span>
                                                                                <?php
                                                                                break;
                                                                            }
                                                                        case 3:
                                                                            { ?>
                                                                                <span class="fa fa-star checked"></span>
                                                                                <span class="fa fa-star checked"></span>
                                                                                <span class="fa fa-star checked"></span>
                                                                                <?php
                                                                                break;
                                                                            }
                                                                        case 4:
                                                                            { ?>
                                                                                <span class="fa fa-star checked"></span>
                                                                                <span class="fa fa-star checked"></span>
                                                                                <span class="fa fa-star checked"></span>
                                                                                <span class="fa fa-star checked"></span>
                                                                                <?php
                                                                                break;
                                                                            }
                                                                        case 5:
                                                                            { ?>
                                                                                <span class="fa fa-star checked"></span>
                                                                                <span class="fa fa-star checked"></span>
                                                                                <span class="fa fa-star checked"></span>
                                                                                <span class="fa fa-star checked"></span>
                                                                                <span class="fa fa-star checked"></span>
                                                                                <?php
                                                                                break;
                                                                            }
                                                                    }
                                                                    ?></td>
                                                                <td><?php echo $comentario ?>
                                                                <td><?php echo $date_creation ?></td>
                                                                <td><?php
                                                                    echo "<a onclick='javascript:confirmationDelete($(this));return false;' href='scripts/review_delete_confirm.php?id=" . $id_reviews . "'><i
                                                                            class='fa fa-trash' title='Delete'></i></a>";
                                                                    ?>

                                                                    <script>function confirmationDelete(anchor) {
                                                                            var conf = confirm('Tem a certeza que pretende apagar este evento?\nEsta ação é IRREVERSÍVEL!');
                                                                            if (conf)
                                                                                window.location = anchor.attr('href');
                                                                        }</script>

                                                                </td>
                                                            </tr> <?php
                                                        }

                                                        /* close statement */
                                                        mysqli_stmt_close($stmt);
                                                    }

                                                    /* close connection */
                                                    mysqli_close($link);

                                                    ?>


                                                    </tbody>
                                                </table>
                                            </div>
                                            <!-- /.table-responsive -->
                                        </div>
                                        <!-- /.panel-body -->
                                    </div>
                                    <!-- /.panel -->
                                </div>

                            </div>
                            <!-- /.row -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include_once 'helpers/js.php'; ?>
</body>
</html>