<!DOCTYPE HTML>
<html>
<head>
    <?php include "components/meta.php" ?>



    <?php include "components/css.php" ?>


</head>
<body>


<div id="page">

    <?php include "components/menu.php" ?>
    <div class="col-lg-6 col-12 pb-3 col-lg-offset-3">

        <br>
        <h2 class="text-center mb-4 col-lg-offset-0">Workshops</h2>
        <div class="row text-center">

            <?php

            $query = "SELECT id_eventos, nome, imagem, description_short, tipo FROM eventos INNER JOIN eventos_tipo ON eventos_tipo.id_eventos_tipo = eventos.ref_id_eventos_tipo WHERE ref_id_eventos_tipo=1";


            include_once "conections/conections.php";

            $link = new_db_connection();
            $stmt = mysqli_stmt_init($link);

            if (mysqli_stmt_prepare($stmt, $query)) {

                //mysqli_stmt_bind_param($stmt, "s", $pesquisa);

                mysqli_stmt_execute($stmt);
                mysqli_stmt_bind_result($stmt, $id, $nome, $imagem, $short, $tipo);

                while (mysqli_stmt_fetch($stmt)) { ?>

                    <div class="card">
                        <hr>
                        <img class="card-img-top" src="../admin/images/eventos<?php echo $imagem ?>" alt="">
                        <div class="card-body">
                            <br>
                            <h4><?php echo $nome ?></h4>
                            <h6><?php echo $short ?></h6>

                            <a href="workshops.php?id=<?php echo $id ?>" class="btn btn-primary" style="color: white">Ver mais</a>
                        </div>
                    </div>
                    <?php
                }
                mysqli_stmt_close($stmt);
            }
            mysqli_close($link);

            ?>






        </div>
    </div>
</div>


<br>
<?php include "components/footer.php" ?>


<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
</div>

<?php include "components/script.php" ?>


</body>
</html>
