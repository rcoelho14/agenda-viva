<!DOCTYPE HTML>
<html>
<?php include "components/meta.php" ?>

<!-- <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet"> -->
<!-- <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i" rel="stylesheet"> -->

<?php include "components/css.php" ?>


</head>
<body>

<div class="fh5co-loader"></div>

<div id="page" class="">
    <?php include "components/menu.php" ?>
    <?php
    if (isset($_GET["novo"])) {
        $novo = $_GET["novo"];
    } else {
        header("Location: recuperacao_pass.php");
    }
    ?>
    <div class="col-lg-6 col-12 pb-3 col-lg-offset-3">
        <div class="card h-100">
            <div class="card-body">
                <br>
                <h2 class="text-center mb-4 col-lg-offset-0">Insira a nova password</h2>
                <form class="col-lg-offset-2" method="post" role="form" id="register-form"
                      action="components/update_pass.php?form=<?= $novo ?>">
                    <div class="form-group">
                        <div class="mx-auto col-sm-10 pb-3 pt-2">
                            <?php
                            if (isset($_SESSION["msg"])) {
                                if ($_SESSION["msg"] == 4) {
                                    echo "<br><div class='alert alert-success'>
                <strong>Sucesso!</strong> Pode inserir uma nova password!
          </div>";
                                    $_SESSION["msg"] = 12;
                                }
                                if ($_SESSION["msg"]==5) {
                                    echo "<br><div class='alert alert-danger'>
                <strong>Erro!</strong> As passwords não coincidem!
          </div>";
                                    $_SESSION["msg"] = 12;
                                }
                            }
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputpass" class="sr-only form-control-label">Password</label>
                        <div class="mx-auto col-sm-10">
                            <input type="password" class="form-control" id="password" name="password"
                                   placeholder="Password"
                                   required="required">
                            <br>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputpass" class="sr-only form-control-label">Confirmar Password</label>
                        <div class="mx-auto col-sm-10">
                            <input type="password" class="form-control" id="confirmar_password" name="confirmar_password"
                                   placeholder="Confirmar Password"
                                   required="required">
                            <br>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="mx-auto col-sm-10 pb-3 pt-2">
                            <button type="submit" class="btn btn-outline-secondary btn-lg btn-block">Definir nova password
                            </button><br>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?php include "components/footer.php" ?>
</div>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
</div>

<?php include "components/script.php" ?>
</body>
</html>