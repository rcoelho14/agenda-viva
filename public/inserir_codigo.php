<!DOCTYPE HTML>
<html>
<?php include "components/meta.php" ?>

<!-- <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet"> -->
<!-- <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i" rel="stylesheet"> -->

<?php include "components/css.php" ?>


</head>
<body>

<div class="fh5co-loader"></div>

<div id="page" class="">
    <?php include "components/menu.php" ?>
    <?php
    if (isset($_GET["rec"])) {
        $get = $_GET["rec"];
    } else {
        header("Location: recuperacao_pass.php");
    }
    ?>
    <div class="col-lg-6 col-12 pb-3 col-lg-offset-3">
        <div class="card h-100">
            <div class="card-body">
                <br>
                <h2 class="text-center mb-4 col-lg-offset-0">Insira o código de recuperação</h2>
                <form class="col-lg-offset-2" method="post" role="form" id="register-form"
                      action="components/verifica_codigos.php?pass=<?= $get ?>">
                    <div class="form-group">
                        <div class="mx-auto col-sm-10 pb-3 pt-2">
                            <?php
                            if (isset($_SESSION["msg"])) {
                                if ($_SESSION["msg"] == 2) {
                                    echo "<div class='alert alert-success'>
                Foi enviado um email de recuperação de password
          </div>";
                                    $_SESSION["msg"] = 12;
                                }
                                if ($_SESSION["msg"]==3) {
                                    echo "<div class='alert alert-danger'>
                <strong>Erro!</strong> Código de recuperação inválido!
          </div>";
                                    $_SESSION["msg"] = 13;
                                }
                            }
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="mx-auto col-sm-10">
                            <input type="text" class="form-control" id="codigo" name="codigo"
                                   placeholder="Código"
                                   required="required">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="mx-auto col-sm-10 pb-3 pt-2">
                            <button type="submit" class="btn btn-outline-secondary btn-lg btn-block">Verificar código
                            </button><br>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>

<?php include "components/footer.php" ?>
<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
</div>

<?php include "components/script.php" ?>
</body>
</html>