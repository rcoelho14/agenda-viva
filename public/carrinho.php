<!DOCTYPE HTML>
<html>
<head>
    <?php include "components/meta.php" ?>


    <?php include "components/css.php" ?>


</head>
<body>
<div id="page">

    <?php include "components/menu.php" ;
    ?>


        <br>
        <h2 class="text-center mb-4 col-lg-offset-0">Carrinho de Compras</h2>
    <br>
        <?php

        include_once 'components/cart_details.php';
        ?>
    <div class="col-lg-6 col-12 pb-3 col-lg-offset-3">
        <?php

        include_once 'components/cart_try.php';
        ?>

    </div>
</div>
</div>
<?php include "components/footer.php" ?>


<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
</div>

<?php include "components/script.php" ?>
</body>
</html>