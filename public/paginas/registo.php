<?php
require_once "conections/conections.php";
?>

<div class="col-lg-6 col-6 pb-3 col-lg-offset-3">
    <div class="card h-100">
        <div class="card-body">
            <br>
            <h2 class="text-center mb-4 col-lg-11">Registo</h2>
            <form method="post" role="form" id="register-form" action="components/registo.php" name="form1">

                <div class="form-group col-lg-offset-1">
                    <div class="mx-auto col-sm-10">
                        <label for="input2EmailForm" class="form-control-label">Username</label>
                        <input type="text" class="form-control" id="username" name="username"
                               placeholder="Username"
                               required="required">
                    </div>

                </div>

                <div class="form-group col-lg-offset-1">
                    <div class="mx-auto col-sm-10">
                        <p>
                            <label for="inputpass" class="form-control-label">Password</label>
                            <input type="password" class="form-control" id="password" name="password"
                                   placeholder="Password"
                                   required="required">
                        </p>
                    </div>

                </div>
                <div class="form-group col-lg-offset-1">
                    <div class="mx-auto col-sm-10">
                            <label for="inputpass" class="form-control-label">Confirmar Password</label>
                            <input type="password" class="form-control" id="confirmar_password" name="confirmar_password"
                                   placeholder="Confirmar Password"
                                   required="required">
                    </div>

                </div>
                <div class="form-group col-lg-offset-1">
                    <div class="mx-auto col-sm-10">
                        <?php
                        if (isset($_SESSION["pass"])) {
                            if ($_SESSION["pass"] == 18) {
                                echo "<div class='alert alert-danger'>
                As passwords não coincidem!
          </div>";
                                $_SESSION["pass"] = 12;
                            }
                        }
                        ?>
                    </div>
                </div>
                <div class="form-group col-lg-offset-1">
                    <div class="mx-auto col-sm-10">
                        <label for="inputpass" class="form-control-label">Nome</label>
                        <input type="text" class="form-control" id="nome" name="nome"
                               placeholder="Nome"
                               required="required">
                    </div>
                </div>
                <div class="form-group col-lg-offset-1">
                    <div class="mx-auto col-sm-10">
                        <label for="inputpass" class="form-control-label">Apelido</label>
                        <input type="text" class="form-control" id="apelido" name="apelido"
                               placeholder="Apelido"
                               required="required">
                    </div>
                </div>
                <div class="form-group col-lg-offset-1">
                    <div class="mx-auto col-sm-10">
                        <label for="inputpass" class="form-control-label">Email</label>
                        <input type="email" class="form-control" id="email" name="email"
                               placeholder="Email"
                               required="required">
                    </div>
                </div>
                <div class="form-group col-lg-offset-1">
                    <div class="mx-auto col-sm-10">
                        <label for="inputpass" class="form-control-label">Data de nascimento</label>
                        <input type="date" class="form-control" id="data" name="nascimento"
                               placeholder="Data de Nascimento"
                               required="required">
                    </div>

                </div>
                <div class="form-group col-lg-offset-1">
                    <div class="mx-auto col-sm-10">
                        <?php
                        if (isset($_SESSION["data"])) {
                            if ($_SESSION["data"] == 18) {
                                echo "<div class='alert alert-danger'>
                Data de nascimento inválida!
          </div>";
                                $_SESSION["data"] = 12;
                            }
                        }
                        ?>
                    </div>
                </div>
                <div class="form-group col-lg-offset-1">
                    <div class="mx-auto col-sm-10">
                        <label>País</label>
                        <select class="selectpicker" name="nacionalidade">
                            <?php
                            $link = new_db_connection(); // Create a new DB connection
                            $stmt = mysqli_stmt_init($link); // create a prepared statement
                            $query = "SELECT id_pais, nome FROM pais";
                            if (mysqli_stmt_prepare($stmt, $query)) { // Prepare the statement
                                mysqli_stmt_execute($stmt); // Execute the prepared statement
                                mysqli_stmt_bind_result($stmt, $id_pais, $pais); // Bind results
                                while (mysqli_stmt_fetch($stmt)) { // Fetch values
                                    echo "<option value='$id_pais'>$pais</option>";
                                }
                                mysqli_stmt_close($stmt); // Close statement
                            }
                            mysqli_close($link); // Close connection
                            ?>
                        </select>
                        <br>
                    </div>
                </div>
                <div class="form-group col-lg-offset-1">
                    <div class="mx-auto col-sm-10">
                        <label>Profissão</label>
                        <select class="selectpicker" name="profissao">
                            <?php
                            $link = new_db_connection(); // Create a new DB connection
                            $stmt = mysqli_stmt_init($link); // create a prepared statement
                            $query = "SELECT id_profissao, tipo FROM profissao";
                            if (mysqli_stmt_prepare($stmt, $query)) { // Prepare the statement
                                mysqli_stmt_execute($stmt); // Execute the prepared statement
                                mysqli_stmt_bind_result($stmt, $id_profissao, $tipo); // Bind results
                                while (mysqli_stmt_fetch($stmt)) { // Fetch values
                                    echo "<option value='$id_profissao'>$tipo</option>";
                                }
                                mysqli_stmt_close($stmt); // Close statement
                            }
                            mysqli_close($link); // Close connection
                            ?>
                        </select>
                        <br>
                    </div>
                </div>
                <div class="form-group col-lg-offset-1">
                    <div class="mx-auto col-sm-10">
                        <label>Escolaridade</label>
                        <select class="selectpicker" name="escolaridade">
                            <?php
                            $link = new_db_connection(); // Create a new DB connection
                            $stmt = mysqli_stmt_init($link); // create a prepared statement
                            $query = "SELECT id_escolaridade, tipo FROM escolaridade";
                            if (mysqli_stmt_prepare($stmt, $query)) { // Prepare the statement
                                mysqli_stmt_execute($stmt); // Execute the prepared statement
                                mysqli_stmt_bind_result($stmt, $id_escolaridade, $tipo); // Bind results
                                while (mysqli_stmt_fetch($stmt)) { // Fetch values
                                    echo "<option value='$id_escolaridade'>$tipo</option>";
                                }
                                mysqli_stmt_close($stmt); // Close statement
                            }
                            mysqli_close($link); // Close connection
                            ?>
                        </select>

                    </div>
                </div>
                <div class="form-group col-lg-offset-1">
                    <div class="mx-auto col-sm-10">
                        <?php
                        if (isset($_SESSION["erro"])) {
                            if ($_SESSION["erro"] == 17) {
                                echo "<div class='alert alert-danger'>
                <strong>Erro!</strong> Corrija os seus dados inválidos
          </div>";
                                $_SESSION["erro"] = 12;
                            }
                        }
                        if (isset($_SESSION["mail"])) {
                            if ($_SESSION["mail"] == 19) {
                                echo "<div class='alert alert-danger'>
                <strong>Erro!</strong> Username ou email já registados!
          </div>";
                                $_SESSION["erro"] = 12;
                            }
                        }
                        ?>
                    </div>

                </div>

                <div class="form-group col-lg-offset-1">
                    <div class="mx-auto col-sm-10 pb-3 pt-2">
                        <button type="submit" class="btn btn-outline-secondary btn-lg btn-block col-lg-6 ">
                            Registar
                        </button>

                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
