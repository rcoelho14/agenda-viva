<?php
require_once "components/sql_query.php";


if (isset($_SESSION['username'])) {
    $user = $_SESSION['username'];
    require_once "conections/conections.php";
    $link = new_db_connection();
    $stmt = mysqli_stmt_init($link);
    if (mysqli_stmt_prepare($stmt, $user_data_all_sql)) {
        mysqli_stmt_bind_param($stmt, 's', $_SESSION["username"]);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $id_user, $nome, $apelido, $data_nascimento, $username, $email, $photo_name_uniq, $password_hash, $ref_id_pais, $ref_id_profissao, $ref_id_escolaridade, $pontos_disponiveis, $country, $profissao, $escolaridade);
        mysqli_stmt_fetch($stmt);
    } else {
        mysqli_stmt_close($stmt);
        mysqli_close($link);
        header('Location: ../index.php');
    }
} else
    header('Location: index.php');
?>

<link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
<link rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.2/css/star-rating.min.css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.2/js/star-rating.min.js"></script>


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
    .checked {
        color: orange;
    }
</style>

<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

<link rel="stylesheet" href="css/profile.css">
<link rel="stylesheet" href="js/profile.js">


<section>
    <div class="container main_header">
        <div class="profile-head">
            <div class="col-md- col-sm-4 col-xs-12">
                <img style="width: 70%" src="images/img_profile/<?php echo $photo_name_uniq ?>"
                     class="img-responsive"/>
                <?php
                if (isset($_SESSION["msg"])) {
                    $class = "alert-danger";
                    if ($_SESSION["msg"] == 13) {
                        echo "<div class= 'alert alert-success alert-dismissable ' data-dismiss= 'alert' style='cursor:pointer'>
                            <i class= 'fa fa-check '></i><strong> Sucesso! </strong>A sua fotografia foi atualizada!
          </div>";

                        $_SESSION["msg"] = 12;
                    }
                    if ($_SESSION["msg"] == 14) {
                        echo "<div class= 'alert alert-danger alert-dismissable ' data-dismiss= 'alert' style='cursor:pointer'>
                            <i class= 'fa fa-close '></i><strong> Erro! </strong>Ocorreu um erro!
          </div>";

                        $_SESSION["msg"] = 12;
                    }
                    if ($_SESSION["msg"] == 15) {
                        echo "<div class= 'alert alert-danger alert-dismissable ' data-dismiss= 'alert' style='cursor:pointer'>
                            <i class= 'fa fa-close '></i><strong> Erro! </strong>Formato de imagem não suportado!
          </div>";

                        $_SESSION["msg"] = 12;
                    }
                    if ($_SESSION["msg"] == 16) {
                        echo "<div class= 'alert alert-danger alert-dismissable ' data-dismiss= 'alert' style='cursor:pointer'>
                            <i class= 'fa fa-close '></i><strong> Erro! </strong>Excede o tamanho suportado!
          </div>";
                        $_SESSION["msg"] = 12;
                    }
                }
                ?>
            </div>


            <div class="col-md-5 col-sm-5 col-xs-12">
                <h1><?php echo $username ?></h1>
                <ul style="list-style: none">
                    <li><span class="glyphicon glyphicon-user"></span> <?php echo $nome . "&nbsp;";
                        echo $apelido; ?></li>
                    <li><span class="glyphicon glyphicon-calendar"></span> <?php echo $data_nascimento ?></li>
                    <li><span class="glyphicon glyphicon-globe"></span> <?php echo $country ?></li>
                    <li><span class="glyphicon glyphicon-envelope"></span> <?php echo $email ?></li>
                    <li><span class="glyphicon glyphicon-briefcase"></span> <?php echo $profissao ?></li>
                    <li><span class="glyphicon glyphicon-book
"></span> <?php echo $escolaridade ?></li>
                    <li><span class="glyphicon glyphicon-send
"></span> <?php echo $pontos_disponiveis ?> pontos
                    </li>
                </ul>


            </div><!--col-md-8 col-sm-8 col-xs-12 close-->


        </div><!--profile-head close-->
    </div><!--container close-->

    <p></p>
    <div id="sticky" class="container profile_body">


        <!-- Nav tabs -->
        <ul class="nav nav-tabs nav-menu" role="tablist">
            <li class="active">
                <a href="#profile" role="tab" data-toggle="tab">
                    Reviews
                </a>
            </li>
            <li><a href="#change" role="tab" data-toggle="tab">
                    Bilhetes
                </a>
            </li>
            <li><a role="tab" data-toggle="modal" data-backdrop="static" data-keyboard="false" style="cursor: pointer"
                   data-target="#myModal">
                    Atualizar Perfil
                </a>
            </li>

            <?php

            if (isset($_SESSION['tipo'])) {
                $tipo = $_SESSION['tipo'];


                if ($tipo == 1) {

                    echo "<li><a role='tab' data-toggle='modal' data-backdrop='static' data-keyboard='false' style='cursor: pointer'
                   data-target='#myModal2'>
                    Outras Ações
                </a>
            </li>";

                }
            }
            ?>
        </ul><!--nav-tabs close-->

        <!-- Tab panes -->
        <div class="tab-content">

            <!-- ----------------------------REVIEWS---------------------- -->
            <div class="tab-pane fade active in" id="profile">
                <br clear="all"/>
                <div class="row">
                    <?php

                    $query2 = "SELECT users_geral.id_users_geral, reviews.id_reviews, reviews.rating, reviews.comentario, reviews.date_creation, eventos.nome
FROM reviews
INNER JOIN users_geral
ON reviews.ref_id_users_geral_geral = users_geral.id_users_geral
INNER JOIN eventos
ON reviews.ref_id_eventos = eventos.id_eventos
WHERE users_geral.username = ?
ORDER BY reviews.date_creation ASC";


                    require_once 'conections/conections.php';

                    $link2 = new_db_connection();
                    $stmt2 = mysqli_stmt_init($link2);

                    if (mysqli_stmt_prepare($stmt2, $query2)) {

                        mysqli_stmt_bind_param($stmt2, "s", $user);

                        mysqli_stmt_execute($stmt2);
                        mysqli_stmt_bind_result($stmt2, $id_users_geral, $id_reviews, $rating, $comentario, $data, $event_name);

                        while (mysqli_stmt_fetch($stmt2)) { ?>

                            <div class="col-md-6">

                                <div class="table-responsive responsiv-table">
                                    <table class="table bio-table">
                                        <tr>
                                            <h4><?php echo $event_name ?></h4>
                                            <h5><b>Comentário: </b><?php echo $comentario ?></h5>
                                            <h5><b>Rating: </b>
                                                <?php
                                                switch ($rating) {
                                                    case 0:
                                                        { ?>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <?php
                                                            break;
                                                        }
                                                    case 1:
                                                        { ?>
                                                            <span class="fa fa-star checked"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <?php
                                                            break;
                                                        }
                                                    case 2:
                                                        { ?>
                                                            <span class="fa fa-star checked"></span>
                                                            <span class="fa fa-star checked"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <?php
                                                            break;
                                                        }
                                                    case 3:
                                                        { ?>
                                                            <span class="fa fa-star checked"></span>
                                                            <span class="fa fa-star checked"></span>
                                                            <span class="fa fa-star checked"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <?php
                                                            break;
                                                        }
                                                    case 4:
                                                        { ?>
                                                            <span class="fa fa-star checked"></span>
                                                            <span class="fa fa-star checked"></span>
                                                            <span class="fa fa-star checked"></span>
                                                            <span class="fa fa-star checked"></span>
                                                            <span class="fa fa-star"></span>
                                                            <?php
                                                            break;
                                                        }
                                                    case 5:
                                                        { ?>
                                                            <span class="fa fa-star checked"></span>
                                                            <span class="fa fa-star checked"></span>
                                                            <span class="fa fa-star checked"></span>
                                                            <span class="fa fa-star checked"></span>
                                                            <span class="fa fa-star checked"></span>
                                                            <?php
                                                            break;
                                                        }
                                                }
                                                ?>
                                            </h5>
                                            <h6><b>Data: </b> <?php echo $data ?> </h6>
                                        </tr>
                                        <tr>

                                            <?php
                                            echo "<a onclick='javascript:confirmationDelete($(this));return false;'
                                             href='scripts/user_review_delete_confirm.php?id=" . $id_reviews . "'><i
                                                class='fa fa-trash' title='Delete'></i></a>";
                                            ?>
                                            <script>function confirmationDelete(anchor) {
                                                    var conf = confirm('Tem a certeza que pretende apagar esta review?\nEsta ação é IRREVERSÍVEL!');
                                                    if (conf)
                                                        window.location = anchor.attr('href');
                                                }</script>

                                            <?php
                                            echo "</td>
                                        </tr>";
                                            ?>
                                        </tr>

                                        <hr>
                                    </table>
                                </div><!--table-responsive close-->
                            </div><!--col-md-6 close-->
                            <?php
                        }

                        /* close statement */
                        mysqli_stmt_close($stmt2);
                    }

                    /* close connection */
                    mysqli_close($link2);

                    ?>
                </div>
            </div>


            <!-- -------------------------BILHETES----------------------------------------- -->

            <div class="tab-pane fade" id="change">

                <br clear="all"/>
                <div class="row">

                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>

                                <th>Número de Bilhetes</a></th>
                                <th>Compra finalizada</a></th>
                                <th>Data de Finalização</a></th>
                                <th>Data de Pagamento</a></th>
                                <th>Operações</th>
                            </tr>

                            </thead>
                            <tbody>
                            <?php

                            $query3 = "SELECT bilhetes.id_bilhetes, bilhetes.numero_bilhetes, bilhetes.finalizado, bilhetes.ref_id_user_geral, bilhetes.date_pago, bilhetes.date_finalizado
FROM bilhetes
INNER JOIN users_geral
ON bilhetes.ref_id_user_geral = users_geral.id_users_geral
WHERE users_geral.username = ?
ORDER BY bilhetes.date_pago";


                            require_once 'conections/conections.php';

                            $link3 = new_db_connection();
                            $stmt3 = mysqli_stmt_init($link3);

                            if (mysqli_stmt_prepare($stmt3, $query3)) {

                                mysqli_stmt_bind_param($stmt3, "s", $user);

                                mysqli_stmt_execute($stmt3);
                                mysqli_stmt_bind_result($stmt3, $id_bilhetes, $numero_bilhetes, $bilhetes_finalizado, $ref_id_user_geral, $date_pago, $date_finalizado);

                                while (mysqli_stmt_fetch($stmt3)) { ?>
                                    <?php
                                    echo "<tr><td>$numero_bilhetes</td><td>";
                                    if ($bilhetes_finalizado == 1) {

                                        echo "Sim";
                                    } elseif ($bilhetes_finalizado == 0) {

                                        echo "Não";
                                    }
                                    echo "</td>
                                    <td>$date_finalizado</td>
                                    <td>$date_pago</td>
                                    <td>";

                                    if ($bilhetes_finalizado == 0) {

                                        echo "<a onclick='javascript:confirmationDelete($(this));return false;' href='scripts/user_ticket_delete_confirm.php?id=" . $id_bilhetes . "'><i
                                                                            class='fa fa-trash' title='Delete'></i></a>";
                                        ?>

                                        <script>function confirmationDelete(anchor) {
                                                var conf = confirm('Tem a certeza que pretende apagar este bilhete?\nEsta ação é IRREVERSÍVEL!');
                                                if (conf)
                                                    window.location = anchor.attr('href');
                                            }</script>

                                        <?php
                                        echo "</td>
                                        </tr>";
                                        ?>
                                        <?php
                                    }
                                }
                                /* close statement */
                                mysqli_stmt_close($stmt3);
                            }

                            /* close connection */
                            mysqli_close($link3);

                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <!-- -------------------------MODAL----------------------- -->


            <div id="myModal" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Os seus dados</h4>
                        </div>
                        <div class="modal-body">
                            <form class="form-horizontal" id="edit-form"
                                  action="components/update.php" method="post"
                                  role="form" enctype="multipart/form-data">
                                <fieldset>


                                    <div class="form-group col-md-12 data_user_update">
                                        <div class="form-group">
                                            <img style="width: 30%;"
                                                 src="images/img_profile/<?php echo $photo_name_uniq ?>"
                                                 class="avatar img-fluid" alt="avatar">
                                        </div>
                                        <div class="col-md-12 inputGroupContainer">
                                            <div class="input-group">
                                                <input class="form-control" type="file" id="fileToUpload"
                                                       name="fileToUpload">
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group col-md-12">
                                        <label class="col-md-10 control-label">Nome</label>
                                        <div class="col-md-12 inputGroupContainer">
                                            <div class="input-group">
                                                <input type="text" class="form-control"
                                                       id="input2UserForm"
                                                       name="nome_user"
                                                       placeholder="nome_user"
                                                       required="required"
                                                       value="<?php echo $nome ?>">
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Text input-->

                                    <div class="form-group col-md-12">
                                        <label class="col-md-10 control-label">Apelido</label>
                                        <div class="col-md-12 inputGroupContainer">
                                            <div class="input-group">
                                                <input type="text" class="form-control"
                                                       id="input2UserForm"
                                                       name="apelido_user"
                                                       placeholder="apelido_user"
                                                       required="required"
                                                       value="<?php echo $apelido ?>">
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Text input-->
                                    <div class="form-group col-md-12">
                                        <label class="col-md-10 control-label">E-Mail</label>
                                        <div class="col-md-12 inputGroupContainer">
                                            <div class="input-group">
                                                <input type="email" class="form-control"
                                                       id="input2EmailForm" name="email"
                                                       placeholder="email"
                                                       required="required"
                                                       onchange="email_validate(this.value);"
                                                       value="<?php echo $email ?>">
                                            </div>
                                        </div>
                                    </div>


                                    <!-- Text input-->

                                    <div class="form-group col-md-12">
                                        <label class="col-md-10 control-label">Data de Nascimento</label>
                                        <div class="col-md-12 inputGroupContainer">
                                            <div class="input-group">
                                                <input type="date" class="form-control"
                                                       id="input2UserForm"
                                                       name="data_nascimento"
                                                       placeholder="apelido_user"
                                                       required="required"
                                                       value="<?php echo $data_nascimento ?>">
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Text input-->

                                    <div class="form-group col-md-12">
                                        <label class="col-md-10 control-label">Nacionalidade</label>
                                        <div class="col-md-12 inputGroupContainer">
                                            <div class="input-group">
                                                <select class="selectpicker" name="nacionalidade">
                                                    <?php
                                                    // Create a new DB connection
                                                    $link = new_db_connection();

                                                    /* create a prepared statement */
                                                    $stmt = mysqli_stmt_init($link);


                                                    if (mysqli_stmt_prepare($stmt, $country_dropdown)) {

                                                        /* execute the prepared statement */
                                                        mysqli_stmt_execute($stmt);

                                                        /* bind result variables */
                                                        mysqli_stmt_bind_result($stmt, $id_pais, $nome_pais);

                                                        /* fetch values */
                                                        while (mysqli_stmt_fetch($stmt)) {
                                                            if ($ref_id_pais == $id_pais) {
                                                                $selected = "selected";
                                                            } else {
                                                                $selected = "";
                                                            }
                                                            echo "<option value=\"$id_pais\" $selected>$nome_pais</option>";
                                                        }

                                                        /* close statement */
                                                        mysqli_stmt_close($stmt);
                                                    }
                                                    /* close connection */
                                                    mysqli_close($link);
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-12">
                                        <label class="col-md-10 control-label">Profissão</label>
                                        <div class="col-md-12 inputGroupContainer">
                                            <div class="input-group">
                                                <select class="selectpicker" name="profissao">
                                                    <?php
                                                    // Create a new DB connection
                                                    $link = new_db_connection();

                                                    /* create a prepared statement */
                                                    $stmt = mysqli_stmt_init($link);


                                                    if (mysqli_stmt_prepare($stmt, $profissao_dropdown)) {

                                                        /* execute the prepared statement */
                                                        mysqli_stmt_execute($stmt);

                                                        /* bind result variables */
                                                        mysqli_stmt_bind_result($stmt, $id_profissao, $nome_profissao);

                                                        /* fetch values */
                                                        while (mysqli_stmt_fetch($stmt)) {
                                                            if ($ref_id_profissao == $id_profissao) {
                                                                $selected = "selected";
                                                            } else {
                                                                $selected = "";
                                                            }
                                                            echo "<option value=\"$id_profissao\" $selected>$nome_profissao</option>";
                                                        }

                                                        /* close statement */
                                                        mysqli_stmt_close($stmt);
                                                    }

                                                    /* close connection */
                                                    mysqli_close($link);
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group col-md-12">
                                        <label class="col-md-10 control-label">Escolaridade</label>
                                        <div class="col-md-12 selectContainer">
                                            <div class="input-group">
                                                <select class="selectpicker" name="escolaridade">
                                                    <?php
                                                    // Create a new DB connection
                                                    $link = new_db_connection();

                                                    /* create a prepared statement */
                                                    $stmt = mysqli_stmt_init($link);


                                                    if (mysqli_stmt_prepare($stmt, $escolaridade_dropdown)) {

                                                        /* execute the prepared statement */
                                                        mysqli_stmt_execute($stmt);

                                                        /* bind result variables */
                                                        mysqli_stmt_bind_result($stmt, $id_escolaridade, $nome_escolaridade);

                                                        /* fetch values */
                                                        while (mysqli_stmt_fetch($stmt)) {
                                                            if ($ref_id_escolaridade == $id_escolaridade) {
                                                                $selected = "selected";
                                                            } else {
                                                                $selected = "";
                                                            }
                                                            echo "<option value=\"$id_escolaridade\" $selected>$nome_escolaridade</option>";
                                                        }

                                                        /* close statement */
                                                        mysqli_stmt_close($stmt);
                                                    }

                                                    /* close connection */
                                                    mysqli_close($link);
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- radio checks -->
                                    <div class="form-group col-md-12">
                                    </div>
                                    <!-- upload profile picture -->
                                    <div class="col-md-12 text-left">
                                    </div><!--col-md-12 close-->
                                    <!-- Button -->
                                    <div class="form-group col-md-10">
                                        <div class="col-md-6">
                                            <button type="submit"
                                                    class="btn btn-outline-secondary btn-lg btn-block atualizar_perfil">
                                                Atualizar perfil
                                            </button>

                                        </div>
                                    </div>
                                    <div class="form-group col-md-12">
                                </fieldset>
                            </form>
                        </div>
                    </div>

                </div>
            </div>

            <!-- -------------------------MODAL DELET USER----------------------- -->


            <div id="myModal2" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Outras Ações</h4>
                        </div>
                        <div class="modal-body">
                            <form class="form-horizontal" id="edit-form"
                                  action="components/update.php" method="post"
                                  role="form" enctype="multipart/form-data">
                                <fieldset>
                                    <div class=" col-md-12">
                                        <h4>Ao clicar no botão abaixo, a sua conta será suspensa durante 15
                                            dias.</h4>
                                        <h4>Durante esse tempo poderá requisitar que o processo seja cancelado.</h4>
                                        <h4>Após os 15 dias, a conta será permanentemente eliminada. <br> Esta ação é
                                            <b>IRREVERSÍVEL</b>
                                        </h4>

                                        <?php

                                        require_once "conections/conections.php";

                                        $link4 = new_db_connection();
                                        $stmt4 = mysqli_stmt_init($link4);
                                        $query4 = "SELECT users_geral.id_users_geral, users_geral.activo, tipo.tipo 
                                                    FROM users_geral 
                                                    INNER JOIN tipo
                                                    ON users_geral.ref_id_tipo = tipo.id_tipo
                                                    WHERE users_geral.username = ?";

                                        if (mysqli_stmt_prepare($stmt4, $query4)) {
                                            mysqli_stmt_execute($stmt4);

                                            mysqli_stmt_bind_param($stmt4, "s", $user);

                                            mysqli_stmt_bind_result($stmt4, $id_user_geral, $activo, $tipo);

                                            /* fetch values */
                                            mysqli_stmt_fetch($stmt4) ?>

                                            <?php
                                            echo "<button class=\"btn btn-outline-secondary btn-lg btn-block atualizar_perfil\" onclick='javascript:confirmationDelete($(this));return false;' href='scripts/user_self_delete_confirm.php?id=" . $user . "'>
                                                Eliminar Conta de Utilizador
                                            </button>";
                                            ?>

                                            <script>function confirmationDelete(anchor) {
                                                    var conf = confirm('Tem a certeza que pretende apagar este evento activo?\nEsta ação é IRREVERSÍVEL!');
                                                    if (conf)
                                                        window.location = anchor.attr('href');
                                                }</script>
                                            <?php
                                            /* close statement */
                                            mysqli_stmt_close($stmt4);
                                        }
                                        /* close connection */
                                        mysqli_close($link4);
                                        ?>

                                    </div>
                                    <div class="form-group col-md-12">
                                </fieldset>
                            </form>
                        </div>
                    </div>

                </div>
            </div>

        </div><!--tab-content close-->

    </div>

    </div><!--container close-->
</section><!--section close-->
<p></p>