<div class="col-lg-6 col-12 pb-3 col-lg-offset-3">
    <div class="card h-100">
        <div class="card-body">
            <br>
            <h2 class="text-center mb-4 col-lg-offset-0">Iniciar Sessão</h2>

            <form class="col-lg-offset-2" method="post" role="form" id="register-form" action="components/login.php">
                <div class="form-group">
                    <div class="mx-auto col-sm-10 pb-3 pt-2">
                        <?php
                        if (isset($_SESSION["msg"])) {
                            if ($_SESSION["msg"] == 10) {
                                echo "<div class='alert alert-danger'>
                <strong>Erro!</strong> Ocorreu um erro no login
          </div>";
                                $_SESSION["msg"] = 12;
                            }
                            if ($_SESSION["msg"] == 7) {
                                echo "<div class='alert alert-danger'>
                <strong>Erro!</strong> O username ou password estão incorretos!
          </div>";
                                $_SESSION["msg"] = 12;
                            }
                            if ($_SESSION["msg"] == 8) {
                                echo "<div class='alert alert-danger'>
                <strong>Erro!</strong> Preencha todos os campos!
          </div>";
                                $_SESSION["msg"] = 12;
                            }
                            if ($_SESSION["msg"] == 9) {
                                echo "<div class='alert alert-danger'>
                <strong>Erro!</strong> Conta desativada. Contacte o administrador.
          </div>";
                                $_SESSION["msg"] = 12;
                            }
                            if ($_SESSION["msg"] == 6) {
                                echo "<div class='alert alert-success'>
                <strong>Sucesso!</strong> Password alterada com sucesso!!
          </div>";
                                $_SESSION["msg"] = 12;
                            }
                        }
                        ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="input2EmailForm" class="sr-only form-control-label">Username</label>
                    <div class="mx-auto col-sm-10">
                        <input type="text" class="form-control" id="input2UserForm" name="username"
                               placeholder="Username"
                               required="required">
                    </div>
                </div>
                <div class="form-group">
                    <label for="pass" class="sr-only form-control-label">Password</label>
                    <div class="mx-auto col-sm-10">
                        <input type="password" class="form-control" id="pass" name="password"
                               placeholder="Password"
                               required="required">
                    </div>
                </div>
                <div class="form-group">
                    <div class="mx-auto col-sm-10 pb-3 pt-2">
                        <a href="recuperacao_pass.php">Recuperar Password</a>
                        <br>
                        <button type="submit" class="btn btn-outline-secondary btn-lg btn-block">Login
                        </button>
                        <a href="registo.php">Não tem registo? Registe-se aqui</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>