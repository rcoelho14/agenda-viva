<?php
require_once "components/sql_query.php";


if (isset($_GET['username'])) {
    $user = $_GET['username'];


    require_once "conections/conections.php";
    $link = new_db_connection();
    $stmt = mysqli_stmt_init($link);
    if (mysqli_stmt_prepare($stmt, $public_profile_sql)) {
        mysqli_stmt_bind_param($stmt, 's', $user);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $id_user, $nome, $apelido, $data_nascimento, $username, $email, $photo_name_uniq, $password_hash, $ref_id_pais, $ref_id_profissao, $ref_id_escolaridade, $pontos_disponiveis, $country, $profissao, $escolaridade);
        mysqli_stmt_fetch($stmt);
    } else {
        mysqli_stmt_close($stmt);
        mysqli_close($link);
        header('Location: ../index.php');
    }
} else
    header('Location: index.php');
?>

<link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
<link rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.2/css/star-rating.min.css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.2/js/star-rating.min.js"></script>


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
    .checked {
        color: orange;
    }
</style>

<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

<link rel="stylesheet" href="css/profile.css">
<link rel="stylesheet" href="js/profile.js">


<section>
    <div class="container main_header">
        <div class="profile-head">
            <div class="col-md- col-sm-4 col-xs-12">
                <img style="width: 70%" src="images/img_profile/<?php echo $photo_name_uniq ?>"
                     class="img-responsive"/>
            </div>


            <div class="col-md-5 col-sm-5 col-xs-12">
                <h1><?php echo $username ?></h1>
                <ul style="list-style: none">
                    <li><span class="glyphicon glyphicon-calendar"></span> <?php echo $data_nascimento ?></li>
                    <li><span class="glyphicon glyphicon-globe"></span> <?php echo $country ?></li>
                </ul>


            </div><!--col-md-8 col-sm-8 col-xs-12 close-->


        </div><!--profile-head close-->
    </div><!--container close-->

    <p></p>
    <div id="sticky" class="container profile_body">


        <!-- Nav tabs -->
        <ul class="nav nav-tabs nav-menu" role="tablist">
            <li class="active">
                <a href="#profile" role="tab" data-toggle="tab">
                    Reviews
                </a>
            </li>
        </ul><!--nav-tabs close-->

        <!-- Tab panes -->
        <div class="tab-content">

            <!-- ----------------------------REVIEWS---------------------- -->
            <div class="tab-pane fade active in" id="profile">
                <br clear="all"/>
                <div class="row">
                    <?php

                    $query2 = "SELECT users_geral.id_users_geral, reviews.id_reviews, reviews.rating, reviews.comentario, reviews.date_creation, eventos.nome
FROM reviews
INNER JOIN users_geral
ON reviews.ref_id_users_geral_geral = users_geral.id_users_geral
INNER JOIN eventos
ON reviews.ref_id_eventos = eventos.id_eventos
WHERE users_geral.username = ?
ORDER BY reviews.date_creation ASC";


                    require_once 'conections/conections.php';

                    $link2 = new_db_connection();
                    $stmt2 = mysqli_stmt_init($link2);

                    if (mysqli_stmt_prepare($stmt2, $query2)) {

                        mysqli_stmt_bind_param($stmt2, "s", $user);

                        mysqli_stmt_execute($stmt2);
                        mysqli_stmt_bind_result($stmt2, $id_users_geral, $id_reviews, $rating, $comentario, $data, $event_name);

                        while (mysqli_stmt_fetch($stmt2)) { ?>

                            <div class="col-md-6">

                                <div class="table-responsive responsiv-table">
                                    <table class="table bio-table">
                                        <tr>
                                            <h4><?php echo $event_name ?></h4>
                                            <h5><b>Comentário: </b><?php echo $comentario ?></h5>
                                            <h5><b>Rating: </b>
                                                <?php
                                                switch ($rating) {
                                                    case 0:
                                                        { ?>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <?php
                                                            break;
                                                        }
                                                    case 1:
                                                        { ?>
                                                            <span class="fa fa-star checked"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <?php
                                                            break;
                                                        }
                                                    case 2:
                                                        { ?>
                                                            <span class="fa fa-star checked"></span>
                                                            <span class="fa fa-star checked"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <?php
                                                            break;
                                                        }
                                                    case 3:
                                                        { ?>
                                                            <span class="fa fa-star checked"></span>
                                                            <span class="fa fa-star checked"></span>
                                                            <span class="fa fa-star checked"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <?php
                                                            break;
                                                        }
                                                    case 4:
                                                        { ?>
                                                            <span class="fa fa-star checked"></span>
                                                            <span class="fa fa-star checked"></span>
                                                            <span class="fa fa-star checked"></span>
                                                            <span class="fa fa-star checked"></span>
                                                            <span class="fa fa-star"></span>
                                                            <?php
                                                            break;
                                                        }
                                                    case 5:
                                                        { ?>
                                                            <span class="fa fa-star checked"></span>
                                                            <span class="fa fa-star checked"></span>
                                                            <span class="fa fa-star checked"></span>
                                                            <span class="fa fa-star checked"></span>
                                                            <span class="fa fa-star checked"></span>
                                                            <?php
                                                            break;
                                                        }
                                                }
                                                ?>
                                            </h5>
                                            <h6><b>Data: </b> <?php echo $data ?> </h6>
                                        </tr>

                                        <hr>
                                    </table>
                                </div><!--table-responsive close-->
                            </div><!--col-md-6 close-->
                            <?php
                        }

                        /* close statement */
                        mysqli_stmt_close($stmt2);
                    }

                    /* close connection */
                    mysqli_close($link2);

                    ?>
                </div>
            </div>
        </div><!--container close-->
</section><!--section close-->
<p></p>