<!DOCTYPE HTML>
<html>
<head>
    <?php include "components/meta.php" ?>

    <!-- <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet"> -->
    <!-- <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i" rel="stylesheet"> -->

    <?php include "components/css.php" ?>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        body {
            font-family: Arial, Helvetica, sans-serif;
        }

        input[type=text], select, textarea {
            width: 100%;
            padding: 12px;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
            margin-top: 6px;
            margin-bottom: 16px;
            resize: vertical;
        }

        input[type=submit] {
            background-color: #4CAF50;
            color: white;
            padding: 12px 20px;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }

        input[type=submit]:hover {
            background-color: #45a049;
        }

        .container {
            border-radius: 5px;
            background-color: #f2f2f2;
            padding: 20px;
        }
    </style>

</head>
<body>

<div class="fh5co-loader"></div>

<div id="page" class="">
    <?php include "components/menu.php" ?>

    <br>

    <h2 class="text-center mb-4 col-lg-offset-0">Contactos</h2>
    <div style="background-color: #4cae4c">
        <div style="position: relative; margin-left: 10%; float: left">
            <p><b> Telefone: </b> (+ 351) 234 427 053 </p>
            <p><b> Morada: </b> Fábrica Centro Ciência Viva de Aveiro
                <br>Rua dos Santos Mártires, nº 1A
                <br>3810 - 171 Aveiro
            </p>
            <p><b>E-mail: </b> fabrica.cienciaviva@ua.pt </p>
            <p><b>Website: </b><a href="http://www.fabrica.cienciaviva.ua.pt" style="color: #4cae4c">
                    www.fabrica.cienciaviva.ua.pt</a></p>
            <p><b>Facebook: </b><a href="http://www.facebook.com/FCCVA"
                                   style="color: #4cae4c">www.facebook.com/FCCVA</a></p>
            <p><b>Instagram: </b><a href="http://www.instagram.com/fabricacienciaviva" style="color: #4cae4c">www.instagram.com/fabricacienciaviva</a>
            </p>
        </div>


        <div style="float: right; position: relative; width: 52%;">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d900.1151681069558!2d-8.657670949550043!3d40.63838341180291!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd2398018970f515%3A0x10a737e8a06f4b6a!2sF%C3%A1brica+Centro+Ci%C3%AAncia+Viva+de+Aveiro!5e0!3m2!1spt-PT!2spt!4v1528910881064"
                    width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
    </div>
</div>
<br>
<br>
<h2 class="text-center mb-4 col-lg-offset-0">Deixe aqui o seu comentário</h2>
<div class="container">
    <form method="get" action="scripts/comentario_geral.php">
        <label for="fname">Primeiro Nome</label>
        <input type="text" id="fname" name="firstname" placeholder="Primeiro nome">

        <label for="lname">Último Nome</label>
        <input type="text" id="lname" name="lastname" placeholder="Ultimo nome">

        <label for="email">Email</label>
        <input type="text" id="email" name="email" placeholder="Email">

        <label for="subject">Comentário</label>
        <textarea id="subject" name="comentario" placeholder="Deixe aqui o seu comentário/dúvida"
                  style="height:200px"></textarea>

        <input type="submit" value="Enviar">
    </form>
    <br>
    <br>


</div>
<br>
<br>


<?php include "components/footer.php" ?>
</div>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
</div>

<?php include "components/script.php" ?>

</body>
</html>