<!DOCTYPE HTML>
<html>
<?php include "components/meta.php" ?>

<!-- <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet"> -->
<!-- <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i" rel="stylesheet"> -->

<?php include "components/css.php" ?>


</head>
<body>

<div class="fh5co-loader"></div>

<div id="page" class="">
    <?php include "components/menu.php" ?>

    <?php include "paginas/form.php" ?>
    
    <?php include "components/footer.php" ?>
</div>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
</div>

<?php include "components/script.php" ?>
<?php
if (isset($_GET["msg"])) {
    $msg_show = true;
    switch ($_GET["msg"]) {
        case 0:
            $message = "Ocorreu um erro no login";
            break;
        case 1:
            $message = "O username ou password estão incorretos!";
            break;
        case 2:
            $message = "Preencha todos os campos!";
            break;
        default:
            $msg_show = false;
    }

    echo "
<div class=\"modal fade\" id=\"messageModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"Login\" aria-hidden=\"true\">
    <div class=\"modal-dialog\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                    <span aria-hidden=\"true\">&times;</span>
                </button>
                <h5 class=\"modal-title\"></h5>
            </div>

            <div class=\"modal-body\">
                " . $message . "
                <div id=\"errors\"></div>
            </div>

            <div class=\"modal-footer\">
                <button id='fechar' type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Fechar</button>
            </div>
        </div>
    </div>
</div>
";
    if ($msg_show) {
        echo '<script>window.onload=function (){$("#messageModal").modal("show");}</script>';
    }
}
?>
</body>
</html>