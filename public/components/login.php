<?php
session_start();

if ((isset($_POST['username'])) && (isset($_POST['password']))) {
    require_once "../conections/conections.php";
    $link = new_db_connection();
    $stmt = mysqli_stmt_init($link);
    $query = "  SELECT id_users_geral, ref_id_tipo, password_hash, activo, ref_id_users_geral 
                FROM users_geral 
                INNER JOIN user_public 
                ON users_geral.id_users_geral = user_public.ref_id_users_geral 
                WHERE username 
                LIKE ?";

    if (mysqli_stmt_prepare($stmt, $query)) {
        mysqli_stmt_bind_param($stmt, 's', $username);
        $username = strip_tags($_POST['username']);
        $password = strip_tags($_POST['password']);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $id_users_geral, $perfil, $password_hash, $activo, $ref_id_users_geral);
        if (mysqli_stmt_fetch($stmt)) {

            if ($activo == 1) {

                if (password_verify($password, $password_hash)) {

                    $_SESSION['id_users_geral'] = $id_users_geral;
                    $_SESSION['tipo'] = $perfil;
                    $_SESSION["username"] = $username;
                    $_SESSION["ref_id_users_geral"] = $ref_id_users_geral;
                    mysqli_stmt_close($stmt);
                    mysqli_close($link);
                    header('Location: ../index.php?msg=3');

                } else {
                    mysqli_stmt_close($stmt);
                    mysqli_close($link);
                    $_SESSION["msg"] = 7;
                    header('Location: ../login.php');
                }
            }
            elseif ($activo == 0) {
                mysqli_stmt_close($stmt);
                mysqli_close($link);
                $_SESSION["msg"] = 9;
                header('Location: ../login.php');
            } else {
                $_SESSION["msg"] = 10;
                header('Location: ../login.php');
            }

        } else {
            mysqli_stmt_close($stmt);
            mysqli_close($link);
            $_SESSION["msg"] = 7;
            header('Location: ../login.php');
        }
    } else {
        $_SESSION["msg"] = 10;
        header('Location: ../login.php');
    }

} else {
    $_SESSION["msg"] = 8;
    header('Location: ../login.php');
}
?>