<?php session_start(); ?>
<nav class="fh5co-nav" role="navigation">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-xs-2">
                <a href="index.php"><img src="images/logo.png"></a>
            </div>
            <div class="col-md-8 col-xs-8 text-center menu-1">
                <ul>
                    <li>
                        <a href="index.php">Home</a>
                    </li>
                    <li class="has-dropdown">
                        <a>Atividades</a>
                        <ul class="dropdown">
                            <li><a href="atividades_p.php">Atividades</a></li>
                            <li><a href="atividades_w.php">Workshops</a></li>
                            <li><a href="atividades_e.php">Exposições</a></li>
                            <li><a href="atividades_s.php">Salas</a></li>
                        </ul>
                    </li>
                    <li><a href="agenda.php" >Agenda</a></li>
                    <li><a href="contactos.php">Contactos</a></li>
                   
                    <?php
                    if (isset($_SESSION["username"])) {
                        ?>

                        <li class="has-dropdown">
                            <a><?php echo $_SESSION['username']  ?></a>
                            <ul class="dropdown">
                                <li><a href="user_profile.php"><?php echo "Perfil"?></a></li>
                                <li><a href="../admin/index.php"><?php if (isset($_SESSION['tipo']) && $_SESSION['tipo'] == 2) echo "Admin"?></a></li>

                                <li><a href="components/logout.php">Logout</a></li>
                            </ul>
                        </li>


                        <?php
                    } else
                        echo "<li class=\"has-dropdown\"><a href=\"login.php\">Login</a></li>";
                    ?>
					<?php
                    if (isset($_SESSION['username'])){
                        echo '<li class="shopping-cart">
                        <a href="carrinho.php" class="cart"><span>
                                            <small style="background-color: #4cae4c">0</small>
                                            <i class="icon-shopping-cart"></i></span></a></li>';
                    } else{
                        echo "";
                    }

                    ?>
                </ul>

            </div>

        </div>
</nav>