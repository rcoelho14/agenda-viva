<?php

//users

$user_data_all_sql = "
                    SELECT users_geral.id_users_geral, users_geral.nome, users_geral.apelido, users_geral.data_nascimento, users_geral.username, users_geral.email, users_geral.photo_name_uniq, users_geral.password_hash, user_public.ref_id_pais, user_public.ref_id_profissao, user_public.ref_id_escolaridade, user_public.pontos_disponiveis, pais.nome,profissao.tipo,escolaridade.tipo
                    FROM users_geral
                    INNER JOIN user_public
                    ON users_geral.id_users_geral = user_public.ref_id_users_geral
                    INNER JOIN pais
                    ON user_public.ref_id_pais = pais.id_pais
                    INNER JOIN profissao
                    ON user_public.ref_id_profissao = profissao.id_profissao
                    INNER JOIN escolaridade
                    ON user_public.ref_id_escolaridade = escolaridade.id_escolaridade
                    WHERE username
                    LIKE ?";


$public_profile_sql = "
                    SELECT users_geral.id_users_geral, users_geral.nome, users_geral.apelido, users_geral.data_nascimento, users_geral.username, users_geral.email, users_geral.photo_name_uniq, users_geral.password_hash, user_public.ref_id_pais, user_public.ref_id_profissao, user_public.ref_id_escolaridade, user_public.pontos_disponiveis, pais.nome,profissao.tipo,escolaridade.tipo
                    FROM users_geral
                    INNER JOIN user_public
                    ON users_geral.id_users_geral = user_public.ref_id_users_geral
                    INNER JOIN pais
                    ON user_public.ref_id_pais = pais.id_pais
                    INNER JOIN profissao
                    ON user_public.ref_id_profissao = profissao.id_profissao
                    INNER JOIN escolaridade
                    ON user_public.ref_id_escolaridade = escolaridade.id_escolaridade
                    WHERE username = ?";


//user form dropdown data select

$country_dropdown = "SELECT id_pais, nome FROM pais";

$profissao_dropdown = "SELECT id_profissao, tipo FROM profissao";

$escolaridade_dropdown = "SELECT id_escolaridade, tipo FROM escolaridade";


//
$user_register_insert_users_geral_sql = "
                    INSERT INTO users_geral (username,email,password_hash)
                    VALUES (?,?,?)";


$user_login_sql = "
                    SELECT id_users_geral, password_hash, ref_id_tipo, active
                    FROM users_geral
                    WHERE username = ?";

$user_review_sql;

$user_data_edit_sql;

//admin

$admin_data_sql;

$admin_data_edit_sql;

$admin_review_moderation_sql;

$admin_user_delete_sql;

//eventos

$events_data_sql = "
                    SELECT eventos.id_eventos, eventos.nome, eventos.imagem, eventos.description_short, eventos.description, eventos.lotacao, eventos.preco, eventos_tipo.tipo, salas.nome, eventos_horarios.date, eventos_horarios.hora_inicio, eventos_horarios.duracao, eventos_horarios.bilhetes_disponiveis, eventos_horarios.date_creation
                    FROM eventos
                    INNER JOIN salas_has_eventos
                    ON salas_has_eventos.ref_id_eventos = eventos.id_eventos
                    INNER JOIN salas
                    ON salas_has_eventos.ref_id_salas = salas.id_salas
                    INNER JOIN eventos_tipo
                    ON eventos_tipo.id_eventos_tipo = eventos.ref_id_eventos_tipo
                    INNER JOIN eventos_horarios
                    ON eventos_horarios.ref_id_eventos = eventos.id_eventos
                  ";

$shopping_cart_sql;

$events_data_edit_sql;