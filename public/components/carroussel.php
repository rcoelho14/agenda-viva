<br>
<div class="col-md-10 col-md-offset-1 animate-box">
    <div class="owl-carousel owl-carousel-fullwidth product-carousel">
        <div class="item">
            <div class="active text-center">
                <figure>
                    <img src="images/nova_1.jpg" alt="user">
                </figure>
            </div>
        </div>
        <div class="item">
            <div class="active text-center">
                <figure>
                    <img src="images/nova.png" alt="user">
                </figure>
            </div>
        </div>
    </div>
</div>
