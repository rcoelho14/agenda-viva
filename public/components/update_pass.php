<?php
session_start();
require_once "../conections/conections.php";
if ((isset($_POST["password"])) && (isset($_POST["confirmar_password"])) && (isset($_GET["form"]))) {
    $form = $_GET["form"];
    $pass = $_POST["password"];
    if ($_POST["password"] == $_POST["confirmar_password"]) {
        $link = new_db_connection();
        $stmt = mysqli_stmt_init($link);
        $query = "SELECT users_geral_id_users_geral FROM recupera_pass  WHERE codigo2=?";
        if (mysqli_stmt_prepare($stmt, $query)) {
            mysqli_stmt_bind_param($stmt, 's', $form);
            mysqli_stmt_bind_result($stmt, $id_users_geral);
            mysqli_stmt_execute($stmt);
            if (mysqli_stmt_fetch($stmt)) {
                $link3 = new_db_connection();
                $stmt3 = mysqli_stmt_init($link3);
                $query3 = "UPDATE users_geral SET password_hash = ? WHERE users_geral.id_users_geral=?";

                if (mysqli_stmt_prepare($stmt3, $query3)) {
                    mysqli_stmt_bind_param($stmt3, 'ss', $password_hash, $id_users_geral);
                    $password_hash = password_hash($pass, PASSWORD_DEFAULT);
                    mysqli_stmt_execute($stmt3);
                    mysqli_stmt_close($stmt3);
                    $link2 = new_db_connection();
                    $stmt2 = mysqli_stmt_init($link2);
                    $query2 = "DELETE FROM recupera_pass WHERE recupera_pass.codigo2=?";
                    if (mysqli_stmt_prepare($stmt2, $query2)) {
                        mysqli_stmt_bind_param($stmt2, 's', $form);
                        mysqli_stmt_execute($stmt2);
                        mysqli_stmt_close($stmt2);
                        $_SESSION["msg"] = 6;
                        mysqli_close($link2);
                        mysqli_close($link3);
                        mysqli_close($link);
                        header ("Location: ../login.php");
                    } else {
                        $_SESSION["msg"] = 5;
                        mysqli_close($link2);
                        mysqli_close($link3);
                        mysqli_close($link);
                        header("Location: ../nova_pass.php?novo=$form");
                    }
                } else {
                    $_SESSION["msg"] = 5;
                    header("Location: ../nova_pass.php?novo=$form");
                    mysqli_close($link);
                    mysqli_close($link3);
                }
                mysqli_close($link);
            } else {
                $_SESSION["msg"] = 5;
                header("Location: ../nova_pass.php?novo=$form");
                mysqli_close($link);

            }

            mysqli_stmt_close($stmt);
        }
        else {
            $_SESSION["msg"] = 5;
            mysqli_close($link);
            header("Location: ../nova_pass.php?novo=$form");
        }

    }
    else {
        $_SESSION["msg"] = 5;
        header("Location: ../nova_pass.php?novo=$form");
    }

}
