


        <div class="table table-responsive " style="overflow-x:auto; width:70%; margin-left:auto; margin-right: auto; font-family: 'icomoon' !important">
        <table class="table table-condensed">



        <tr>
             <th width="40%">Evento</th>
             <th width="20%">Quantidade</th>

             <th width="20%">Preço</th>
             <th width="20%">Total</th>
             <th width="10%">Acção</th>
        </tr>
        <?php

        $product_ids = array();
        //session_destroy();

        //check if Add to Cart button has been submitted
        if (filter_input(INPUT_POST, 'add_to_cart')) {
            if (isset($_SESSION['shopping_cart'])) {

                //keep track of how mnay products are in the shopping cart
                $count = count($_SESSION['shopping_cart']);

                //create sequential array for matching array keys to products id's
                $product_ids = array_column($_SESSION['shopping_cart'], 'id');

                if (!in_array(filter_input(INPUT_GET, 'id'), $product_ids)) {
                    $_SESSION['shopping_cart'][$count] = array
                    (
                        'id_eventos' => filter_input(INPUT_GET, 'id'),
                        'nome' => filter_input(INPUT_POST, 'nome'),
                        'preco' => filter_input(INPUT_POST, 'preco'),
                        'quantity' => filter_input(INPUT_POST, 'quantity')
                        /* 'date' => filter_input(INPUT_POST, 'date'),
                         'hora_inicio' => filter_input(INPUT_POST, 'hora_inicio'),
                         'lotacao' => filter_input(INPUT_POST, 'lotacao'),
                         'bilhetes_disponiveis' => filter_input(INPUT_POST, 'bilhetes_disponiveis'),
                         'duracao' => filter_input(INPUT_POST, 'duracao') */
                    );
                } else { //product already exists, increase quantity
                    //match array key to id of the product being added to the cart
                    for ($i = 0; $i < count($product_ids); $i++) {
                        if ($product_ids[$i] == filter_input(INPUT_GET, 'id')) {
                            //add item quantity to the existing product in the array
                            $_SESSION['shopping_cart'][$i]['quantity'] += filter_input(INPUT_POST, 'quantity');
                        }
                    }
                }

            } else { //if shopping cart doesn't exist, create first product with array key 0
                //create array using submitted form data, start from key 0 and fill it with values
                $_SESSION['shopping_cart'][0] = array
                (
                    'id_eventos' => filter_input(INPUT_GET, 'id'),
                    'nome' => filter_input(INPUT_POST, 'nome'),
                    'preco' => filter_input(INPUT_POST, 'preco'),
                    'quantity' => filter_input(INPUT_POST, 'quantity')
                    /*  'date' => filter_input(INPUT_POST, 'date'),
                      'hora_inicio' => filter_input(INPUT_POST, 'hora_inicio'),
                      'lotacao' => filter_input(INPUT_POST, 'lotacao'),
                      'bilhetes_disponiveis' => filter_input(INPUT_POST, 'bilhetes_disponiveis'),
                      'duracao' => filter_input(INPUT_POST, 'duracao')*/
                );
            }
        }

        if (filter_input(INPUT_GET, 'action') == 'delete') {
            //loop through all products in the shopping cart until it matches with GET id variable
            foreach ($_SESSION['shopping_cart'] as $key => $product) {
                if ($product['id_eventos'] == filter_input(INPUT_GET, 'id')) {
                    //remove product from the shopping cart when it matches with the GET id
                    unset($_SESSION['shopping_cart'][$key]);
                }
            }
            //reset session array keys so they match with $product_ids numeric array
            $_SESSION['shopping_cart'] = array_values($_SESSION['shopping_cart']);
        }

        if(!empty($_SESSION['shopping_cart'])):

             $total = 0;

             foreach($_SESSION['shopping_cart'] as $key => $product):
        ?>
        <tr>
           <td><?php echo $product['nome']; ?></td>
           <td><?php echo $product['quantity']; ?></td>
           <td><?php echo $product['preco']; ?> €</td>
           <td><?php echo number_format($product['quantity'] * $product['preco'], 2);?>€</td>
           <td><center>
               <a href="carrinho.php?action=delete&id=<?php echo $product['id_eventos']; ?>">
                    <div class="btn btn-danger">Remover</div>
               </a>
           </td>
        </tr>
        <?php
                  $total = $total + ($product['quantity'] * $product['preco']);
             endforeach;
        ?>
        <tr>
             <td colspan="3">Total</td>
             <td><?php echo number_format($total, 2); ?>€</td>
            <td colspan="5">

            <!-- Show checkout button only if the shopping cart is not empty -->

             <?php
                if (isset($_SESSION['shopping_cart'])):
                if (count($_SESSION['shopping_cart']) > 0):
             ?>
             <?php
                if (stripos($_SERVER['REQUEST_URI'], 'checkout.php')){
             echo '<a href="#" class="btn btn-primary">Comprar</a>';
                }
                else{
             ?> <a href="checkout.php?qt=<?php echo $product['quantity']?>" class="btn btn-primary"> Checkout</a>
               <?php }

             ?>

             <?php endif; endif; ?>
            </td>
        </tr>
        <?php
        endif;
        ?>
        </table>
         </div>

