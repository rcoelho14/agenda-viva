<?php


$connect = mysqli_connect('labmm.clients.ua.pt', 'deca_17L4_23_dbo', '005710', 'deca_17L4_23');
/*$connect = mysqli_connect('localhost', 'root', '', 'deca_17l4_22');*/
$product_ids = array();
//session_destroy();

//check if Add to Cart button has been submitted
if(filter_input(INPUT_POST, 'add_to_cart')){
    if(isset($_SESSION['shopping_cart'])){

        //keep track of how mnay products are in the shopping cart
        $count = count($_SESSION['shopping_cart']);

        //create sequential array for matching array keys to products id's
        $product_ids = array_column($_SESSION['shopping_cart'], 'id');

        if (!in_array(filter_input(INPUT_GET, 'id'), $product_ids)){
            $_SESSION['shopping_cart'][$count] = array
            (
                'id_eventos' => filter_input(INPUT_GET, 'id'),
                'nome' => filter_input(INPUT_POST, 'nome'),
                'preco' => filter_input(INPUT_POST, 'preco'),
                'quantity' => filter_input(INPUT_POST, 'quantity')
                /* 'date' => filter_input(INPUT_POST, 'date'),
                 'hora_inicio' => filter_input(INPUT_POST, 'hora_inicio'),
                 'lotacao' => filter_input(INPUT_POST, 'lotacao'),
                 'bilhetes_disponiveis' => filter_input(INPUT_POST, 'bilhetes_disponiveis'),
                 'duracao' => filter_input(INPUT_POST, 'duracao') */
            );
        }
        else { //product already exists, increase quantity
            //match array key to id of the product being added to the cart
            for ($i = 0; $i < count($product_ids); $i++){
                if ($product_ids[$i] == filter_input(INPUT_GET, 'id')){
                    //add item quantity to the existing product in the array
                    $_SESSION['shopping_cart'][$i]['quantity'] += filter_input(INPUT_POST, 'quantity');
                }
            }
        }

    }
    else { //if shopping cart doesn't exist, create first product with array key 0
        //create array using submitted form data, start from key 0 and fill it with values
        $_SESSION['shopping_cart'][0] = array
        (
            'id_eventos' => filter_input(INPUT_GET, 'id'),
            'nome' => filter_input(INPUT_POST, 'nome'),
            'preco' => filter_input(INPUT_POST, 'preco'),
            'quantity' => filter_input(INPUT_POST, 'quantity')
            /*  'date' => filter_input(INPUT_POST, 'date'),
              'hora_inicio' => filter_input(INPUT_POST, 'hora_inicio'),
              'lotacao' => filter_input(INPUT_POST, 'lotacao'),
              'bilhetes_disponiveis' => filter_input(INPUT_POST, 'bilhetes_disponiveis'),
              'duracao' => filter_input(INPUT_POST, 'duracao')*/
        );
    }
}

if(filter_input(INPUT_GET, 'action') == 'delete'){
    //loop through all products in the shopping cart until it matches with GET id variable
    foreach($_SESSION['shopping_cart'] as $key => $product){
        if ($product['id_eventos'] == filter_input(INPUT_GET, 'id')){
            //remove product from the shopping cart when it matches with the GET id
            unset($_SESSION['shopping_cart'][$key]);
        }
    }
    //reset session array keys so they match with $product_ids numeric array
    $_SESSION['shopping_cart'] = array_values($_SESSION['shopping_cart']);
}

//pre_r($_SESSION);

function pre_r($array){
    echo '<pre>';
    print_r($array);
    echo '</pre>';
}
?>

<!-- Tabela -->
<?php    if(isset($_SESSION['shopping_cart'])){

}
else {
    null;
}

?>

<!-- Cards -->
<?php
if (isset($_SESSION['username'])){
    echo '<div id="fh5co-product">
    <div class="container">
        <div class="row animate-box">
            <div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
                <span>Agenda Viva</span>
                <h2>Destaques</h2>
            </div>
        </div>
        <div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
            <div class="col-md-4 text-center animate-box">
                <div class="product">
                    <div class="product-grid" style="background-image:url(images/atividades/home_1.png);">
                        <div class="inner">
                            <p>
                                <a href="carrinho.php" class="icon"><i class="icon-shopping-cart"></i></a>
                                <a href="atividades_w.php" class="icon"><i class="icon-eye"></i></a>
                            </p>
                        </div>
                    </div>
                    <div class="desc">
                        <h3><a>ARTE EM PLÁSTICO </a></h3>
                        <span class="price">Vamos juntar arte à reciclagem de plásticos!</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 text-center animate-box">
                <div class="product">
                    <div class="product-grid" style="background-image:url(images/atividades/home_2.jpg);">

                        <div class="inner">
                            <p>
                                <a href="carrinho.php" class="icon"><i class="icon-shopping-cart"></i></a>
                                <a href="atividades_p.php" class="icon"><i class="icon-eye"></i></a>
                            </p>
                        </div>
                    </div>
                    <div class="desc">
                        <h3><a>BABYSITTING DE CIÊNCIA</a></h3>
                        <span class="price">Vamos cuidar de ti com ciência!</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 text-center animate-box">
                <div class="product">
                    <div class="product-grid" style="background-image:url(images/atividades/home_3.jpg);">
                        <div class="inner">
                            <p>
                                <a href="carrinho.php" class="icon"><i class="icon-shopping-cart"></i></a>
                                <a href="atividades_p.php" class="icon"><i class="icon-eye"></i></a>
                            </p>
                        </div>
                    </div>
                    <div class="desc">
                        <h3><a>BARRIGA DO CARACOL</a></h3>
                        <span class="price">Dentro de um caracol contam-se histórias.</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
            <div class="col-md-4 text-center animate-box">
                <div class="product">
                    <div class="product-grid" style="background-image:url(images/atividades/home_4.png);">
                        <div class="inner">
                            <p>
                                <a href="carrinho.php" class="icon"><i class="icon-shopping-cart"></i></a>
                                <a href="atividades_e.php" class="icon"><i class="icon-eye"></i></a>
                            </p>
                        </div>
                    </div>
                    <div class="desc">
                        <h3><a>VEM FAZER UM HOLOGRAMA</a></h3>
                        <span class="price">Vamos perceber o que é um holograma!</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 text-center animate-box">
                <div class="product">
                    <div class="product-grid" style="background-image:url(images/atividades/home_5.jpg);">
                        <div class="inner">
                            <p>
                                <a href="carrinho.php" class="icon"><i class="icon-shopping-cart"></i></a>
                                <a href="atividades_p.php" class="icon"><i class="icon-eye"></i></a>
                            </p>
                        </div>
                    </div>
                    <div class="desc">
                        <h3><a>MÃOS NA MASSA</a></h3>
                        <span class="price">Vamos brincar com as leis da ótica!</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 text-center animate-box">
                <div class="product">
                    <div class="product-grid" style="background-image:url(images/atividades/home_6.jpg);">
                        <div class="inner">
                            <p>
                                <a href="carrinho.php" class="icon"><i class="icon-shopping-cart"></i></a>
                                <a href="atividades_s.php" class="icon"><i class="icon-eye"></i></a>
                            </p>
                        </div>
                    </div>
                    <div class="desc">
                        <h3><a>SALA DA ROBÓTICA</a></h3>
                        <span class="price">Vamos construir um robô?</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>';
} else{
    echo '<div id="fh5co-product">
    <div class="container">
        <div class="row animate-box">
            <div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
                <span>Agenda Viva</span>
                <h2>Destaques</h2>
            </div>
        </div>
        <div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
            <div class="col-md-4 text-center animate-box">
                <div class="product">
                    <div class="product-grid" style="background-image:url(images/atividades/home_1.png);">
                        <div class="inner">
                            <p>

                                <a href="atividades_w.php" class="icon"><i class="icon-eye"></i></a>
                            </p>
                        </div>
                    </div>
                    <div class="desc">
                        <h3><a>ARTE EM PLÁSTICO </a></h3>
                        <span class="price">Vamos juntar arte à reciclagem de plásticos!</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 text-center animate-box">
                <div class="product">
                    <div class="product-grid" style="background-image:url(images/atividades/home_2.jpg);">

                        <div class="inner">
                            <p>

                                <a href="atividades_p.php" class="icon"><i class="icon-eye"></i></a>
                            </p>
                        </div>
                    </div>
                    <div class="desc">
                        <h3><a>BABYSITTING DE CIÊNCIA</a></h3>
                        <span class="price">Vamos cuidar de ti com ciência!</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 text-center animate-box">
                <div class="product">
                    <div class="product-grid" style="background-image:url(images/atividades/home_3.jpg);">
                        <div class="inner">
                            <p>

                                <a href="atividades_p.php" class="icon"><i class="icon-eye"></i></a>
                            </p>
                        </div>
                    </div>
                    <div class="desc">
                        <h3><a>BARRIGA DO CARACOL</a></h3>
                        <span class="price">Dentro de um caracol contam-se histórias.</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
            <div class="col-md-4 text-center animate-box">
                <div class="product">
                    <div class="product-grid" style="background-image:url(images/atividades/home_4.png);">
                        <div class="inner">
                            <p>

                                <a href="atividades_e.php" class="icon"><i class="icon-eye"></i></a>
                            </p>
                        </div>
                    </div>
                    <div class="desc">
                        <h3><a>VEM FAZER UM HOLOGRAMA</a></h3>
                        <span class="price">Vamos perceber o que é um holograma!</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 text-center animate-box">
                <div class="product">
                    <div class="product-grid" style="background-image:url(images/atividades/home_5.jpg);">
                        <div class="inner">
                            <p>

                                <a href="atividades_p.php" class="icon"><i class="icon-eye"></i></a>
                            </p>
                        </div>
                    </div>
                    <div class="desc">
                        <h3><a>MÃOS NA MASSA</a></h3>
                        <span class="price">Vamos brincar com as leis da ótica!</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 text-center animate-box">
                <div class="product">
                    <div class="product-grid" style="background-image:url(images/atividades/home_6.jpg);">
                        <div class="inner">
                            <p>
                                
                                <a href="atividades_s.php" class="icon"><i class="icon-eye"></i></a>
                            </p>
                        </div>
                    </div>
                    <div class="desc">
                        <h3><a>SALA DA ROBÓTICA</a></h3>
                        <span class="price">Vamos construir um robô?</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>';
}

?>


