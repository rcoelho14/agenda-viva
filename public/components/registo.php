<?php
session_start();
require_once "../conections/conections.php";

if ((isset($_POST['username'])) && (isset($_POST['password'])) && (isset($_POST['confirmar_password'])) && (isset($_POST['nome'])) && (isset($_POST['apelido'])) && (isset($_POST['email'])) && (isset($_POST['nascimento'])) && (isset($_POST['escolaridade'])) && (isset($_POST['nacionalidade'])) && (isset($_POST['profissao']))) {

    $data = date('Y-m-d');
    $nascimento = strip_tags($_POST["nascimento"]);
    $pass = strip_tags($_POST['password']);
    $confirmar = strip_tags($_POST['confirmar_password']);
    if ($data < $nascimento || $pass != $confirmar) {
        if ($data < $nascimento) {
            $_SESSION["data"] = 18;
        }
        if ($pass != $confirmar) {
            $_SESSION["pass"] = 18;
        }
        $_SESSION["erro"] = 17;

        echo "<script>javascript:history.back(-1)</script>";
    } else {
        $username = strip_tags($_POST['username']);
        $email = strip_tags($_POST['email']);
        $password_hash = password_hash($_POST['password'], PASSWORD_DEFAULT);
        $nome = strip_tags($_POST["nome"]);
        $apelido = strip_tags($_POST["apelido"]);
        $data_nascimento = strip_tags($_POST["nascimento"]);
        $profissao = strip_tags($_POST["profissao"]);
        $escolaridade = strip_tags($_POST["escolaridade"]);
        $nacionalidade = strip_tags($_POST["nacionalidade"]);
        /*$link4 = new_db_connection();
        $stmt4 = mysqli_stmt_init($link4);
        $query4 = "SELECT username FROM users_geral WHERE username LIKE ?";
        if (mysqli_stmt_prepare($stmt4, $query4)) {
            mysqli_stmt_bind_param($stmt4, 's', $username);
            mysqli_stmt_bind_result($stmt4, $user_existe);
            mysqli_stmt_execute($stmt4);
            while (mysqli_stmt_fetch($stmt4)) {
                if ($username == $user_existe) {
                    echo "<script>javascript:history.back(-1)</script>";
                }
            }
        }*/
        $link = new_db_connection();
        $link2 = new_db_connection();
        $link3 = new_db_connection();
        $stmt = mysqli_stmt_init($link);
        $stmt2 = mysqli_stmt_init($link2);
        $stmt3 = mysqli_stmt_init($link3);


        $query = "INSERT INTO users_geral (email, username, password_hash, nome, apelido, data_nascimento) VALUES (?,?,?,?,?,?)";
        $select_id = "SELECT id_users_geral, ref_id_tipo FROM users_geral WHERE username=?";
        $query2 = "INSERT INTO user_public (ref_id_users_geral, ref_id_pais, ref_id_profissao, ref_id_escolaridade) VALUES (?,?,?,?)";
        if (mysqli_stmt_prepare($stmt, $query)) {
            mysqli_stmt_bind_param($stmt, 'ssssss', $email, $username, $password_hash, $nome, $apelido, $data_nascimento);

            // devemos validar também o resultado do execute!
            if (mysqli_stmt_execute($stmt)) {
                if (mysqli_stmt_prepare($stmt2, $select_id)) {
                    mysqli_stmt_bind_param($stmt2, 's', $username);
                    mysqli_stmt_bind_result($stmt2, $id_users_geral, $perfil);
                    if (mysqli_stmt_execute($stmt2)) {
                        while (mysqli_stmt_fetch($stmt2)) {

                            // Fetch values
                            if (mysqli_stmt_prepare($stmt3, $query2)) {
                                mysqli_stmt_bind_param($stmt3, 'iiii', $id_users_geral, $nacionalidade, $profissao, $escolaridade);
                                if (mysqli_stmt_execute($stmt3)) {
                                    session_start();
                                    $_SESSION['tipo'] = $perfil;
                                    $_SESSION["username"] = $username;
                                    mysqli_stmt_close($stmt);
                                    mysqli_stmt_close($stmt2);
                                    mysqli_stmt_close($stmt3);
                                    mysqli_close($link);
                                    mysqli_close($link2);
                                    mysqli_close($link3);
                                    header('Location: ../index.php?msg=1');
                                }
                            }
                        }
                    }
                }

            } else {
                mysqli_stmt_close($stmt);
                mysqli_close($link);
                $_SESSION["mail"] = 19;

                echo "<script>javascript:history.back(-1)</script>";
            }
            // Acção de sucesso

        } else {
            mysqli_close($link);
            echo "<script>javascript:history.back(-1)</script>";
        }
    }
} else {
    header('Location: ../registo.php');
}

