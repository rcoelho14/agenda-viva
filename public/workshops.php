<!DOCTYPE HTML>
<html>
<head>
    <?php include "components/meta.php" ?>
    <?php include "components/css.php" ?>


    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.2/css/star-rating.min.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.2/js/star-rating.min.js"></script>


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        .checked {
            color: orange;
        }
    </style>

</head>
<body>


<div id="page">

    <?php include "components/menu.php" ?>
    <div class="col-lg-6 col-12 pb-3 col-lg-offset-3">

        <br>
        <h2 class="text-center mb-4 col-lg-offset-0">Workshops</h2>
        <div class="row text-center">


            <?php
            $id = $_GET['id'];

            $query = "SELECT nome, imagem, description, eventos.lotacao, preco, eventos_horarios.duracao, eventos_horarios.hora_inicio, eventos_horarios.bilhetes_disponiveis, eventos_horarios.date
FROM eventos
INNER JOIN eventos_horarios ON eventos_horarios.ref_id_eventos = eventos.id_eventos
WHERE id_eventos=?";


            include_once "conections/conections.php";

            $link = new_db_connection();
            $stmt = mysqli_stmt_init($link);

            if (mysqli_stmt_prepare($stmt, $query)) {

                mysqli_stmt_bind_param($stmt, "s", $id);

                mysqli_stmt_execute($stmt);
                mysqli_stmt_bind_result($stmt, $nome, $imagem, $desc, $lotacao, $preco, $duracao, $hora, $bilhetes, $data);

                while (mysqli_stmt_fetch($stmt)) { ?>

                    <div class="card">
                        <hr>
                        <img class="card-img-top" src="../admin/images/eventos<?php echo $imagem ?>" alt="">
                        <div class="card-body" style="
                                margin-left: 10px;
                                margin-right: 10px;">
                            <br>
                            <h4><?php echo $nome ?></h4>
                            <p></p> <?php echo $desc ?></p>
                            <p><b>Data: </b><?php echo $data ?>
                                <br><b>Inicio:</b> <?php echo $hora ?>h
                                <br><b>Duração: </b> <?php echo $duracao ?>h
                                <br><b>Preço:</b> <?php echo $preco ?>€
                                <br><b>Lotação:</b> <?php echo $lotacao ?>
                                <br><b>Bilhetes disponiveis:</b> <?php echo $bilhetes ?></p>

                            <a href="atividades_p.php" class="btn btn-primary">Voltar</a>
                            <a href="#" class="btn btn-primary">Agenda</a>
                        </div>
                    </div>
                    <?php
                }
                mysqli_stmt_close($stmt);
            }
            mysqli_close($link);

            ?>


        </div>
    </div>
</div>
<br>
<br>
<?php if (isset($_SESSION["username"])) { ?>
    <form class="form-horizontal" method="POST" action="scripts/add_comentarios_workshops.php?id=<?php echo $id ?>">
        <h2 class="text-center mb-4 col-lg-offset-0">Deixe aqui a sua review</h2>
        <div class="container">
            <div class="form-group col-sm-10">
                <label for="inputPassword" class="">Coment&aacute;rio</label>
                <textarea name="comentario" class="form-control" id="comentario"></textarea>
                <br>
                <input id="input-3" name="rating" class="rating rating-loading" data-min="0" data-max="5" data-step="1"
                       value="5">

            </div>


            <div class="form-group col-sm-10">
                <button type="submit" class="btn btn-default">Submeter</button>
            </div>
        </div>
    </form>
<?php } ?>
<br>
<h2 class="text-center mb-4 col-lg-offset-0">Todas as reviews</h2>
<div class="container">
    <?php
    $query2 = "SELECT users_geral.username, reviews.id_reviews, reviews.ref_id_users_geral_geral, reviews.rating, reviews.comentario, reviews.date_creation, eventos.nome
                                                                FROM reviews
                                                                INNER JOIN users_geral
                                                                ON reviews.ref_id_users_geral_geral = users_geral.id_users_geral
                                                                INNER JOIN eventos
                                                                ON reviews.ref_id_eventos = eventos.id_eventos
                                                                WHERE ref_id_eventos = ?
                                                                ORDER BY reviews.date_creation ASC";

    include_once "conections/conections.php";

    $link = new_db_connection();
    $stmt = mysqli_stmt_init($link);

    if (mysqli_stmt_prepare($stmt, $query2)) {

        mysqli_stmt_bind_param($stmt, "s", $id);

        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $username, $id_reviews, $ref_id_users_geral_geral, $r, $comentario, $data, $event_name);

        while (mysqli_stmt_fetch($stmt)) { ?>

            <div class="card text-center">
                <h5><b>Username: </b> <a
                            href='public_profile.php?username=<?php echo $username ?>'> <?php echo $username ?></a>
                </h5>
                <h5><b>Comentário: </b><?php echo $comentario ?></h5>
                <h5><b>Rating: </b>

                    <?php
                    switch ($r) {
                        case 0:
                            { ?>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <?php
                                break;
                            }
                        case 1:
                            { ?>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <?php
                                break;
                            }
                        case 2:
                            { ?>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <?php
                                break;
                            }
                        case 3:
                            { ?>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <?php
                                break;
                            }
                        case 4:
                            { ?>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star"></span>
                                <?php
                                break;
                            }
                        case 5:
                            { ?>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <?php
                                break;
                            }
                    }

                    ?>

                </h5>
                <h6><b>Data: </b> <?php echo $data ?> </h6>
                <hr>
            </div>
            <?php
        }
        mysqli_stmt_close($stmt);
    }
    mysqli_close($link);

    ?>
</div>

<br>
<?php include "components/footer.php" ?>


<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
</div>

<?php include "components/script.php" ?>


</body>
</html>