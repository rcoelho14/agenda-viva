<!DOCTYPE HTML>
<html
<head>
    <?php include "components/meta.php" ?>

    <!-- <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet"> -->
    <!-- <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i" rel="stylesheet"> -->

    <?php include "components/css.php" ?>


</head>
<body>

<div class="fh5co-loader"></div>

<div id="page">
    <?php include "components/menu.php" ?>

    <?php include "paginas/publico_profile.php" ?>

    <?php include "components/footer.php" ?>
</div>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
</div>

<?php include "components/script.php" ?>

</body>
</html>