<!DOCTYPE HTML>
<html>
<head>
    <?php include "components/meta.php"?>

    <!-- <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet"> -->
    <!-- <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i" rel="stylesheet"> -->

    <?php include "components/css.php"?>


</head>
<body>

<div id="page">

    <?php include "components/menu.php" ;
    ?>


    <br>
    <h2 class="text-center mb-4 col-lg-offset-0">Checkout do Carrinho de Compras</h2>
    <br>

<?php
    include_once 'components/cart_details.php';?>
<div class="col-lg-6 col-12 pb-3 col-lg-offset-3">

    <?php include_once 'components/checkout_stats.php';

    ?>
</div>
</div>
</div>
    <?php include "components/footer.php"?>
</div>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
</div>

<?php include "components/script.php"?>
<?php
if (isset($_GET["msg"])) {
    $msg_show = true;
    switch ($_GET["msg"]) {
        case 0:
            $message = "Preencha todos os campos!";
            break;
        case 1:
            $message = "Registo efectuado com sucesso";
            break;
        case 2:
            $message = "Ocorreu um erro no login";
            break;
        case 3:
            $message = "Login efectuado com sucesso";
            break;
        default:
            $msg_show = false;
    }

    echo "
<div class=\"modal fade\" id=\"messageModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"Login\" aria-hidden=\"true\">
    <div class=\"modal-dialog\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                    <span aria-hidden=\"true\">&times;</span>
                </button>
                <h5 class=\"modal-title\"></h5>
            </div>

            <div class=\"modal-body\">
                " . $message . "
                <div id=\"errors\"></div>
            </div>

            <div class=\"modal-footer\">
                <button id='fechar' type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Fechar</button>
            </div>
        </div>
    </div>
</div>
";
    if ($msg_show) {
        echo '<script>window.onload=function (){$("#messageModal").modal("show");}</script>';
    }
}
?>

</body>
</html>