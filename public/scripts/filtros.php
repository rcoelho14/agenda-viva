<?php
if(isset($_POST["idade"])&& isset($_POST["interesse"])&& isset($_POST["parte"])){
    $idade = $_POST["idade"];
    $interesse = $_POST["interesse"];
    $parte = $_POST["parte"];

 echo'<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Resultados da sua pesquisa</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Esta é a programação para os filtros selecionados!
                </div>
                <!-- /.panel-heading -->

                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Hora</th>
                                <th>Duração</th>
                            </tr>
                            </thead>
                            <tbody>';

                            require_once "../conections/conections.php";
                            $area = $_POST['interesse'];
                            $link = new_db_connection();
                            $stmt = mysqli_stmt_init($link);
                            $query = "SELECT eventos.nome, eventos.id_eventos, eventos_horarios.date, eventos_horarios.hora_inicio, eventos_horarios.duracao, eventos_horarios.ref_id_eventos FROM eventos_horarios INNER JOIN eventos ON eventos_horarios.ref_id_eventos = eventos.id_eventos WHERE area= ?";
                            if (mysqli_stmt_prepare($stmt, $query)) {
                                mysqli_stmt_bind_param($stmt, "s", $area);
                                mysqli_stmt_execute($stmt);
                                mysqli_stmt_bind_result($stmt, $nome, $id, $date, $hora, $duracao, $ref_id);
                                while (mysqli_stmt_fetch($stmt)) {
                                    echo '<tr>
<td>' . $nome . '</td>
<td>' . $hora . '</td>
<td>' . $duracao . '</td>
</tr>';

                                }

                                mysqli_stmt_close($stmt);


                            }
                            mysqli_close($link);

                            echo '</tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>

    </div>
</div>';










}
