<?php
session_start();
if (isset($_SESSION['id_users_geral'])) {
    $id_user = $_SESSION['id_users_geral'];
}


$evento = $_GET['id'];
$comentario = strip_tags($_POST['comentario']);
$rating = strip_tags($_POST['rating']);

$query2 = "INSERT INTO reviews (id_reviews, comentario, date_creation, ref_id_users_geral_admin, ref_id_users_geral_geral, ref_id_eventos_horarios, ref_id_eventos, rating) VALUES (NULL, ?, CURRENT_TIMESTAMP, 69, ?, '15', ?, ?);";


require_once "../conections/conections.php";

$link = new_db_connection();
$stmt = mysqli_stmt_init($link);

if (mysqli_stmt_prepare($stmt, $query2)) {

    mysqli_stmt_bind_param($stmt, "ssss", $comentario, $id_user, $evento, $rating);

    mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt, $n, $com, $dat, $ref1, $ref2, $ref3, $ref4, $r);

    mysqli_stmt_fetch($stmt);

    mysqli_stmt_close($stmt);
}
mysqli_close($link);

header('Location: ../periodicas.php?id=' . $evento);
?>
