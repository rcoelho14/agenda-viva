<?php
require_once 'scripts/encode_json.php';
?>

<!DOCTYPE HTML>
<html>
<head>
    <?php include "components/meta.php" ?>

    <!-- <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet"> -->
    <!-- <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i" rel="stylesheet"> -->

    <?php include "components/css.php" ?>


</head>
<body>


<?php include "components/menu.php" ?>
<h2 style="margin-left: 55%; margin-top: 2%">Agenda</h2>
<div class="row col-md-2 col-lg-2 col-xs-2 col-lg-offset-2 col-md-offset-2 col-xs-offset-2"
     style="margin-left: 11%; margin-top: 1.5%">
    <form class="form" action="#" method="post">
        <h2 style="margin-top: 22%;!important; margin-left: 0%; display: inline">Filtros</h2>
        <div class="form-group">
            <label>Escalão</label>
            <select name="idade" class="form-control" id="exampleSelect1">
                <option value="1">Crianças</option>
                <option value="2">Jovens</option>
                <option value="3">Adultos</option>
            </select>
        </div>
        <div class="form-group">
            <label>Área de interesse</label>
            <select name="interesse" class="form-control" id="exampleSelect1">
                <option value="1">Matemática</option>
                <option value="2">Arte</option>
                <option value="3">Física</option>
                <option value="4">Robótica</option>
                <option value="5">Ciências</option>
            </select>
        </div>
        <div class="form-group">
            <label>Parte do dia</label>
            <select name="parte" class="form-control" id="exampleSelect1">
                <option value="1">Manhã</option>
                <option value="2">Tarde</option>
            </select>
        </div>

        <button type="submit" class="btn btn-primary" id="botaosubmit">Pesquisar</button>
    </form>
</div>

<div id="page" class="col-md-offset-5 col-lg-offset-4 col-md-offset-4 col-xs-offset-4"
     style="width: 50%; margin-top: 2%; !important">
    <div class="container-public">
        <div class="monthly fh5co-container container-fluid     " id="mycalendar"></div>
    </div>
</div>


<?php
if (isset($_POST["idade"]) && isset($_POST["interesse"]) && isset($_POST["parte"])) {
    $idade = $_POST["idade"];
    $interesse = $_POST["interesse"];
    $parte = $_POST["parte"];

    echo '<div id="page-wrapper" style="width: 70%; display: block; margin: auto">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Resultados da sua pesquisa</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Esta é a programação para os filtros selecionados!
                </div>
                <!-- /.panel-heading -->

                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Hora</th>
                                <th>Duração</th>
                            </tr>
                            </thead>
                            <tbody>';

    require_once "conections/conections.php";
    $area = $_POST['interesse'];
    $link = new_db_connection();
    $stmt = mysqli_stmt_init($link);
    $query = "SELECT eventos.nome, eventos.id_eventos, eventos_horarios.date, eventos_horarios.hora_inicio, eventos_horarios.duracao, eventos_horarios.ref_id_eventos, eventos_horarios.parte FROM eventos_horarios INNER JOIN eventos ON eventos_horarios.ref_id_eventos = eventos.id_eventos WHERE area= ? AND idade = ? AND parte = ?";
    if (mysqli_stmt_prepare($stmt, $query)) {
        mysqli_stmt_bind_param($stmt, "sss", $area, $idade, $parte);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $nome, $id, $date, $hora, $duracao, $ref_id, $parte);
        while (mysqli_stmt_fetch($stmt)) {
            echo '<tr>
<td>' . $nome . '</td>
<td>' . $hora . '</td>
<td>' . $duracao . '</td>
</tr>';
            $array_json=array();
            $file_json = "events.json";
            $array_json=get_json($file_json);
            //echo'<pre>';
           // var_dump($array_json);
          //  echo '</pre>';

            //var_dump($array_json['monthly']["1"]['name']);
            /*foreach ($array_json['monthly'] as $key => $value){
                if($area == $array_json['monthly']["1"]['area']){
                    echo '<script type="text/javascript">
                        $("botaosubmit").click(function(){
                            $("#myEltId span").css("border","0 none transparent");
                                        });
                        </script>';*/



        }
        mysqli_stmt_close($stmt);
    }
    mysqli_close($link);

    echo '</tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>

    </div>
</div>';


}
?>
<?php include "components/footer.php" ?>


<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
</div>

<?php include "components/script.php" ?>


</body>
</html>
