<!DOCTYPE HTML>
<html>
<?php require_once "conections/conections.php" ?>
<?php include "components/meta.php" ?>

<!-- <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet"> -->
<!-- <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i" rel="stylesheet"> -->

<?php include "components/css.php" ?>


</head>
<body>

<div class="fh5co-loader"></div>

<div id="page" class="">
    <?php include "components/menu.php" ?>

    <div class="col-lg-6 col-12 pb-3 col-lg-offset-3">
        <div class="card h-100">
            <div class="card-body">
                <br>
                <h2 class="text-center mb-4 col-lg-offset-0">Recuperação de Password</h2>
                <h4 class="text-center mb-4 col-lg-offset-0">Insira o seu email</h4>
                <form class="col-lg-offset-2" method="post" role="form" id="register-form"
                      action="sendgrid-php/send_email.php">
                    <div class="form-group">
                        <div class="mx-auto col-sm-10 pb-3 pt-2">
                            <?php
                            if (isset($_SESSION["msg"])) {
                                if ($_SESSION["msg"] == 0) {
                                    echo "<div class='alert alert-danger'>
                <strong>Erro!</strong> Tente outra vez
          </div>";
                                    $_SESSION["msg"] = 12;
                                }
                                if ($_SESSION["msg"] == 1) {
                                    echo "<div class='alert alert-danger'>
                <strong>Erro!</strong> Email não registado!
          </div>";
                                    $_SESSION["msg"] = 12;
                                }
                            }
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="mx-auto col-sm-10">
                            <input type="email" class="form-control" id="email" name="email"
                                   placeholder="Email"
                                   required="required">
                            <br>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="mx-auto col-sm-10 pb-3 pt-2">
                            <button type="submit" class="btn btn-outline-secondary btn-lg btn-block">Enviar email de
                                recuperação
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <?php include "components/footer.php" ?>
</div>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
</div>

<?php include "components/script.php" ?>
</body>
</html>